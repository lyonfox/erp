object frmBaseRunningAccount: TfrmBaseRunningAccount
  Left = 0
  Top = 0
  Caption = #27969#27700#24080#22522#31867
  ClientHeight = 594
  ClientWidth = 1087
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 274
    Top = 0
    Width = 10
    Height = 594
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitTop = -194
    ExplicitHeight = 791
  end
  object Client: TRzPanel
    Left = 284
    Top = 0
    Width = 803
    Height = 594
    Align = alClient
    BorderOuter = fsFlat
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 1
      Top = 240
      Width = 801
      Height = 10
      Cursor = crVSplit
      Align = alBottom
      Color = 16250613
      ParentColor = False
      ResizeStyle = rsUpdate
      ExplicitLeft = -4
      ExplicitTop = 470
      ExplicitWidth = 783
    end
    object RzPanel2: TRzPanel
      Left = 1
      Top = 1
      Width = 801
      Height = 239
      Align = alClient
      BorderOuter = fsNone
      TabOrder = 0
      object Grid: TcxGrid
        Left = 0
        Top = 29
        Width = 801
        Height = 210
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object tvMaster: TcxGridDBTableView
          PopupMenu = pmMaster
          OnDblClick = tvMasterDblClick
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Append.Enabled = False
          OnEditing = tvMasterEditing
          OnEditKeyDown = tvMasterEditKeyDown
          DataController.DataModeController.GridModeBufferCount = 15
          DataController.DataSource = MasterGrid
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = #21512#35745
              Position = spFooter
            end
            item
              Format = #25968#20540':0.########;-0.########'
              Kind = skSum
            end
            item
              Format = #25968#20540':0.########;-0.########'
              Kind = skSum
              Position = spFooter
            end
            item
              Format = #25968#37327':0.###;-0.###'
              Kind = skSum
            end
            item
              Format = #25968#37327':0.###;-0.###'
              Kind = skSum
              Position = spFooter
            end
            item
              Format = #37329#39069':'#165',0.00;'#165'-,0.00'
              Kind = skSum
              Position = spFooter
              Column = tvCol13
            end
            item
              Format = #37329#39069':'#165',0.00;'#165'-,0.00'
              Kind = skSum
              Column = tvCol13
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = #24635#39069':'#165',0.00;'#165'-,0.00'
              Kind = skSum
              OnGetText = tvMasterTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
              Column = tvCol13
            end
            item
              Format = #22823#20889
              Kind = skCount
              Column = tvCol14
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
          OptionsView.DataRowHeight = 23
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderFilterButtonShowMode = fbmButton
          OptionsView.HeaderHeight = 26
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 14
          Styles.OnGetContentStyle = tvMasterStylesGetContentStyle
          OnColumnSizeChanged = tvMasterColumnSizeChanged
          object tvMasterColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
          end
          object tvCol1: TcxGridDBColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            OnGetDisplayText = tvCol1GetDisplayText
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Moving = False
            Options.Sorting = False
            Width = 40
          end
          object tvCol2: TcxGridDBColumn
            Caption = #29366#24577
            DataBinding.FieldName = 'status'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.OnValidate = tvCol2PropertiesValidate
            HeaderAlignmentHorz = taCenter
            Width = 50
          end
          object tvCol3: TcxGridDBColumn
            Caption = #32534#21495
            DataBinding.FieldName = 'numbers'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 91
          end
          object tvCol4: TcxGridDBColumn
            Caption = #26085#26399
            DataBinding.FieldName = 'cxDate'
            PropertiesClassName = 'TcxDateEditProperties'
            Width = 79
          end
          object tvCol5: TcxGridDBColumn
            Caption = #21046#34920#31867#21035
            DataBinding.FieldName = 'TabType'
            PropertiesClassName = 'TcxTextEditProperties'
            RepositoryItem = DM.TabBox
            Options.Editing = False
            Width = 62
          end
          object tvCol6: TcxGridDBColumn
            Caption = #25910#27454#21333#20301
            DataBinding.FieldName = 'Receivables'
            RepositoryItem = DM.SignNameBox
          end
          object tvCol7: TcxGridDBColumn
            Caption = #20132#26131#26041#24335
            DataBinding.FieldName = 'TradeType'
            RepositoryItem = DM.MarketTypeBox
            Width = 84
          end
          object tvCol8: TcxGridDBColumn
            Caption = #20184#27454#21333#20301
            DataBinding.FieldName = 'PaymentType'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownWidth = 180
            Properties.ImmediateDropDownWhenActivated = True
            Properties.KeyFieldNames = 'SignName'
            Properties.ListColumns = <
              item
                Width = 120
                FieldName = 'SignName'
              end
              item
                Width = 60
                FieldName = 'PYCode'
              end>
            RepositoryItem = DM.SignNameBox
            Width = 124
          end
          object tvCol9: TcxGridDBColumn
            Caption = #24037#31243'/'#21517#31216
            DataBinding.FieldName = 'ProjectName'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsEditList
            Properties.DropDownWidth = 200
            Properties.ImmediateDropDownWhenActivated = True
            Properties.KeyFieldNames = 'ProjectName'
            Properties.ListColumns = <
              item
                Caption = #24037#31243#21517
                Width = 140
                FieldName = 'ProjectName'
              end
              item
                Caption = #31616#30721
                Width = 60
                FieldName = 'ProjectPYCode'
              end>
            RepositoryItem = DM.ProjectNameBox
            Width = 72
          end
          object tvCol10: TcxGridDBColumn
            Caption = #36153#29992#31185#30446
            DataBinding.FieldName = 'CategoryName'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.ImmediateDropDownWhenActivated = True
            Properties.KeyFieldNames = 'CostName'
            Properties.ListColumns = <
              item
                Caption = 'CostName'
                FieldName = 'CostName'
              end>
            Properties.ListOptions.ShowHeader = False
            RepositoryItem = DM.CostComboBox
            Width = 86
          end
          object tvCol11: TcxGridDBColumn
            Caption = #25910#27454#20154#21517#31216
            DataBinding.FieldName = 'Payee'
            PropertiesClassName = 'TcxTextEditProperties'
            Width = 77
          end
          object tvCol12: TcxGridDBColumn
            Caption = #20184#27454#20154#21517#31216
            DataBinding.FieldName = 'Drawee'
            PropertiesClassName = 'TcxTextEditProperties'
            Width = 102
          end
          object tvCol13: TcxGridDBColumn
            Caption = #37329#39069
            DataBinding.FieldName = 'SumMoney'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.DecimalPlaces = 4
            Properties.DisplayFormat = #165',0.00##;'#165'-,0.00##'
            Properties.OnValidate = tvCol13PropertiesValidate
            HeaderAlignmentHorz = taRightJustify
            Styles.Content = DM.SumMoney
            Width = 102
          end
          object tvCol14: TcxGridDBColumn
            Caption = #22823#20889#37329#39069
            DataBinding.FieldName = 'CapitalMoney'
            Width = 230
          end
          object tvCol15: TcxGridDBColumn
            Caption = #22791#27880
            DataBinding.FieldName = 'Remarks'
            PropertiesClassName = 'TcxMemoProperties'
            Properties.Alignment = taLeftJustify
            Properties.ScrollBars = ssVertical
            Width = 230
          end
        end
        object Lv1: TcxGridLevel
          GridView = tvMaster
        end
      end
      object RzToolbar3: TRzToolbar
        Left = 0
        Top = 0
        Width = 801
        Height = 29
        AutoStyle = False
        Images = DM.cxImageList1
        BorderInner = fsNone
        BorderOuter = fsNone
        BorderSides = []
        BorderWidth = 0
        GradientColorStyle = gcsCustom
        TabOrder = 1
        VisualStyle = vsGradient
        ToolbarControls = (
          RzSpacer15
          RzToolButton11
          RzSpacer16
          RzToolButton13
          RzSpacer17
          RzToolButton12
          RzSpacer18
          RzToolButton15
          RzSpacer19
          RzToolButton1
          RzSpacer1
          RzToolButton16
          RzSpacer2
          RzToolButton2)
        object RzSpacer15: TRzSpacer
          Left = 4
          Top = 2
        end
        object RzToolButton11: TRzToolButton
          Left = 12
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 37
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #26032#22686
          OnClick = RzToolButton11Click
        end
        object RzSpacer16: TRzSpacer
          Left = 74
          Top = 2
        end
        object RzToolButton12: TRzToolButton
          Left = 152
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 3
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #20445#23384
          OnClick = RzToolButton12Click
        end
        object RzSpacer17: TRzSpacer
          Left = 144
          Top = 2
        end
        object RzToolButton13: TRzToolButton
          Left = 82
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 0
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #32534#36753
          OnClick = RzToolButton13Click
        end
        object RzSpacer18: TRzSpacer
          Left = 214
          Top = 2
        end
        object RzToolButton15: TRzToolButton
          Left = 222
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 51
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #21024#38500
        end
        object RzSpacer19: TRzSpacer
          Left = 284
          Top = 2
        end
        object RzToolButton16: TRzToolButton
          Left = 382
          Top = 2
          Width = 76
          SelectionColorStop = 16119543
          DropDownMenu = Repot
          ImageIndex = 5
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          ToolStyle = tsDropDown
          Caption = #25253#34920
        end
        object RzSpacer1: TRzSpacer
          Left = 374
          Top = 2
        end
        object RzToolButton1: TRzToolButton
          Left = 292
          Top = 2
          Width = 82
          SelectionColorStop = 16119543
          ImageIndex = 4
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #34920#26684#35774#32622
          OnClick = RzToolButton1Click
        end
        object RzSpacer2: TRzSpacer
          Left = 458
          Top = 2
        end
        object RzToolButton2: TRzToolButton
          Left = 466
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 8
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #36864#20986
          OnClick = RzToolButton2Click
        end
      end
    end
    object RzPanel3: TRzPanel
      Left = 1
      Top = 250
      Width = 801
      Height = 343
      Align = alBottom
      BorderOuter = fsNone
      TabOrder = 1
      object cxGrid1: TcxGrid
        Left = 0
        Top = 29
        Width = 801
        Height = 314
        Align = alClient
        TabOrder = 0
        object tvDetailed: TcxGridDBTableView
          PopupMenu = pmDetailed
          Navigator.Buttons.CustomButtons = <>
          OnEditing = cxGrid1DBTableView1Editing
          OnEditKeyDown = tvDetailedEditKeyDown
          DataController.DataSource = DetailedGrid
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skSum
              Column = tvDetailedColumn8
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsView.DataRowHeight = 23
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 14
          OnColumnSizeChanged = tvDetailedColumnSizeChanged
          object tvDetailedColumn1: TcxGridDBColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            OnGetDisplayText = tvCol1GetDisplayText
            Options.Editing = False
            Width = 40
          end
          object tvDetailedColumn2: TcxGridDBColumn
            Caption = #32534#21495
            DataBinding.FieldName = 'Parent'
            Width = 137
          end
          object tvDetailedColumn4: TcxGridDBColumn
            Caption = #39033#30446#21517#31216
            DataBinding.FieldName = 'appropriation'
            Width = 104
          end
          object tvDetailedColumn3: TcxGridDBColumn
            Caption = #36153#29992#31185#30446
            DataBinding.FieldName = 'ProjectName'
            RepositoryItem = DM.CostComboBox
            Width = 123
          end
          object tvDetailedColumn5: TcxGridDBColumn
            Caption = #25968#37327
            DataBinding.FieldName = 'fine'
            PropertiesClassName = 'TcxSpinEditProperties'
            Properties.OnValidate = tvDetailedColumn5PropertiesValidate
            Width = 65
          end
          object tvDetailedColumn6: TcxGridDBColumn
            Caption = #21333#20215
            DataBinding.FieldName = 'Armourforwood'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DecimalPlaces = 4
            Properties.DisplayFormat = #165',0.00##;'#165'-,0.00##'
            Properties.OnValidate = tvDetailedColumn6PropertiesValidate
            Width = 64
          end
          object tvDetailedColumn7: TcxGridDBColumn
            Caption = #35745#31639#20844#24335
            DataBinding.FieldName = 'DesignFormulas'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.OnValidate = tvDetailedColumn7PropertiesValidate
            Width = 174
          end
          object tvDetailedColumn8: TcxGridDBColumn
            Caption = #37329#39069
            DataBinding.FieldName = 'receiptsMoney'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = #165',0.00##;'#165'-,0.00##'
            Styles.Content = DM.SumMoney
            Width = 92
          end
          object tvDetailedColumn9: TcxGridDBColumn
            Caption = #22791#27880
            DataBinding.FieldName = 'Remarks'
            PropertiesClassName = 'TcxMemoProperties'
            Width = 332
          end
        end
        object lv2: TcxGridLevel
          GridView = tvDetailed
        end
      end
      object RzToolbar1: TRzToolbar
        Left = 0
        Top = 0
        Width = 801
        Height = 29
        Images = DM.cxImageList1
        BorderInner = fsNone
        BorderOuter = fsGroove
        BorderSides = [sdTop]
        BorderWidth = 0
        GradientColorStyle = gcsCustom
        TabOrder = 1
        VisualStyle = vsGradient
        ToolbarControls = (
          rzspcrA
          btnNew
          rzspcrB
          btnEdit
          rzspcrC
          btnSave
          rzspcrD
          btnDelete
          rzspcrE
          btnSetGrid
          rzspcrr
          btnReport)
        object rzspcrA: TRzSpacer
          Left = 4
          Top = 2
        end
        object btnNew: TRzToolButton
          Left = 12
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 37
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #26032#22686
          OnClick = btnNewClick
        end
        object rzspcrB: TRzSpacer
          Left = 74
          Top = 2
        end
        object btnSave: TRzToolButton
          Left = 152
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 3
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #20445#23384
          Enabled = False
          OnClick = RzToolButton12Click
        end
        object rzspcrC: TRzSpacer
          Left = 144
          Top = 2
        end
        object btnEdit: TRzToolButton
          Left = 82
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 0
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #32534#36753
          Enabled = False
          OnClick = btnEditClick
        end
        object rzspcrD: TRzSpacer
          Left = 214
          Top = 2
        end
        object btnDelete: TRzToolButton
          Left = 222
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 51
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #21024#38500
          Enabled = False
        end
        object rzspcrE: TRzSpacer
          Left = 284
          Top = 2
        end
        object btnReport: TRzToolButton
          Left = 380
          Top = 2
          Width = 76
          SelectionColorStop = 16119543
          DropDownMenu = Repot
          ImageIndex = 5
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          ToolStyle = tsDropDown
          Caption = #25253#34920
          Enabled = False
        end
        object rzspcrr: TRzSpacer
          Left = 372
          Top = 2
        end
        object btnSetGrid: TRzToolButton
          Left = 292
          Top = 2
          Width = 80
          ImageIndex = 4
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #34920#26684#35774#32622
          Enabled = False
          OnClick = btnSetGridClick
        end
      end
    end
  end
  object Left: TRzPanel
    Left = 0
    Top = 0
    Width = 274
    Height = 594
    Align = alLeft
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdRight]
    TabOrder = 1
    object TopSplitter: TSplitter
      Left = 1
      Top = 308
      Width = 272
      Height = 10
      Cursor = crVSplit
      Align = alTop
      Color = 16250613
      ParentColor = False
      ResizeStyle = rsUpdate
      ExplicitLeft = 6
      ExplicitTop = 201
    end
    object Tv: TTreeView
      AlignWithMargins = True
      Left = 4
      Top = 32
      Width = 266
      Height = 273
      Align = alTop
      DragMode = dmAutomatic
      HideSelection = False
      Images = DM.TreeImageList
      Indent = 19
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      OnClick = TvClick
      Items.NodeData = {
        0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        00000000000104805F656755534D4F}
    end
    object KeyTool: TRzToolbar
      Left = 1
      Top = 0
      Width = 272
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 1
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer7
        cxLabel3
        cxLookupComboBox2
        RzSpacer3
        RzToolButton4
        RzSpacer51
        Refresh)
      object RzSpacer7: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzSpacer3: TRzSpacer
        Left = 201
        Top = 2
      end
      object RzToolButton4: TRzToolButton
        Left = 209
        Top = 2
        ImageIndex = 10
      end
      object RzSpacer51: TRzSpacer
        Left = 234
        Top = 2
      end
      object Refresh: TRzToolButton
        Left = 242
        Top = 2
        ImageIndex = 11
        OnClick = RefreshClick
      end
      object cxLabel3: TcxLabel
        Left = 12
        Top = 6
        Caption = #20851#38190#23383':'
        Transparent = True
      end
      object cxLookupComboBox2: TcxLookupComboBox
        Left = 56
        Top = 4
        RepositoryItem = DM.SignNameBox
        Properties.DropDownWidth = 260
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'SignName'
        Properties.ListColumns = <
          item
            Caption = #24448#26469#21333#20301
            Width = 150
            FieldName = 'SignName'
          end
          item
            Caption = #31616#30721
            Width = 60
            FieldName = 'PYCode'
          end>
        TabOrder = 1
        Width = 145
      end
    end
    object RzPageControl1: TRzPageControl
      Left = 1
      Top = 318
      Width = 272
      Height = 276
      Hint = ''
      ActivePage = TabSheet1
      Align = alClient
      TabIndex = 0
      TabOrder = 2
      TabStyle = tsCutCorner
      FixedDimension = 20
      object TabSheet1: TRzTabSheet
        Caption = #24448#26469#21333#20301
        object RzToolbar5: TRzToolbar
          Left = 0
          Top = 0
          Width = 268
          Height = 29
          AutoStyle = False
          Images = DM.cxImageList1
          BorderInner = fsFlat
          BorderOuter = fsNone
          BorderSides = [sdTop, sdBottom]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 0
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer26
            cxLabel15
            Search
            RzSpacer27
            RzToolButton9)
          object RzSpacer26: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzSpacer27: TRzSpacer
            Left = 225
            Top = 2
          end
          object RzToolButton9: TRzToolButton
            Left = 233
            Top = 2
            SelectionColorStop = 16119543
            ImageIndex = 10
          end
          object cxLabel15: TcxLabel
            Left = 12
            Top = 6
            Caption = #24448#26469#21333#20301#65306
            Transparent = True
          end
          object Search: TcxLookupComboBox
            Left = 76
            Top = 4
            RepositoryItem = DM.SignNameBox
            Properties.DropDownWidth = 260
            Properties.ImmediateDropDownWhenActivated = True
            Properties.KeyFieldNames = 'SignName'
            Properties.ListColumns = <
              item
                Caption = #24448#26469#21333#20301
                Width = 150
                FieldName = 'SignName'
              end
              item
                Caption = #31616#30721
                Width = 60
                FieldName = 'PYCode'
              end>
            TabOrder = 1
            OnKeyDown = SearchKeyDown
            Width = 149
          end
        end
        object cxGrid2: TcxGrid
          Left = 0
          Top = 29
          Width = 268
          Height = 223
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 1
          object tvSignName: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DataCompany
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.DataRowHeight = 23
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 16
            object tvSignNameCol1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvSignNameCol1GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 30
            end
            object tvSignNameCol2: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'SignName'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 118
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = tvSignName
          end
        end
      end
      object TabSheet2: TRzTabSheet
        Caption = #20844#21496#21517#31216
        object RzToolbar2: TRzToolbar
          Left = 0
          Top = 0
          Width = 268
          Height = 29
          AutoStyle = False
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsNone
          BorderSides = [sdBottom]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 0
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer12
            cxLabel2
            cxLookupComboBox1
            RzSpacer13
            RzToolButton6)
          object RzSpacer12: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzSpacer13: TRzSpacer
            Left = 225
            Top = 2
          end
          object RzToolButton6: TRzToolButton
            Left = 233
            Top = 2
            ImageIndex = 10
            OnClick = RzToolButton13Click
          end
          object cxLabel2: TcxLabel
            Left = 12
            Top = 6
            Caption = #20844#21496#21517#31216#65306
            Transparent = True
          end
          object cxLookupComboBox1: TcxLookupComboBox
            Left = 76
            Top = 4
            RepositoryItem = DM.CompanyList
            Properties.ImmediateDropDownWhenActivated = True
            Properties.ListColumns = <>
            TabOrder = 1
            Width = 149
          end
        end
        object cxGrid3: TcxGrid
          Left = 0
          Top = 29
          Width = 268
          Height = 223
          Align = alClient
          TabOrder = 1
          object tvCompany: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DM.DataCompany
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.DataRowHeight = 23
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            object tvCompanyColumn1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              HeaderAlignmentHorz = taCenter
              HeaderAlignmentVert = vaTop
              Options.Filtering = False
              Options.Moving = False
              Options.Sorting = False
              Width = 35
            end
            object tvCompanyColumn2: TcxGridDBColumn
              Caption = #20844#21496#21517#31216
              DataBinding.FieldName = 'SignName'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Moving = False
              Options.Sorting = False
              Width = 211
            end
          end
          object lv: TcxGridLevel
            GridView = tvCompany
          end
        end
      end
    end
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = MasterAfterInsert
    Left = 192
    Top = 184
  end
  object MasterGrid: TDataSource
    DataSet = Master
    Left = 192
    Top = 128
  end
  object Repot: TPopupMenu
    Images = DM.cxImageList1
    Left = 387
    Top = 98
    object Execl1: TMenuItem
      Caption = #36755#20986'Excel'
      OnClick = Execl1Click
    end
    object N6: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
      OnClick = N6Click
    end
  end
  object DetailedGrid: TDataSource
    DataSet = Detailed
    Left = 120
    Top = 312
  end
  object pmDetailed: TPopupMenu
    Images = DM.cxImageList1
    Left = 336
    Top = 408
    object N5: TMenuItem
      Caption = #32534#36753
      ImageIndex = 0
      OnClick = N4Click
    end
    object N4: TMenuItem
      Caption = #20445#23384
      ImageIndex = 3
    end
    object N8: TMenuItem
      Caption = #21024#38500
      ImageIndex = 51
    end
  end
  object Detailed: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = DetailedAfterOpen
    AfterClose = DetailedAfterClose
    AfterInsert = DetailedAfterInsert
    Left = 120
    Top = 256
  end
  object pmMaster: TPopupMenu
    Images = DM.cxImageList1
    Left = 336
    Top = 352
    object N1: TMenuItem
      Caption = #26032#22686
      ImageIndex = 37
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #32534#36753
      ImageIndex = 0
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #20445#23384
      ImageIndex = 3
      OnClick = N3Click
    end
    object N7: TMenuItem
      Caption = #21024#38500
      ImageIndex = 51
      OnClick = N7Click
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N9: TMenuItem
      Caption = #21047#26032
      ImageIndex = 11
      OnClick = N9Click
    end
    object N10: TMenuItem
      Caption = #38468#20214
      ImageIndex = 55
      OnClick = N10Click
    end
  end
  object ADOCompany: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 80
    Top = 416
  end
  object DataCompany: TDataSource
    DataSet = ADOCompany
    Left = 80
    Top = 464
  end
end
