unit ufrmPayDetailedPost;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxGroupBox, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxLabel, cxCurrencyEdit, cxMemo, cxDBEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Data.DB;

type
  TfrmPayDetailedPost = class(TForm)
    cxGroupBox1: TcxGroupBox;
    cxDBLookupComboBox1: TcxDBLookupComboBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxDBMemo1: TcxDBMemo;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxDBTextEdit1: TcxDBTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    ds: TDataSource;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPayDetailedPost: TfrmPayDetailedPost;

implementation

uses
   uDataModule;

{$R *.dfm}

procedure TfrmPayDetailedPost.cxButton1Click(Sender: TObject);
begin
  {
  Self.cxDBLookupComboBox1.PostEditValue;
  Self.cxDBTextEdit1.PostEditValue;
  Self.cxDBCurrencyEdit1.PostEditValue;
  Self.cxDBMemo1.PostEditValue;
  }
  with Self.cxDBLookupComboBox1.DataBinding.DataSource.DataSet do
  begin
    Post;
    First;
    Insert;
  end;
  Self.cxDBLookupComboBox1.SetFocus;

end;

procedure TfrmPayDetailedPost.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    27:
    begin

    end;
    13:
    begin

    end;
  end;
end;

end.
