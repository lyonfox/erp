unit NewDateTimePicker;

interface

uses
  SysUtils, Classes, Controls, ComCtrls, ComStrs, Math;

type
  TDateTimePickerNew = class(TDateTimePicker)
  private
    { Private declarations }
    FMaxDateTime, FMinDateTime: TDateTime;
  protected
    { Protected declarations }
    procedure SetMaxDateTime(AMax: TDateTime);
    procedure SetMinDateTime(AMin: TDateTime);
    procedure Change; override;
    procedure CreateWnd; override;
  public
    { Public declarations }

    constructor Create(AOwner: TComponent); override;
    procedure OnDateTimeChange(Sender: TObject);
  published
    { Published declarations }
    property MaxDateTime: TDateTime read FMaxDateTime write SetMaxDateTime;
    property MinDateTime: TDateTime read FMinDateTime write SetMinDateTime;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DSLWORK', [TDateTimePickerNew]);
end;

procedure TDateTimePickerNew.Change;
begin
  OnDateTimeChange(Self);
  inherited Change;
end;

constructor TDateTimePickerNew.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  { //下面的代码已经被移动到 CreateWnd 中,否则会报 Control '' has no parent
Format := 'yyyy-MM-dd HH:mm:ss';
DateFormat := dfLong;
DateMode := dmUpDown;
Kind := dtkTime; }

  FMaxDateTime := 0.0;
  FMinDateTime := 0.0;

end;

procedure TDateTimePickerNew.CreateWnd;
begin
  inherited;
  Format     := 'yyyy-MM-dd HH:mm:ss';
  DateFormat := dfLong;
  DateMode   := dmUpDown;
  Kind       := dtkTime;

{ 第一中文文摘 http://www.1zwwz.cn/ }
  //FMaxDateTime := 0.0;
  //FMinDateTime := 0.0;
end;

procedure TDateTimePickerNew.OnDateTimeChange(Sender: TObject);
var
  dtMax, dtMin: string;
begin
  dtMax := FormatDateTime(Self.Format, FMaxDateTime);
  dtMin := FormatDateTime(Self.Format, FMinDateTime);
  DateTime := StrToDateTime(Self.Caption); //这一行上一次发布时忘了写上了
  if (FMinDateTime <> 0.0) and (Self.Caption < dtMin) then
    DateTime := FMinDateTime;
  if (FMaxDateTime <> 0.0) and (Self.Caption > dtMax) then
    DateTime := FMaxDateTime;
end;

procedure TDateTimePickerNew.SetMaxDateTime(AMax: TDateTime);
begin
  if (FMinDateTime <> 0.0) and (AMax < FMinDateTime) then
    raise CalExceptionClass.CreateFmt(SDateTimeMin, [DateToStr(FMinDateTime)]);
  if FMaxDateTime <> AMax then begin
    FMaxDateTime := AMax;
    //MaxDate := Ceil(AMax);
  end;
end;

procedure TDateTimePickerNew.SetMinDateTime(AMin: TDateTime);
begin
  if (FMaxDateTime <> 0.0) and (AMin > FMaxDateTime) then
    raise CalExceptionClass.CreateFmt(SDateTimeMax, [DateToStr(FMaxDateTime)]);
  if FMinDateTime <> AMin then begin
    FMinDateTime := AMin;
    //MinDate := Floor(AMin);
  end;
end;

end.

 