object frmCustomer: TfrmCustomer
  Left = 0
  Top = 0
  Caption = #25253#20215#21333#20301
  ClientHeight = 494
  ClientWidth = 1024
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 475
    Width = 1024
    Height = 19
    Panels = <>
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1024
    Height = 475
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnl1'
    TabOrder = 1
    object bvl1: TBevel
      Left = 281
      Top = 0
      Width = 10
      Height = 475
      Align = alLeft
      Shape = bsSpacer
      ExplicitLeft = 328
      ExplicitHeight = 409
    end
    object RzPanel10: TRzPanel
      Left = 0
      Top = 0
      Width = 281
      Height = 475
      Align = alLeft
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdLeft, sdRight]
      TabOrder = 0
      object tv: TTreeView
        Left = 1
        Top = 32
        Width = 279
        Height = 443
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        DragMode = dmAutomatic
        HideSelection = False
        Images = DM.TreeImageList
        Indent = 19
        RowSelect = True
        SortType = stBoth
        TabOrder = 0
        OnClick = tvClick
        OnDblClick = tvDblClick
        OnEdited = tvEdited
        OnKeyDown = tvKeyDown
        Items.NodeData = {
          0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
          00000000000104A562F74E06527B7C}
      end
      object RzToolbar6: TRzToolbar
        Left = 1
        Top = 0
        Width = 279
        AutoStyle = False
        Images = DM.cxImageList1
        RowHeight = 28
        BorderInner = fsNone
        BorderOuter = fsFlat
        BorderSides = [sdTop]
        BorderWidth = 0
        GradientColorStyle = gcsCustom
        TabOrder = 1
        VisualStyle = vsGradient
        ToolbarControls = (
          RzSpacer16
          RzToolButton18
          RzSpacer26
          RzToolButton19
          RzSpacer17
          RzToolButton20
          RzSpacer21)
        object RzSpacer16: TRzSpacer
          Left = 4
          Top = 4
        end
        object RzToolButton18: TRzToolButton
          Left = 12
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 37
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #26032#22686
          OnClick = RzToolButton18Click
        end
        object RzSpacer17: TRzSpacer
          Left = 124
          Top = 4
        end
        object RzToolButton20: TRzToolButton
          Left = 132
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 51
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #21024#38500
          OnClick = RzToolButton20Click
        end
        object RzSpacer21: TRzSpacer
          Left = 184
          Top = 4
        end
        object RzSpacer26: TRzSpacer
          Left = 64
          Top = 4
        end
        object RzToolButton19: TRzToolButton
          Left = 72
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 0
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #32534#36753
          OnClick = RzToolButton18Click
        end
      end
    end
    object Panel1: TPanel
      Left = 291
      Top = 0
      Width = 733
      Height = 475
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Grid: TcxGrid
        Left = 0
        Top = 34
        Width = 733
        Height = 441
        Align = alClient
        TabOrder = 0
        object tvView: TcxGridDBTableView
          PopupMenu = Old
          OnDblClick = tvViewDblClick
          Navigator.Buttons.CustomButtons = <>
          OnEditing = tvViewEditing
          DataController.DataSource = dsMasterSource
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsView.DataRowHeight = 22
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 14
          object tvViewCol1: TcxGridDBColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            OnGetDisplayText = tvViewCol1GetDisplayText
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 40
          end
          object tvViewCol6: TcxGridDBColumn
            DataBinding.FieldName = 'Id'
            Visible = False
            Width = 40
          end
          object tvViewCol3: TcxGridDBColumn
            Caption = #26085#26399
            DataBinding.FieldName = 'InputDate'
            PropertiesClassName = 'TcxDateEditProperties'
            Properties.Alignment.Horz = taCenter
            HeaderAlignmentHorz = taCenter
            Options.Filtering = False
            Options.Sorting = False
            Width = 81
          end
          object tvViewCol2: TcxGridDBColumn
            Caption = #24448#26469#21333#20301
            DataBinding.FieldName = 'SignName'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = tvViewCol2PropertiesButtonClick
            HeaderAlignmentHorz = taCenter
            Width = 218
          end
          object tvViewCol5: TcxGridDBColumn
            Caption = #31867#21035
            DataBinding.FieldName = 'remarks'
            PropertiesClassName = 'TcxTextEditProperties'
            Width = 60
          end
          object tvViewCol7: TcxGridDBColumn
            DataBinding.FieldName = 'parent'
            Visible = False
            Width = 60
          end
          object tvViewCol4: TcxGridDBColumn
            Caption = #22791#27880
            DataBinding.FieldName = 'status'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.Alignment.Horz = taCenter
            Properties.DropDownListStyle = lsEditFixedList
            Properties.ImmediateDropDownWhenActivated = True
            HeaderAlignmentHorz = taCenter
            Width = 244
          end
          object tvViewCol8: TcxGridDBColumn
            Caption = #30005#35805
            DataBinding.FieldName = 'CustomerPhone'
            Visible = False
            Width = 81
          end
        end
        object Lv: TcxGridLevel
          GridView = tvView
        end
      end
      object RzToolbar10: TRzToolbar
        Left = 0
        Top = 0
        Width = 733
        Height = 34
        AutoStyle = False
        Images = DM.cxImageList1
        RowHeight = 30
        BorderInner = fsNone
        BorderOuter = fsNone
        BorderSides = [sdTop]
        BorderWidth = 0
        GradientColorStyle = gcsCustom
        TabOrder = 1
        VisualStyle = vsGradient
        ToolbarControls = (
          RzSpacer77
          cxLabel2
          cxLookupComboBox1
          RzSpacer3
          RzBut1
          RzSpacer1
          RzSpacer4
          RzBut2
          RzSpacer5
          RzBut3
          RzSpacer78
          RzBut4
          RzSpacer79
          RzBut5
          RzSpacer7
          RzBut6
          RzSpacer6
          RzBut7
          RzSpacer80
          RzBut8
          RzSpacer8)
        object RzSpacer77: TRzSpacer
          Left = 4
          Top = 5
        end
        object RzBut2: TRzToolButton
          Left = 273
          Top = 5
          Hint = #26032#28155#21152#19968#26465#24037#31243#39033#30446
          ImageIndex = 37
          ShowCaption = False
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          ParentShowHint = False
          ShowHint = True
          OnClick = RzBut2Click
        end
        object RzSpacer78: TRzSpacer
          Left = 331
          Top = 5
        end
        object RzBut4: TRzToolButton
          Left = 339
          Top = 5
          ImageIndex = 3
          ShowCaption = False
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #20445#23384
          ParentShowHint = False
          ShowHint = True
          OnClick = RzBut4Click
        end
        object RzSpacer79: TRzSpacer
          Left = 364
          Top = 5
        end
        object RzBut5: TRzToolButton
          Left = 372
          Top = 5
          Hint = #21024#38500#25152#26377#19982#26412#24037#31243#30456#20851#32852#30340#25968#25454#19982#23376#24037#31243
          ImageIndex = 51
          ShowCaption = False
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #21024#38500
          ParentShowHint = False
          ShowHint = True
          OnClick = RzBut5Click
        end
        object RzSpacer80: TRzSpacer
          Left = 477
          Top = 5
        end
        object RzBut8: TRzToolButton
          Left = 485
          Top = 5
          Width = 30
          ImageIndex = 8
          ShowCaption = False
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #36864#20986
          ParentShowHint = False
          ShowHint = True
          OnClick = RzBut8Click
        end
        object RzSpacer1: TRzSpacer
          Left = 250
          Top = 5
          Width = 15
        end
        object RzBut1: TRzToolButton
          Left = 225
          Top = 5
          ImageIndex = 10
          OnClick = RzBut1Click
        end
        object RzBut7: TRzToolButton
          Left = 438
          Top = 5
          Width = 39
          DropDownMenu = pm
          ImageIndex = 16
          ToolStyle = tsDropDown
          Caption = #21382#21490#25968#25454
        end
        object RzSpacer3: TRzSpacer
          Left = 217
          Top = 5
        end
        object RzSpacer4: TRzSpacer
          Left = 265
          Top = 5
        end
        object RzSpacer5: TRzSpacer
          Left = 298
          Top = 5
        end
        object RzBut3: TRzToolButton
          Left = 306
          Top = 5
          Hint = #32534#36753#24037#31243#39033#30446
          ImageIndex = 0
          ParentShowHint = False
          ShowHint = True
          OnClick = RzBut3Click
        end
        object RzSpacer6: TRzSpacer
          Left = 430
          Top = 5
        end
        object RzSpacer7: TRzSpacer
          Left = 397
          Top = 5
        end
        object RzSpacer8: TRzSpacer
          Left = 515
          Top = 5
        end
        object RzBut6: TRzToolButton
          Left = 405
          Top = 5
          Hint = #35835#21462#25152#26377#26410#21024#38500#30340#24037#31243
          ImageIndex = 11
          Caption = #21047#26032
          ParentShowHint = False
          ShowHint = True
          OnClick = RzBut6Click
        end
        object cxLabel2: TcxLabel
          Left = 12
          Top = 9
          Caption = #20851#38190#23383':'
          Transparent = True
        end
        object cxLookupComboBox1: TcxLookupComboBox
          Left = 56
          Top = 7
          RepositoryItem = DM.SignNameBox
          Properties.DropDownListStyle = lsEditList
          Properties.ImmediateDropDownWhenActivated = True
          Properties.KeyFieldNames = 'ProjectName'
          Properties.ListColumns = <
            item
              Caption = #24037#31243#21517#31216
              FieldName = 'ProjectName'
            end
            item
              Caption = #31616#30721
              FieldName = 'ProjectPYCode'
            end>
          Properties.ListOptions.GridLines = glVertical
          Properties.ListOptions.ShowHeader = False
          Properties.ListOptions.SyncMode = True
          Properties.OnEditValueChanged = cxLookupComboBox1PropertiesEditValueChanged
          TabOrder = 1
          Width = 161
        end
      end
    end
  end
  object dsMaster: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = dsMasterAfterInsert
    Left = 336
    Top = 200
  end
  object dsMasterSource: TDataSource
    DataSet = dsMaster
    Left = 336
    Top = 256
  end
  object pm: TPopupMenu
    Left = 680
    Top = 152
    object N7: TMenuItem
      Caption = #21382#21490#24037#31243
      OnClick = MenuItem1Click
    end
    object N9: TMenuItem
      Caption = #24674#22797#24037#31243
      Enabled = False
      OnClick = N1Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object N11: TMenuItem
      Caption = #24443#24213#21024#38500#24037#31243
      Enabled = False
      OnClick = MenuItem2Click
    end
  end
  object Old: TPopupMenu
    Images = DM.cxImageList1
    Left = 624
    Top = 152
    object N6: TMenuItem
      Caption = #26032#12288#12288#22686
      ImageIndex = 37
      OnClick = RzBut2Click
    end
    object N8: TMenuItem
      Caption = #20445#12288#12288#23384
      ImageIndex = 3
      OnClick = RzBut4Click
    end
    object N5: TMenuItem
      Caption = #32534#12288#12288#36753
      ImageIndex = 0
      OnClick = RzBut3Click
    end
    object N4: TMenuItem
      Caption = #21024#12288#12288#38500
      ImageIndex = 51
      OnClick = RzBut5Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object MenuItem1: TMenuItem
      Caption = #21382#21490#24037#31243
      ImageIndex = 41
      OnClick = MenuItem1Click
    end
    object N1: TMenuItem
      Caption = #24674#22797#24037#31243
      Enabled = False
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object MenuItem2: TMenuItem
      Caption = #24443#24213#21024#38500#24037#31243
      Enabled = False
      ImageIndex = 51
      OnClick = MenuItem2Click
    end
  end
end
