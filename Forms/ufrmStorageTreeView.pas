unit ufrmStorageTreeView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls,UtilsTree, RzButton,
  RzPanel, Vcl.ExtCtrls;

type
  TfrmStorageTreeView = class(TForm)
    tv: TTreeView;
    RzToolbar5: TRzToolbar;
    RzSpacer27: TRzSpacer;
    RzBut1: TRzToolButton;
    RzSpacer28: TRzSpacer;
    RzBut2: TRzToolButton;
    RzSpacer29: TRzSpacer;
    RzBut3: TRzToolButton;
    RzSpacer7: TRzSpacer;
    procedure FormActivate(Sender: TObject);
    procedure tvDblClick(Sender: TObject);
    procedure tvClick(Sender: TObject);
    procedure RzBut1Click(Sender: TObject);
    procedure RzBut3Click(Sender: TObject);
    procedure tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
  private
    { Private declarations }
    g_TreeView : TNewUtilsTree;
    procedure TreeAddName(Tv : TTreeView); //树增加名称
    procedure UpDateIndex(lpNode : TTreeNode);
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;

  public
    { Public declarations }
    dwTableName : string;
    dwStorageName:string;
    dwTreeName : string;
    dwParent : Integer;
    dwNodeId : Integer;
    dwTreeCode : string;
  end;

var
  frmStorageTreeView: TfrmStorageTreeView;

implementation

uses
    uDataModule,FillThrdTree,global,ufunctions;

{$R *.dfm}

procedure TfrmStorageTreeView.FormActivate(Sender: TObject);
begin
  dwStorageName := '';
  g_TreeView := TNewUtilsTree.Create(Self.tv,
                                     DM.ADOconn,
                                     dwTableName,
                                     dwTreeName);
  g_TreeView.GetTreeTable( dwParent );
end;

procedure TfrmStorageTreeView.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  szCode : string;

begin
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin
      if Node.Level = 0 then
      begin
        MessageBox(handle, PChar('不可以在顶级节点新建?'),'提示',
               MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2)
      end else
      begin
        szCode := 'FS-' + GetRowCode( g_Table_Company_StorageTree ,'Code','100007',170000);
        g_TreeView.AddChildNode(szCode,'',Date,m_OutText);
      end;

    end;

  end;

end;

procedure TfrmStorageTreeView.RzBut1Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  szColName  : string;
begin
  inherited;
  if sender  = Self.RzBut1 then
  begin
    //添加
    TreeAddName(Self.tv);
  end else
  if Sender = Self.RzBut2 then
  begin
    //编辑
    Node := Self.tv.Selected;
    if Node.Level <= 1 then
    begin
      MessageBox(handle, PChar('不可以编辑顶级节点?'),'提示',
             MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2)
    end else
    begin
      Node.EditText;
    end;

  end;

end;

procedure TfrmStorageTreeView.RzBut3Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  szColName  : string;
  szTableList : array[0..4] of TTableList;
  szData : PNodeData;
  szParent : Integer;

begin
  inherited;
//删除
  Node := Self.Tv.Selected;
  if Assigned(Node)  then
  begin
    if Node.Level <= 1 then
    begin
      MessageBox(handle, '顶级节点不可以删除？', '提示', MB_ICONQUESTION + MB_YESNO)
    end else
    begin
      if MessageBox(handle, '是否删除节点？', '提示', MB_ICONQUESTION + MB_YESNO) = IDYES then
      begin
      //  Self.cxShellTreeView1.Root.CustomPath := dwCurrentPath;
        if Assigned(Node) then
        begin
          szData := PNodeData(Node.Data);
          szParent := szData.Index;
        end;
      //  GetTreeIndexList(Node,szCodeList);
        DM.getTreeCode(g_Table_Company_StorageTree, szParent,szCodeList); //获取要删除的id表
        szTableList[0].ATableName := g_Table_Company_StorageBillList;
        szTableList[0].ADirectory := '';

        szTableList[1].ATableName := g_Table_Company_StorageDetailed;
        szTableList[1].ADirectory := '';

        szTableList[2].ATableName := g_Table_Company_Storage_ContractList;
        szTableList[2].ADirectory := '';

        szTableList[3].ATableName := g_Table_Company_Storage_ContractDetailed;
        szTableList[3].ADirectory := '';

        szTableList[4].ATableName := g_Table_Company_Storage_GroupUnitPrice;
        szTableList[4].ADirectory := '';

        DeleteTableFile(szTableList,szCodeList);
        DM.InDeleteData(g_Table_Company_StorageTree,szCodeList);
        g_TreeView.DeleteTree(Node);

      end;

    end;

  end;
end;


procedure TfrmStorageTreeView.tvClick(Sender: TObject);
var
  Node : TTreeNode;
  PData: PNodeData;
begin
  Node := (Sender AS TTreeView).Selected;
  if Assigned(Node) then
  begin
    if Node.Level >= 2 then
    begin
      PData := PNodeData(Node.Data);
      if Assigned(PData) then
      begin
        dwStorageName := Node.Text;
        dwNodeId      := pData^.Index;
        dwTreeCode    := pData^.Code;
      end;
    end;
  end;
end;

procedure TfrmStorageTreeView.tvDblClick(Sender: TObject);
var
  Node : TTreeNode;
  PData: PNodeData;
begin
  Node := (Sender AS TTreeView).Selected;
  if Assigned(Node) then
  begin
    if Node.Level >= 2 then
    begin
      PData := PNodeData(Node.Data);
      if Assigned(PData) then
      begin
        dwStorageName := Node.Text;
        dwNodeId      := pData^.Index;
        dwTreeCode    := pData^.Code;
        Close;
      end;
    end;
  end;
end;

procedure TfrmStorageTreeView.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  if lpNode <> nil then
  begin
    S := 'UPDATE ' + g_Table_Company_StorageTree + ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      ExecSQL;
    end;
  end;
end;

function TfrmStorageTreeView.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;

begin
  if s = m_OutText then
  begin
    treeCldnode(Node,szCodeList);
    g_TreeView.DeleteTree(Node);
  end else
  begin
    g_TreeView.ModifyNodeSignName(s,Node);
    UpDateIndex(Node);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
    NextNode   := Tv.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(NextNode);
  end;
end;


procedure TfrmStorageTreeView.tvEdited(Sender: TObject; Node: TTreeNode;
  var S: string);
begin
  IstreeEdit(s,Node);
end;

end.
