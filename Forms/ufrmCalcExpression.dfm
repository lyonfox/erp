object frmCalcExpression: TfrmCalcExpression
  AlignWithMargins = True
  Left = 0
  Top = 0
  AlphaBlend = True
  Caption = #24037#31243#37327#34920#36798#24335
  ClientHeight = 447
  ClientWidth = 843
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 428
    Width = 843
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 200
      end
      item
        Width = 50
      end>
  end
  object CalcGrid: TcxGrid
    Left = 0
    Top = 39
    Width = 843
    Height = 389
    Align = alClient
    TabOrder = 1
    ExplicitTop = 43
    object tvCalcExpression: TcxGridDBTableView
      PopupMenu = pm
      OnDblClick = tvCalcExpressionDblClick
      Navigator.Buttons.CustomButtons = <>
      OnEditing = tvCalcExpressionEditing
      OnEditKeyDown = tvCalcExpressionEditKeyDown
      DataController.DataSource = ds
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          OnGetText = tvCalcExpressionTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
          Column = tvCalcExpressionColumn5
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsCustomize.DataRowSizing = True
      OptionsCustomize.GroupRowSizing = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.CellMultiSelect = True
      OptionsView.CellAutoHeight = True
      OptionsView.DataRowHeight = 32
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 26
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 13
      OnColumnSizeChanged = tvCalcExpressionColumnSizeChanged
      object tvCalcExpressionColumn9: TcxGridDBColumn
        Caption = #24207#21495
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        OnGetDisplayText = tvCalcExpressionColumn9GetDisplayText
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Sorting = False
        Width = 40
      end
      object tvCalcExpressionColumn1: TcxGridDBColumn
        Caption = #32534#21495
        DataBinding.FieldName = 'numbers'
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Width = 97
      end
      object tvCalcExpressionColumn2: TcxGridDBColumn
        Caption = #35745#31639#37096#20301
        DataBinding.FieldName = 'ItemPosition'
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Width = 126
      end
      object tvCalcExpressionColumn3: TcxGridDBColumn
        Caption = #35268#26684#31867#21035
        DataBinding.FieldName = 'ItemType'
        PropertiesClassName = 'TcxTextEditProperties'
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Width = 87
      end
      object tvCalcExpressionColumn4: TcxGridDBColumn
        Caption = #35745#31639#20844#24335
        DataBinding.FieldName = 'ItemSpec'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnValidate = tvCalcExpressionColumn4PropertiesValidate
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Sorting = False
        Width = 216
      end
      object tvCalcExpressionColumn5: TcxGridDBColumn
        Caption = #25968#37327
        DataBinding.FieldName = 'ItemValue'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.ValueType = vtFloat
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Sorting = False
        Width = 69
      end
      object tvCalcExpressionColumn6: TcxGridDBColumn
        Caption = #21333#20301
        DataBinding.FieldName = 'ItemUnit'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.OnValidate = tvCalcExpressionColumn6PropertiesValidate
        OnGetPropertiesForEdit = tvCalcExpressionColumn6GetPropertiesForEdit
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Sorting = False
        Width = 85
      end
      object tvCalcExpressionColumn7: TcxGridDBColumn
        Caption = #22791#27880
        DataBinding.FieldName = 'remarks'
        PropertiesClassName = 'TcxMemoProperties'
        Options.Filtering = False
        Options.Sorting = False
        Width = 230
      end
      object tvCalcExpressionColumn8: TcxGridDBColumn
        Caption = #22791#27880'1'
        DataBinding.FieldName = 'remarks1'
        Options.Filtering = False
        Options.Sorting = False
        Width = 230
      end
    end
    object Lv: TcxGridLevel
      GridView = tvCalcExpression
    end
  end
  object RzToolbar12: TRzToolbar
    Left = 0
    Top = 10
    Width = 843
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 2
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer100
      RzToolButton75
      RzSpacer101
      RzToolButton1
      RzSpacer109
      RzToolButton76
      RzSpacer102
      RzToolButton77
      RzSpacer103
      RzToolButton78
      RzSpacer104
      RzToolButton79
      RzSpacer108
      RzToolButton80)
    object RzSpacer100: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzToolButton75: TRzToolButton
      Left = 12
      Top = 2
      Width = 65
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 37
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #28155#21152
      OnClick = RzToolButton75Click
    end
    object RzSpacer101: TRzSpacer
      Left = 77
      Top = 2
    end
    object RzToolButton76: TRzToolButton
      Left = 155
      Top = 2
      Width = 67
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 3
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #20445#23384
      OnClick = RzToolButton76Click
    end
    object RzSpacer102: TRzSpacer
      Left = 222
      Top = 2
    end
    object RzToolButton77: TRzToolButton
      Left = 230
      Top = 2
      Width = 65
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 2
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21024#38500
      OnClick = RzToolButton77Click
    end
    object RzSpacer103: TRzSpacer
      Left = 295
      Top = 2
    end
    object RzToolButton79: TRzToolButton
      Left = 381
      Top = 2
      Width = 65
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      DropDownMenu = Print
      ImageIndex = 5
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      ToolStyle = tsDropDown
      Caption = #25253#34920
    end
    object RzSpacer104: TRzSpacer
      Left = 373
      Top = 2
    end
    object RzToolButton80: TRzToolButton
      Left = 454
      Top = 2
      Width = 65
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986
      OnClick = RzToolButton80Click
    end
    object RzSpacer108: TRzSpacer
      Left = 446
      Top = 2
    end
    object RzSpacer109: TRzSpacer
      Left = 147
      Top = 2
    end
    object RzToolButton78: TRzToolButton
      Left = 303
      Top = 2
      Width = 70
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 4
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #34920#26684#35774#32622
      OnClick = RzToolButton78Click
    end
    object RzToolButton1: TRzToolButton
      Left = 85
      Top = 2
      Width = 62
      ImageIndex = 0
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #32534#36753
      OnClick = RzToolButton1Click
    end
  end
  object RzPanel1: TRzPanel
    Left = 0
    Top = 0
    Width = 843
    Height = 10
    Align = alTop
    BorderOuter = fsNone
    TabOrder = 3
  end
  object ds: TDataSource
    DataSet = qry
    Left = 528
    Top = 264
  end
  object qry: TADOQuery
    CacheSize = 1000
    Connection = DM.ADOconn
    AfterOpen = qryAfterOpen
    AfterInsert = qryAfterInsert
    Parameters = <>
    Left = 464
    Top = 264
  end
  object Print: TPopupMenu
    Images = DM.cxImageList1
    Left = 216
    Top = 128
    object Excel1: TMenuItem
      Caption = #36755#20986'Excel'
      OnClick = Excel1Click
    end
    object N2: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
      OnClick = N2Click
    end
  end
  object pm: TPopupMenu
    Images = DM.cxImageList1
    Left = 296
    Top = 135
    object N1: TMenuItem
      Caption = #32534#36753
      ImageIndex = 0
      OnClick = N1Click
    end
    object N3: TMenuItem
      Caption = #20445#23384
      ImageIndex = 3
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #21024#38500
      ImageIndex = 51
      OnClick = N4Click
    end
  end
end
