object frmStyleManage: TfrmStyleManage
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #25171#21360#26426#26679#31649#29702
  ClientHeight = 221
  ClientWidth = 347
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object cxGroupBox3: TcxGroupBox
    Left = 8
    Top = 8
    Caption = #27169#26495#26679#24335
    TabOrder = 0
    Height = 153
    Width = 329
    object cxLabel1: TcxLabel
      Left = 24
      Top = 32
      Caption = #27169#26495#21517#31216#65306
    end
    object cxTextEdit1: TcxTextEdit
      Left = 88
      Top = 31
      TabOrder = 1
      TextHint = #25171#21360#27169#26495#21517#31216
      Width = 185
    end
    object cxLabel4: TcxLabel
      Left = 24
      Top = 72
      Caption = #25991#20214#21517#31216#65306
    end
    object cxButtonEdit1: TcxButtonEdit
      Left = 88
      Top = 71
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      TabOrder = 3
      TextHint = #27169#26495#25991#20214#21517
      Width = 185
    end
    object cxLabel5: TcxLabel
      Left = 21
      Top = 110
      Caption = #27169#26495#31867#22411#65306
    end
    object cxComboBox1: TcxComboBox
      Left = 88
      Top = 109
      Properties.Items.Strings = (
        #26222#36890'(A4)'
        #31080#25454'('#23567#31080')'
        #26631#31614'('#26465#30721')')
      TabOrder = 5
      TextHint = #36873#25321#25171#21360#31080#25454#31867#22411
      Width = 185
    end
  end
  object cxButton2: TcxButton
    Left = 209
    Top = 176
    Width = 89
    Height = 33
    Caption = #36864#20986
    TabOrder = 1
  end
  object cxButton1: TcxButton
    Left = 60
    Top = 176
    Width = 89
    Height = 33
    Caption = #30830#23450
    TabOrder = 2
  end
end
