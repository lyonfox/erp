unit uMaterialStorage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxTextEdit,
  cxDropDownEdit, cxMemo, cxCurrencyEdit, cxContainer, Vcl.Menus, Vcl.ComCtrls,
  dxCore, cxDateUtils, cxMaskEdit, cxCalendar, Vcl.StdCtrls, cxButtons,
  cxGroupBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, RzButton,
  Vcl.ExtCtrls, RzPanel, Data.Win.ADODB,ufrmBaseController, dxBar,
  RzBHints, dxCustomHint, cxHint, cxSpinEdit;

type
  TfrmMaterialStorage = class(TfrmBaseController)
    RzPanel7: TRzPanel;
    Label15: TLabel;
    RzPanel8: TRzPanel;
    RzPanel9: TRzPanel;
    Panel3: TPanel;
    RzBitBtn11: TRzBitBtn;
    RzBitBtn13: TRzBitBtn;
    RzBitBtn21: TRzBitBtn;
    RzBitBtn19: TRzBitBtn;
    cxGrid1: TcxGrid;
    tvStorageGrid: TcxGridDBTableView;
    tvStorageGridColumn2: TcxGridDBColumn;
    tvStorageGridColumn3: TcxGridDBColumn;
    tvStorageGridColumn15: TcxGridDBColumn;
    tvStorageGridColumn4: TcxGridDBColumn;
    tvStorageGridColumn5: TcxGridDBColumn;
    tvStorageGridColumn11: TcxGridDBColumn;
    tvStorageGridColumn7: TcxGridDBColumn;
    tvStorageGridColumn6: TcxGridDBColumn;
    tvStorageGridColumn8: TcxGridDBColumn;
    tvStorageGridColumn9: TcxGridDBColumn;
    tvStorageGridColumn10: TcxGridDBColumn;
    tvStorageGridColumn12: TcxGridDBColumn;
    tvStorageGridColumn14: TcxGridDBColumn;
    tvStorageGridColumn13: TcxGridDBColumn;
    lv: TcxGridLevel;
    cxGroupBox1: TcxGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label10: TLabel;
    Label4: TLabel;
    Label33: TLabel;
    Label5: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    cxButton4: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxTextEdit5: TcxTextEdit;
    qry: TADOQuery;
    ds: TDataSource;
    RzBitBtn1: TRzBitBtn;
    tvStorageGridColumn16: TcxGridDBColumn;
    tvStorageGridColumn17: TcxGridDBColumn;
    tvStorageGridColumn18: TcxGridDBColumn;
    RzBitBtn2: TRzBitBtn;
    RzPanel1: TRzPanel;
    RzPanel2: TRzPanel;
    RzBitBtn58: TRzBitBtn;
    cxComboBox1: TcxComboBox;
    cxButton14: TcxButton;
    PmRepot: TPopupMenu;
    Execl1: TMenuItem;
    N6: TMenuItem;
    tvStorageGridColumn1: TcxGridDBColumn;
    tvStorageGridColumn19: TcxGridDBColumn;
    cxComboBox2: TcxComboBox;
    Label1: TLabel;
    Label6: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit4: TcxTextEdit;
    Label7: TLabel;
    tvStorageGridColumn20: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzBitBtn19Click(Sender: TObject);
    procedure RzBitBtn11Click(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn21Click(Sender: TObject);
    procedure RzBitBtn13Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure tvStorageGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvStorageGridColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvStorageGridEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvStorageGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems2GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvStorageGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure RzBitBtn58Click(Sender: TObject);
    procedure cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxDateEdit2PropertiesCloseUp(Sender: TObject);
    procedure cxComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxDateEdit1PropertiesCloseUp(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure Execl1Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure tvStorageGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems3GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvStorageGridDblClick(Sender: TObject);
    procedure tvStorageGridColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvStorageGridColumn5PropertiesCloseUp(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cxTextEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxComboBox2PropertiesCloseUp(Sender: TObject);
  private
    { Private declarations }
    g_DirMaterialStorage : string;
    procedure GetmeterialData(lpName : string ; lpindex: Byte = 0);
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  public
    { Public declarations }
  end;

var
  frmMaterialStorage: TfrmMaterialStorage;
  g_MaterialStorage : string = 'MaterialStorage';
  g_TableName : string = 'sk_MaterialStorage';

implementation


uses
  ufrmIsViewGrid,ufrmMaterialStoragePost,uDataModule,global;

{$R *.dfm}

procedure TfrmMaterialStorage.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    SetGrid(lpGridView.Columns[i],index,lpSuffix);
  end;
end;

procedure TfrmMaterialStorage.GetmeterialData(lpName : string ; lpindex: Byte = 0);
var
  s : string;

begin
  s := 'Select *from ' + g_TableName + ' Where ' + lpName;
  with Self.qry do
  begin
    Close;
    SQL.Clear;
    case lpindex of
      0:
      begin
        SQL.Text := 'Select *from ' + g_TableName;
        Open;
      end;
      1:
      begin
        SQL.Text := s;
        Open;
      end;
      2:
      begin
        SQL.Text := s + ' cxDate between :t1 and :t2';
        Parameters.ParamByName('t1').Value := Self.cxDateEdit1.Date;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := Self.cxDateEdit2.Date;
        Open;
      end;
    end;

  end;
end;

procedure TfrmMaterialStorage.N6Click(Sender: TObject);
begin
  inherited;
//  BasePrinterLink1.Component := Self.cxGrid1;
//  BasePrinter.Preview(True,nil);
end;

procedure TfrmMaterialStorage.cxButton4Click(Sender: TObject);
var
  DD1 : string;
  DD2 : string;
  DD3 : string;
  DD4 : string;
  DD5 : string;
  DD6 : string;
  DD7 : string;
  s : string;
  szColName : string;

begin
  inherited;
  s := Self.cxTextEdit2.Text;  //供应单位
  szColName := Self.tvStorageGridColumn15.DataBinding.FieldName;
  if (Length(s) <> 0) then DD1 := szColName + ' like "%' + s + '%" and ';

  s := Self.cxTextEdit3.Text;
  szColName := Self.tvStorageGridColumn4.DataBinding.FieldName;
  if (Length(s) <> 0) then DD2 := szColName + ' LIKE "%' + s + '%" and ';

  s := Self.cxTextEdit5.Text;
  szColName := Self.tvStorageGridColumn14.DataBinding.FieldName;
  if (Length(s) <> 0) then DD3 := szColName + ' LIKE "%' + s + '%" and ';

  s := Self.cxComboBox1.Text;
  szColName := self.tvStorageGridColumn5.DataBinding.FieldName;
  if (Length(s) <> 0) then DD4 := szColName + ' LIKE "%' + s + '%" and ';

  s := Self.cxTextEdit4.Text;
  szColName := Self.tvStorageGridColumn3.DataBinding.FieldName;
  if (Length(s) <> 0) then DD5 := szColName + ' LIKE "%' + s + '%" and ';

  s := Self.cxTextEdit1.Text;
  szColName := Self.tvStorageGridColumn20.DataBinding.FieldName;
  if (Length(s) <> 0) then DD6 := szColName + ' LIKE "%' + s + '%" and ';

  s := Self.cxComboBox2.Text;
  szColName := Self.tvStorageGridColumn17.DataBinding.FieldName;
  if (Length(s) <> 0) then DD7 := szColName + ' LIKE "%' + s + '%" and ';

  GetmeterialData(DD1 + DD2 + DD3 + DD4 + DD5 + DD6 + DD7,2);
end;

procedure TfrmMaterialStorage.cxComboBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  s : string;
  szColName : string;
begin
  inherited;
  s := Self.cxComboBox1.Text;
  if (Key = 13) and (Length(s) <> 0) then
  begin
    szColName := Self.tvStorageGridColumn5.DataBinding.FieldName;
    GetmeterialData(szColName + ' LIKE "%' + s + '%"',1);
  end;
end;

procedure TfrmMaterialStorage.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  s : string;
  szColName : string;
begin
  inherited;
  s := Self.cxComboBox1.Text;
  if (Length(s) <> 0) then
  begin
    szColName := Self.tvStorageGridColumn5.DataBinding.FieldName;
    GetmeterialData(szColName + ' LIKE "%' + s + '%"',1);
  end;
end;

procedure TfrmMaterialStorage.cxComboBox2PropertiesCloseUp(Sender: TObject);
var
  s : string;
  szColName : string;
begin
  inherited;
  s := Self.cxComboBox2.Text;
  if (Length(s) <> 0) then
  begin
    szColName := Self.tvStorageGridColumn17.DataBinding.FieldName;
    GetmeterialData(szColName + ' LIKE "%' + s + '%"',1);
  end;
end;

procedure TfrmMaterialStorage.cxDateEdit1PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  GetmeterialData('',2)
end;

procedure TfrmMaterialStorage.cxDateEdit2PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  GetmeterialData('',2);
end;

procedure TfrmMaterialStorage.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  s : string;
  szColName : string;

begin
  inherited;
  s := Self.cxTextEdit1.Text;
  if (Key = 13) and (Length(s) <> 0) then
  begin
    szColName := Self.tvStorageGridColumn20.DataBinding.FieldName;
    GetmeterialData(szColName + ' LIKE "%' + s + '%"',1);
  end;

end;

procedure TfrmMaterialStorage.cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  s : string;
  szColName : string;

begin
  inherited;
  s := Self.cxTextEdit2.Text;  //供应单位
  if (Key = 13) and (Length(s) <> 0) then
  begin
    szColName := Self.tvStorageGridColumn15.DataBinding.FieldName;
    GetmeterialData(szColName + ' like "%' + s + '%"',1);
  end;
end;

procedure TfrmMaterialStorage.cxTextEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  s : string;
  szColName : string;

begin
  inherited;
  s := Self.cxTextEdit3.Text;
  if (Key = 13) and (Length(s) <> 0) then
  begin
    szColName := Self.tvStorageGridColumn4.DataBinding.FieldName;
    GetmeterialData(szColName + ' LIKE "%' + s + '%"',1);
  end;
end;

procedure TfrmMaterialStorage.cxTextEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  s : string;
  szColName : string;

begin
  inherited;
  s := Self.cxTextEdit4.Text;
  if (Key = 13) and (Length(s) <> 0) then
  begin
    szColName := Self.tvStorageGridColumn3.DataBinding.FieldName;
    GetmeterialData(szColName + ' LIKE "%' + s + '%"',1);
  end;

end;

procedure TfrmMaterialStorage.cxTextEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  s : string;
  szColName : string;
begin
  inherited;
  s := self.cxTextEdit5.Text;
  if (Key = 13) and (Length(s) <> 0) then
  begin
    szColName := self.tvStorageGridColumn14.DataBinding.FieldName;
    GetmeterialData(szColName + ' LIKE "%' + s + '%"',1);
  end;
end;

procedure TfrmMaterialStorage.Execl1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.cxGrid1,'总仓库') ;
end;

procedure TfrmMaterialStorage.FormActivate(Sender: TObject);
var
  i : Integer;
  List : TStringList;

begin
  inherited;
  GetmeterialData('');
  with (Self.tvStorageGridColumn5.Properties as TcxComboBoxProperties) do
  begin
    Items.Clear;
    for I := 0 to High(RebarList) do
    begin
      Items.Add(RebarList[i].Model);
    end;
    Self.cxComboBox1.Properties.Items := Items;
    Self.cxComboBox1.ItemIndex:=0;

  end;

  with (Self.tvStorageGridColumn16.Properties as TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add('入库');
    Items.Add('出库');
  end ;

  List := TStringList.Create;
  try
    List.Clear;
    GetCompany(List,0);  //单位
    with (Self.tvStorageGridColumn9.Properties as TcxComboBoxProperties) do
    begin
      Items.Clear;
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    end ;
    List.Clear;
    GetCompany(List,7);  //租赁规格
    with (Self.tvStorageGridColumn17.Properties as TcxComboBoxProperties) do
    begin
      Items.Clear;
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    end ;
    Self.cxComboBox2.Properties.Items := List;
  finally
    List.Free;
  end;
  g_DirMaterialStorage :=  Concat(g_Resources,'\' + g_MaterialStorage);
  SetcxGrid(Self.tvStorageGrid,0, g_MaterialStorage);
end;

procedure TfrmMaterialStorage.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmMaterialStorage.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  inherited;
  Self.tvStorageGrid.DataController.Post;
  with Self.qry do
  begin
    Close;
  end;

end;

procedure TfrmMaterialStorage.FormCreate(Sender: TObject);
begin
  inherited;
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.WindowState := wsMaximized;
end;

procedure TfrmMaterialStorage.RzBitBtn11Click(Sender: TObject);
var
  Child : TfrmMaterialStoragePost;
  szTableName : string;

begin
  with Self.qry do
  begin
    if State = dsInactive then
    begin
       Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)

    end else
    begin

      if Sender = RzBitBtn11 then
      begin
        if RecordCount = 0 then
        begin

        end else
        begin
        //  First;
        //  Insert;
        end;
        Append;
        szTableName := Self.tvStorageGridColumn18.DataBinding.FieldName;
        FieldByName(szTableName).Value := DM.getDataMaxDate(g_TableName);

        szTableName := Self.tvStorageGridColumn2.DataBinding.FieldName;
        FieldByName(szTableName).Value := Date;
        szTableName := Self.tvStorageGridColumn6.DataBinding.FieldName;
        FieldByName(szTableName).Value := 1;
        szTableName := Self.tvStorageGridColumn7.DataBinding.FieldName;
        FieldByName(szTableName).Value := 0;
        szTableName := Self.tvStorageGridColumn8.DataBinding.FieldName;
        FieldByName(szTableName).Value := 0;
        Post;
        Refresh;
        Self.cxGrid1.SetFocus;
        SetGridFocus(Self.tvStorageGrid);
        Self.tvStorageGrid.DataController.Edit;
      end;


    end;    
  end;
  
end;

procedure TfrmMaterialStorage.RzBitBtn13Click(Sender: TObject);
begin
  inherited;
  Self.RzPanel8.Caption := '';
  cxGridDeleteData(Self.tvStorageGrid,g_DirMaterialStorage,g_TableName);
  GetmeterialData('');
end;

procedure TfrmMaterialStorage.RzBitBtn19Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
begin
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName :=Self.Name +  g_MaterialStorage ;
    Child.ShowModal;
    SetcxGrid(Self.tvStorageGrid,0, g_MaterialStorage);
  finally
    Child.Free;
  end;
end;

procedure TfrmMaterialStorage.RzBitBtn1Click(Sender: TObject);
begin
  SaveGrid(Self.qry);
end;

procedure TfrmMaterialStorage.RzBitBtn21Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmMaterialStorage.RzBitBtn2Click(Sender: TObject);
begin
  inherited;
  GetmeterialData('');
end;

procedure TfrmMaterialStorage.RzBitBtn58Click(Sender: TObject);
begin
  inherited;
  with Self.cxGroupBox1 do
  begin
    if Visible then
    begin
      Visible := False;
    end else
    begin
      Visible := True;
      Self.cxTextEdit2.SetFocus;
    end;
  end;
end;

procedure TfrmMaterialStorage.tvStorageGridColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmMaterialStorage.tvStorageGridColumn5PropertiesCloseUp(
  Sender: TObject);
var
  s : string;
  RecordIndex,ColumnIndex , i : Integer;
  szColName : string;
  szIndex : Integer;
begin
  inherited;
  {
  with Self.tvStorageGrid.Controller do
  begin
    RecordIndex := SelectedRows[0].RecordIndex;
    ColumnIndex := FocusedColumn.Index;
  end;
  Self.tvStorageGridColumn5.VisibleCaption :
  s := Self.tvStorageGrid.DataController.Values[RecordIndex,ColumnIndex];
  for I := 0 to High( RebarList ) do
  begin

    if s = RebarList[i].Model then
    begin

      with Self.qry do
      begin
        Edit;
        szColName := Self.tvStorageGridColumn6.DataBinding.FieldName;
        FieldByName(szColName).Value := RebarList[i].Weight;
      //  UpdateBatch(arAll);
      //  Refresh;
        Break;
      end;

    end;

  end;
  }
//  Self.tvStorageGrid.DataController.UpdateData;
//  Self.tvStorageGrid.DataController.UpdateData;
//  }
end;

procedure TfrmMaterialStorage.tvStorageGridColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvStorageGrid,1, g_MaterialStorage);
end;

procedure TfrmMaterialStorage.tvStorageGridDblClick(Sender: TObject);
var
  szNumbers : string;
begin
  inherited;
  with Self.tvStorageGrid.DataController.DataSource.DataSet do
  begin
    if not IsEmpty then
    begin
      szNumbers := FieldByName('Numbers').AsString;
      CreateOpenDir(Handle,Concat(g_dirMaterialStorage,'\' + szNumbers),True);
    end;
  end;
end;


procedure TfrmMaterialStorage.tvStorageGridEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
var
  s : string;
  szEditValue : string;
  i: LongInt;
  f: Double;
  szColName : string;
  DD1 , DD2 : Double;
  szLowerMoney : Currency;
  szCapitalMoney : string;
  RecordIndex , ColumnIndex : Integer;
begin
  inherited;

  if (ssCtrl in Shift) and (Key = 83) or (Key = 13) or (Key = 9) then
  begin
    if AItem.Name = Self.tvStorageGridColumn11.Name then
    begin
      Self.tvStorageGrid.DataController.UpdateData();
      with Self.qry do
      begin
        szColName := Self.tvStorageGridColumn11.DataBinding.FieldName;
        szEditValue := FieldByName(szColName).AsString;
        s := FunExpCalc(VarToStr(szEditValue),-2);
        if TryStrToInt(s,i) or TryStrToFloat(s, f) then
        begin
          Edit;
          szColName := Self.tvStorageGridColumn7.DataBinding.FieldName;
          FieldByName(szColName).Value := StrToFloatDef(s,0);
        end;
      end;
    end else
    if (AItem.Name = Self.tvStorageGridColumn6.Name) or
       (AItem.Name = Self.tvStorageGridColumn7.Name)  then
    begin
      Self.tvStorageGrid.DataController.UpdateData;
      with Self.qry do
      begin
        Edit;
        szColName := Self.tvStorageGridColumn6.DataBinding.FieldName;
        DD1 :=  FieldByName(szColName).AsFloat;
        szColName := Self.tvStorageGridColumn7.DataBinding.FieldName;
        DD2 := FieldByName(szColName).AsFloat;
        if (DD1 <> 0) and (DD2 <> 0) then
        begin
          szColName := Self.tvStorageGridColumn8.DataBinding.FieldName;
          FieldByName(szColName).Value := DD1 * DD2;
        end;
      end;

    end else
    if (AItem.Name = Self.tvStorageGridColumn8.Name) or
       (AItem.Name = Self.tvStorageGridColumn10.Name) then
    begin
      Self.tvStorageGrid.DataController.UpdateData;
      with Self.qry do
      begin
        Edit;
        szColName := Self.tvStorageGridColumn8.DataBinding.FieldName;
        DD1 :=  FieldByName(szColName).AsFloat;
        szColName := Self.tvStorageGridColumn10.DataBinding.FieldName;
        DD2 := FieldByName(szColName).AsFloat;
        if (DD1 <> 0) and (DD2 <> 0) then
        begin
          szColName := Self.tvStorageGridColumn12.DataBinding.FieldName;
          FieldByName(szColName).Value := DD1 * DD2;
        end;

      end;

    end else
    if AItem.Name = Self.tvStorageGridColumn12.Name then
    begin
      Self.tvStorageGrid.DataController.UpdateData;
      with Self.tvStorageGrid.Controller do
      begin
        RecordIndex := SelectedRows[0].RecordIndex;
        ColumnIndex := FocusedColumn.Index;
      end;
      s := Self.tvStorageGrid.DataController.Values[RecordIndex,ColumnIndex];
      if Length(s) <> 0 then
      begin
          with Self.qry do
          begin
            Edit;
            szColName := Self.tvStorageGridColumn19.DataBinding.FieldName;
            FieldByName(szColName).Value := MoneyConvert(StrToCurrDef(s,0));
          end;
      end;
    end else
    if AItem.Name = Self.tvStorageGridColumn19.Name then
    begin
      if Self.tvStorageGrid.Controller.FocusedRow.IsLast then
      begin
        //在最后一行新增
        Self.RzBitBtn11.Click;
      end;

    end else
    if AItem.Name = Self.tvStorageGridColumn5.Name then
    begin
      try
        Self.tvStorageGrid.DataController.UpdateData;
        s := AEdit.EditingValue;

        for I := 0 to High( RebarList ) do
        begin

          if s = RebarList[i].Model then
          begin

            with Self.qry do
            begin
              Edit;
              szColName := Self.tvStorageGridColumn6.DataBinding.FieldName;
              FieldByName(szColName).Value := RebarList[i].Weight;
              Post;
              Refresh;
              Break;
            end;

          end;

        end;

      except
      end;

    end;

  end;

end;

procedure TfrmMaterialStorage.tvStorageGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzBitBtn13.Click;
  end;
end;

procedure TfrmMaterialStorage.tvStorageGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
 szCapital : string;

begin
  inherited;

  try
    with Self.RzPanel1 do
    begin
      if AValue <> null then
      begin
        t := StrToCurr( VarToStr(AValue) );
        szCapital :=  MoneyConvert(t);
        Caption := AText + ' 大写:' + szCapital;
      end else
      begin
        Caption := '';
      end;
      Self.tvStorageGridColumn19.Summary.FooterFormat := szCapital;
    end;
  except
  end;

end;

procedure TfrmMaterialStorage.tvStorageGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems2GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
  Self.RzPanel8.Caption := AText;
end;
procedure TfrmMaterialStorage.tvStorageGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems3GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
  Self.RzPanel2.Caption := AText;
end;

end.
