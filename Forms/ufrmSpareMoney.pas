unit ufrmSpareMoney;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, RzPanel, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxLabel,
  cxTextEdit, cxCurrencyEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons;

type
  TfrmSpareMoney = class(TForm)
    RzPanel1: TRzPanel;
    RzPanel2: TRzPanel;
    cxLabel1: TcxLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxLabel2: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    procedure cxButton2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    IsSpareStatus:Boolean;
    DetType : Integer;
    SignName: string;
    TableName:string;
    ModuleIndex : Integer;

    FieldSignName : string;
    FieldSign_Id : string;
    FielddetDate : string;
    FielddetSumMoney : string;
    FielddetType  : string;
    FileDir : string;
    FildModuleIndex : string;
  end;


var
  frmSpareMoney: TfrmSpareMoney;

implementation

uses
   global,uDataModule;

{$R *.dfm}

procedure TfrmSpareMoney.cxButton1Click(Sender: TObject);
var
  szSpareMoney : Currency;
  s : string;
  szSQLText : string;

begin
  //添加债务
  szSpareMoney := Self.cxCurrencyEdit1.Value;
  case DetType of
    0: s := '收';
    1: s := '支';
    2: s := '清';
  end;

  szSQLText := 'Select * from '+ TableName +
                ' WHERE '+ FieldSignName +'="' + SignName + '" AND ' + FildModuleIndex + '=' + IntToStr(ModuleIndex) ;
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := szSQLText;
    Open;

    if RecordCount <> 0 then
    begin
      Edit;
        FieldByName(FieldSignName).Value    := SignName;
        FieldByName(FieldSign_Id).Value     := 0;
        FieldByName(FielddetDate).Value     := Date;
        FieldByName(FielddetSumMoney).Value := szSpareMoney;
        FieldByName(FielddetType).Value     := s;
      UpdateBatch();
    end else
    begin
      if szSpareMoney > 0 then
      begin
        Append;
          FieldByName(FieldSignName).Value    := SignName;
          FieldByName(FieldSign_Id).Value     := 0;
          FieldByName(FielddetDate).Value     := Date;
          FieldByName(FielddetSumMoney).Value := szSpareMoney;
          FieldByName(FielddetType).Value     := s;
          FieldByName(FildModuleIndex).Value  := ModuleIndex;
        Post;
      end;
    end;
  end;
  IsSpareStatus := True;
  Close;
end;

procedure TfrmSpareMoney.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmSpareMoney.cxButton3Click(Sender: TObject);
begin
  CreateOpenDir(Handle,FileDir,True);
end;

procedure TfrmSpareMoney.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit1.Text := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmSpareMoney.FormActivate(Sender: TObject);
var
  s:string;
  szSignName : string;

begin
  szSignName := '往来单位 - ' ;
  case DetType of
    0:
     begin
       s := '收入金额';
     end;
    1:
     begin
       s := '支出金额';
     end;
  end;
  Self.RzPanel2.Caption := szSignName + SignName + ' -- ' + s;

end;

procedure TfrmSpareMoney.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
  begin
    Self.cxButton2.Click;
  end;
end;

end.
