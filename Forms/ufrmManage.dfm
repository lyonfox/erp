object frmManage: TfrmManage
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = #33829#25910#31649#29702
  ClientHeight = 666
  ClientWidth = 1212
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Left: TPanel
    Left = 0
    Top = 0
    Width = 260
    Height = 666
    Align = alLeft
    BevelOuter = bvSpace
    Caption = 'Left'
    Color = 16250613
    ParentBackground = False
    TabOrder = 0
    object TreeView1: TTreeView
      Left = 1
      Top = 30
      Width = 258
      Height = 635
      Align = alClient
      DragMode = dmAutomatic
      HideSelection = False
      Images = DM.TreeImageList
      Indent = 19
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      OnChange = TreeView1Change
      OnDblClick = TreeView1DblClick
    end
    object RzToolbar3: TRzToolbar
      Left = 1
      Top = 1
      Width = 258
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 1
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer16
        RzToolButton17
        RzSpacer17
        RzToolButton18
        RzSpacer18
        RzToolButton19
        RzSpacer24
        RzToolButton21)
      object RzSpacer16: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzToolButton17: TRzToolButton
        Left = 12
        Top = 2
        Width = 53
        SelectionColorStop = 16119543
        ImageIndex = 37
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #26032#24314
        OnClick = RzToolButton17Click
      end
      object RzSpacer17: TRzSpacer
        Left = 65
        Top = 2
      end
      object RzToolButton18: TRzToolButton
        Left = 73
        Top = 2
        Width = 53
        SelectionColorStop = 16119543
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
        OnClick = RzToolButton17Click
      end
      object RzSpacer18: TRzSpacer
        Left = 126
        Top = 2
      end
      object RzToolButton19: TRzToolButton
        Left = 134
        Top = 2
        Width = 53
        SelectionColorStop = 16119543
        ImageIndex = 51
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton17Click
      end
      object RzSpacer24: TRzSpacer
        Left = 187
        Top = 2
      end
      object RzToolButton21: TRzToolButton
        Left = 195
        Top = 2
        Width = 53
        SelectionColorStop = 16119543
        ImageIndex = 11
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21047#26032
        OnClick = RzToolButton21Click
      end
    end
  end
  object RzPanel19: TRzPanel
    Left = 268
    Top = 0
    Width = 944
    Height = 666
    Align = alClient
    BorderOuter = fsNone
    TabOrder = 1
    object RzPanel3: TRzPanel
      Left = 0
      Top = 0
      Width = 944
      Height = 38
      Align = alTop
      BorderOuter = fsFlat
      BorderSides = [sdRight, sdBottom]
      BorderHighlight = clBtnFace
      BorderShadow = clBtnFace
      Color = clBlack
      FlatColor = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clFuchsia
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnResize = RzPanel3Resize
      object RzPanel2: TRzPanel
        Left = 550
        Top = 0
        Width = 393
        Height = 37
        Align = alClient
        BorderOuter = fsNone
        BorderSides = [sdRight, sdBottom]
        BorderHighlight = clBtnFace
        BorderShadow = clBtnFace
        Caption = #20313#39069
        Color = clBlack
        FlatColor = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object RzPanel15: TRzPanel
          Left = 86
          Top = 0
          Width = 307
          Height = 37
          Align = alClient
          BorderOuter = fsFlat
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #20313#39069':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object RzPanel17: TRzPanel
            Left = 0
            Top = 0
            Width = 306
            Height = 18
            Align = alTop
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #23567#20889': 0'
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clLime
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object RzPanel18: TRzPanel
            Left = 0
            Top = 18
            Width = 306
            Height = 18
            Align = alClient
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #22823#20889':'#38646#20803#25972
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clLime
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object RzPanel16: TRzPanel
          Left = 0
          Top = 0
          Width = 86
          Height = 37
          Align = alLeft
          BorderOuter = fsFlat
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #36134#30446#20313#39069':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object RzPanel5: TRzPanel
        Left = 297
        Top = 0
        Width = 253
        Height = 37
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight, sdBottom]
        BorderHighlight = clBtnFace
        BorderShadow = clBtnFace
        Caption = #25320#27454#24635#39069
        Color = clBlack
        FlatColor = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        object RzPanel11: TRzPanel
          Left = 0
          Top = 0
          Width = 86
          Height = 36
          Align = alLeft
          BorderOuter = fsFlat
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #20132#26131#24635#39069':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object RzPanel12: TRzPanel
          Left = 86
          Top = 0
          Width = 166
          Height = 36
          Align = alClient
          BorderOuter = fsNone
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #25320#27454#24635#39069':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          object RzPanel13: TRzPanel
            Left = 0
            Top = 0
            Width = 166
            Height = 18
            Align = alTop
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #23567#20889': 0'
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object RzPanel14: TRzPanel
            Left = 0
            Top = 18
            Width = 166
            Height = 18
            Align = alClient
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #22823#20889':'#38646#20803#25972
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
      end
      object RzPanel6: TRzPanel
        Left = 0
        Top = 0
        Width = 297
        Height = 37
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight, sdBottom]
        BorderHighlight = clBtnFace
        BorderShadow = clBtnFace
        Color = clBlack
        FlatColor = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        object RzPanel7: TRzPanel
          Left = 86
          Top = 0
          Width = 210
          Height = 36
          Align = alClient
          Alignment = taRightJustify
          BorderOuter = fsNone
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object RzPanel9: TRzPanel
            Left = 0
            Top = 18
            Width = 210
            Height = 18
            Align = alTop
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #22823#20889': '#38646#20803#25972
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clLime
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object RzPanel10: TRzPanel
            Left = 0
            Top = 0
            Width = 210
            Height = 18
            Align = alTop
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #23567#20889': 0'
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clLime
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object RzPanel8: TRzPanel
          Left = 0
          Top = 0
          Width = 86
          Height = 36
          Align = alLeft
          BorderOuter = fsFlat
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #21512#21516#24635#20215':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object pcSys: TRzPageControl
      Left = 0
      Top = 38
      Width = 944
      Height = 628
      Hint = ''
      ActivePage = TabSheet1
      Align = alClient
      BackgroundColor = 16250613
      BoldCurrentTab = True
      ButtonColor = clActiveCaption
      Color = 16119543
      UseColoredTabs = True
      FlatColor = 10263441
      ImageMargin = 10
      HotTrackStyle = htsText
      ParentBackgroundColor = False
      ParentColor = False
      ShowShadow = False
      TabOverlap = -6
      TabColors.HighlightBar = 1350640
      TabColors.Shadow = clSkyBlue
      TabHeight = 28
      TabIndex = 0
      TabOrder = 1
      TabStyle = tsSquareCorners
      TabWidth = 80
      OnTabClick = pcSysTabClick
      FixedDimension = 28
      object TabSheet1: TRzTabSheet
        Color = 16119543
        Caption = #21512#21516#24635#20215
        object Bevel1: TBevel
          Left = 0
          Top = 318
          Width = 942
          Height = 10
          Align = alBottom
          Shape = bsSpacer
          ExplicitTop = 249
        end
        object RzToolbar1: TRzToolbar
          Left = 0
          Top = 0
          Width = 942
          Height = 29
          AutoStyle = False
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsNone
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 0
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer1
            RzToolButton1
            RzSpacer2
            RzToolButton22
            RzSpacer25
            RzToolButton2
            RzSpacer3
            RzToolButton3
            RzSpacer4
            RzToolButton4
            RzSpacer7
            RzToolButton24
            RzSpacer27
            RzToolButton5
            RzSpacer19
            cxLabel1
            cxDateEdit1
            RzSpacer20
            cxLabel2
            cxDateEdit2
            RzSpacer21
            RzToolButton15
            RzSpacer11
            RzToolButton6)
          object RzSpacer1: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton1: TRzToolButton
            Left = 12
            Top = 2
            Width = 61
            SelectionColorStop = 16119543
            ImageIndex = 37
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #26032#22686
            Enabled = False
            OnClick = RzToolButton1Click
          end
          object RzSpacer2: TRzSpacer
            Left = 73
            Top = 2
          end
          object RzToolButton2: TRzToolButton
            Left = 151
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
            Enabled = False
            OnClick = RzToolButton2Click
          end
          object RzSpacer3: TRzSpacer
            Left = 221
            Top = 2
          end
          object RzToolButton3: TRzToolButton
            Left = 229
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            ImageIndex = 51
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
            Enabled = False
            OnClick = RzToolButton1Click
          end
          object RzSpacer4: TRzSpacer
            Left = 299
            Top = 2
          end
          object RzToolButton4: TRzToolButton
            Left = 307
            Top = 2
            Width = 80
            SelectionColorStop = 16119543
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
            Enabled = False
            OnClick = RzToolButton1Click
          end
          object RzToolButton5: TRzToolButton
            Left = 469
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            DropDownMenu = Print
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
            Enabled = False
          end
          object RzToolButton6: TRzToolButton
            Left = 884
            Top = 2
            Width = 52
            SelectionColorStop = 16119543
            ImageIndex = 8
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #36864#20986
            OnClick = RzToolButton6Click
          end
          object RzSpacer7: TRzSpacer
            Left = 387
            Top = 2
          end
          object RzSpacer19: TRzSpacer
            Left = 539
            Top = 2
          end
          object RzSpacer20: TRzSpacer
            Left = 693
            Top = 2
            Width = 4
          end
          object RzSpacer21: TRzSpacer
            Left = 843
            Top = 2
          end
          object RzSpacer11: TRzSpacer
            Left = 876
            Top = 2
          end
          object RzToolButton15: TRzToolButton
            Left = 851
            Top = 2
            SelectionColorStop = 16119543
            ImageIndex = 10
            Enabled = False
            OnClick = RzToolButton15Click
          end
          object RzSpacer25: TRzSpacer
            Left = 143
            Top = 2
          end
          object RzToolButton22: TRzToolButton
            Left = 81
            Top = 2
            Width = 62
            SelectionColorStop = 16119543
            ImageIndex = 0
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #32534#36753
            Enabled = False
            OnClick = RzToolButton22Click
          end
          object RzSpacer27: TRzSpacer
            Left = 461
            Top = 2
          end
          object RzToolButton24: TRzToolButton
            Left = 395
            Top = 2
            Width = 66
            DropDownMenu = pmFrozen
            ImageIndex = 45
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #20923#32467
            Enabled = False
          end
          object cxLabel1: TcxLabel
            Left = 547
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit1: TcxDateEdit
            Left = 603
            Top = 4
            Enabled = False
            TabOrder = 1
            Width = 90
          end
          object cxLabel2: TcxLabel
            Left = 697
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit2: TcxDateEdit
            Left = 753
            Top = 4
            Enabled = False
            TabOrder = 3
            Width = 90
          end
        end
        object cxGrid2: TcxGrid
          Left = 0
          Top = 29
          Width = 942
          Height = 289
          Align = alClient
          TabOrder = 1
          LevelTabs.Style = 11
          LookAndFeel.Kind = lfFlat
          LookAndFeel.SkinName = 'Office2010Blue'
          ExplicitLeft = 5
          object tvDealGrid: TcxGridDBTableView
            PopupMenu = pm1
            OnKeyDown = tvDealGridKeyDown
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvDealGridEditing
            OnEditKeyDown = tvDealGridEditKeyDown
            DataController.DataSource = DataMaster
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                OnGetText = tvDealGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                Column = tvDealGridColumn15
              end
              item
                Kind = skCount
                Column = tvDealGridColumn17
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.HideSelection = True
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#26242#26080#20132#26131#21512#21516#25968#25454#26174#31034'>'
            OptionsView.DataRowHeight = 23
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 14
            Styles.OnGetContentStyle = tvDealGridStylesGetContentStyle
            OnColumnSizeChanged = tvDealGridColumnSizeChanged
            object tvDealGridColumn1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvDealGridColumn1GetDisplayText
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 35
            end
            object tvDealGridColumn19: TcxGridDBColumn
              Caption = #29366#24577
              DataBinding.FieldName = 'status'
              Width = 40
            end
            object tvDealGridColumn2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              Options.Filtering = False
              Options.Sorting = False
              Width = 75
            end
            object tvDealGridColumn3: TcxGridDBColumn
              Caption = #24405#20837#26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Width = 115
            end
            object tvDealGridColumn4: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 155
            end
            object tvDealGridColumn5: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 174
            end
            object tvDealGridColumn18: TcxGridDBColumn
              Caption = #36153#29992#31185#30446
              DataBinding.FieldName = 'CategoryName'
              RepositoryItem = DM.CostComboBox
              Width = 92
            end
            object tvDealGridColumn6: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 154
            end
            object tvDealGridColumn7: TcxGridDBColumn
              Caption = #39033#30446#37096#36127#36131#20154
              DataBinding.FieldName = 'PersonCharge'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 155
            end
            object tvDealGridColumn8: TcxGridDBColumn
              Caption = #24635#21253#21333#20301
              DataBinding.FieldName = 'TheContractor'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 114
            end
            object tvDealGridColumn9: TcxGridDBColumn
              Caption = #24314#35774#21333#20301
              DataBinding.FieldName = 'BuildCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 144
            end
            object tvDealGridColumn10: TcxGridDBColumn
              Caption = #24320#22987#26085#26399
              DataBinding.FieldName = 'StartDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Width = 81
            end
            object tvDealGridColumn11: TcxGridDBColumn
              Caption = #32467#26463#26085#26399
              DataBinding.FieldName = 'EndDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Width = 86
            end
            object tvDealGridColumn13: TcxGridDBColumn
              Caption = #25968#37327
              DataBinding.FieldName = 'cValue'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.ValueType = vtFloat
              Properties.OnValidate = tvDealGridColumn13PropertiesValidate
              Width = 80
            end
            object tvDealGridColumn12: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'UnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.OnValidate = tvDealGridColumn12PropertiesValidate
              Width = 80
            end
            object tvDealGridColumn14: TcxGridDBColumn
              Caption = #21333#20301
              DataBinding.FieldName = 'cCompany'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              Width = 60
            end
            object tvDealGridColumn15: TcxGridDBColumn
              Caption = #36896#20215#37329#39069
              DataBinding.FieldName = 'totalsum'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.OnValidate = tvDealGridColumn15PropertiesValidate
              Styles.Content = DM.SumMoney
              Width = 165
            end
            object tvDealGridColumn17: TcxGridDBColumn
              Caption = #22823#20889#37329#39069
              DataBinding.FieldName = 'largeMoney'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 246
            end
            object tvDealGridColumn16: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              PropertiesClassName = 'TcxMemoProperties'
              Width = 320
            end
          end
          object Level1: TcxGridLevel
            GridView = tvDealGrid
          end
        end
        object RzPageControl1: TRzPageControl
          Left = 0
          Top = 328
          Width = 942
          Height = 267
          Hint = ''
          ActivePage = TabSheet2
          Align = alBottom
          TabIndex = 0
          TabOrder = 2
          FixedDimension = 19
          object TabSheet2: TRzTabSheet
            Color = 16119543
            Caption = #20844#21496#36827#38144#20986#24211#32479#35745
            object cxGrid1: TcxGrid
              Left = 0
              Top = 0
              Width = 938
              Height = 244
              Align = alClient
              TabOrder = 0
              object cxGrid1DBTableView1: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.First.Visible = True
                Navigator.Buttons.PriorPage.Visible = True
                Navigator.Buttons.Prior.Visible = True
                Navigator.Buttons.Next.Visible = True
                Navigator.Buttons.NextPage.Visible = True
                Navigator.Buttons.Last.Visible = True
                Navigator.Buttons.Insert.Visible = True
                Navigator.Buttons.Append.Visible = False
                Navigator.Buttons.Delete.Visible = True
                Navigator.Buttons.Edit.Visible = True
                Navigator.Buttons.Post.Visible = True
                Navigator.Buttons.Cancel.Visible = True
                Navigator.Buttons.Refresh.Visible = True
                Navigator.Buttons.SaveBookmark.Visible = True
                Navigator.Buttons.GotoBookmark.Visible = True
                Navigator.Buttons.Filter.Visible = True
                DataController.DataSource = ds1
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.DragDropText = True
                OptionsBehavior.DragFocusing = dfDragDrop
                OptionsBehavior.FocusCellOnTab = True
                OptionsBehavior.FocusFirstCellOnNewRecord = True
                OptionsBehavior.GoToNextCellOnEnter = True
                OptionsBehavior.NavigatorHints = True
                OptionsBehavior.FocusCellOnCycle = True
                OptionsBehavior.PullFocusing = True
                OptionsCustomize.ColumnMoving = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.DataRowSizing = True
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsSelection.InvertSelect = False
                OptionsSelection.MultiSelect = True
                OptionsSelection.CellMultiSelect = True
                OptionsView.CellAutoHeight = True
                OptionsView.DataRowHeight = 23
                OptionsView.GroupByBox = False
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.HeaderHeight = 23
                OptionsView.Indicator = True
                OptionsView.IndicatorWidth = 16
                object cxGrid1DBTableView1Column1: TcxGridDBColumn
                  Caption = #24207#21495
                  OnGetDisplayText = tvDealGridColumn1GetDisplayText
                  Width = 41
                end
                object cxGrid1DBTableView1Column2: TcxGridDBColumn
                  Caption = #24448#26469#21333#20301
                  DataBinding.FieldName = 'c'
                  Width = 154
                end
                object cxGrid1DBTableView1Column3: TcxGridDBColumn
                  Caption = #37329#39069
                  DataBinding.FieldName = 't'
                  Width = 160
                end
                object cxGrid1DBTableView1Column4: TcxGridDBColumn
                  Caption = #22823#20889
                  DataBinding.FieldName = 'd'
                  Width = 176
                end
              end
              object Level2: TcxGridLevel
                GridView = cxGrid1DBTableView1
              end
            end
          end
        end
      end
      object TabSheet3: TRzTabSheet
        Color = 16250613
        ImageIndex = 0
        Caption = #20132#26131#35760#24405
        object Splitter1: TSplitter
          Left = 0
          Top = 304
          Width = 942
          Height = 10
          Cursor = crVSplit
          Align = alBottom
          Color = 16250613
          ParentColor = False
          ExplicitTop = 432
          ExplicitWidth = 993
        end
        object RzToolbar2: TRzToolbar
          Left = 0
          Top = 0
          Width = 942
          Height = 54
          AutoStyle = False
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsNone
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 0
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer5
            RzToolButton7
            RzSpacer15
            RzToolButton23
            RzSpacer8
            RzToolButton16
            RzSpacer26
            RzToolButton8
            RzSpacer9
            RzToolButton9
            RzSpacer10
            RzToolButton10
            RzToolButton11
            RzSpacer12
            RzToolButton25
            RzSpacer28
            RzToolButton13
            RzSpacer13
            RzToolButton14
            RzSpacer14
            cxLabel3
            cxDateEdit3
            RzSpacer6
            cxLabel4
            cxDateEdit4
            RzSpacer22
            RzToolButton20
            RzSpacer23
            RzToolButton12)
          object RzSpacer5: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton7: TRzToolButton
            Left = 12
            Top = 2
            Width = 85
            SelectionColorStop = 16119543
            ImageIndex = 37
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152#20184#27454
            Enabled = False
            OnClick = RzToolButton7Click
          end
          object RzSpacer8: TRzSpacer
            Left = 167
            Top = 2
          end
          object RzToolButton8: TRzToolButton
            Left = 247
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
            Enabled = False
            OnClick = RzToolButton2Click
          end
          object RzSpacer9: TRzSpacer
            Left = 317
            Top = 2
          end
          object RzToolButton9: TRzToolButton
            Left = 325
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            ImageIndex = 51
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
            Enabled = False
            OnClick = RzToolButton7Click
          end
          object RzSpacer10: TRzSpacer
            Left = 395
            Top = 2
          end
          object RzToolButton10: TRzToolButton
            Left = 403
            Top = 2
            Width = 75
            SelectionColorStop = 16119543
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
            Enabled = False
            OnClick = RzToolButton7Click
          end
          object RzToolButton11: TRzToolButton
            Left = 478
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            DropDownMenu = Print
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
            Enabled = False
          end
          object RzToolButton12: TRzToolButton
            Left = 191
            Top = 27
            Width = 52
            SelectionColorStop = 16119543
            ImageIndex = 8
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #36864#20986
            OnClick = RzToolButton6Click
          end
          object RzSpacer12: TRzSpacer
            Left = 548
            Top = 2
          end
          object RzToolButton13: TRzToolButton
            Left = 630
            Top = 2
            Width = 52
            SelectionColorStop = 16119543
            ImageIndex = 12
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #32467#31639
            Enabled = False
            OnClick = RzToolButton13Click
          end
          object RzSpacer13: TRzSpacer
            Left = 682
            Top = 2
          end
          object RzToolButton14: TRzToolButton
            Left = 690
            Top = 2
            Width = 64
            SelectionColorStop = 16119543
            ImageIndex = 14
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #23545#24080#21333
            OnClick = RzToolButton14Click
          end
          object RzSpacer15: TRzSpacer
            Left = 97
            Top = 2
          end
          object RzToolButton16: TRzToolButton
            Left = 175
            Top = 2
            Width = 64
            SelectionColorStop = 16119543
            ImageIndex = 16
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #26126#32454#21333
            Enabled = False
            OnClick = RzToolButton16Click
          end
          object RzSpacer14: TRzSpacer
            Left = 754
            Top = 2
          end
          object RzSpacer6: TRzSpacer
            Left = 908
            Top = 2
            Width = 4
          end
          object RzSpacer22: TRzSpacer
            Left = 150
            Top = 27
          end
          object RzSpacer23: TRzSpacer
            Left = 183
            Top = 27
          end
          object RzToolButton20: TRzToolButton
            Left = 158
            Top = 27
            SelectionColorStop = 16119543
            ImageIndex = 10
            Enabled = False
            OnClick = RzToolButton20Click
          end
          object RzSpacer26: TRzSpacer
            Left = 239
            Top = 2
          end
          object RzToolButton23: TRzToolButton
            Left = 105
            Top = 2
            Width = 62
            SelectionColorStop = 16119543
            ImageIndex = 0
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #32534#36753
            Enabled = False
            OnClick = RzToolButton23Click
          end
          object RzSpacer28: TRzSpacer
            Left = 622
            Top = 2
          end
          object RzToolButton25: TRzToolButton
            Left = 556
            Top = 2
            Width = 66
            DropDownMenu = pmFrozen
            ImageIndex = 45
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #20923#32467
            Enabled = False
          end
          object cxLabel3: TcxLabel
            Left = 762
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit3: TcxDateEdit
            Left = 818
            Top = 4
            Enabled = False
            TabOrder = 1
            Width = 90
          end
          object cxLabel4: TcxLabel
            Left = 4
            Top = 31
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit4: TcxDateEdit
            Left = 60
            Top = 29
            Enabled = False
            TabOrder = 3
            Width = 90
          end
        end
        object RzPanel1: TRzPanel
          Left = 0
          Top = 314
          Width = 942
          Height = 281
          Align = alBottom
          BorderOuter = fsFlat
          TabOrder = 1
          OnResize = RzPanel1Resize
          object Splitter2: TSplitter
            Left = 508
            Top = 1
            Width = 10
            Height = 279
            Align = alRight
            Color = 16250613
            ParentColor = False
            ExplicitLeft = 488
            ExplicitTop = 0
            ExplicitHeight = 174
          end
          object RzPanel4: TRzPanel
            Left = 1
            Top = 1
            Width = 507
            Height = 279
            Align = alClient
            BorderOuter = fsNone
            TabOrder = 0
            object Grid2: TcxGrid
              Left = 0
              Top = 29
              Width = 507
              Height = 250
              Align = alClient
              TabOrder = 0
              object tvGroup: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = DataGroup
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    OnGetText = tvGroupTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                    Column = tvGroupColumn6
                  end
                  item
                    Kind = skCount
                    Column = tvGroupColumn5
                  end>
                DataController.Summary.SummaryGroups = <>
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.NoDataToDisplayInfoText = '<'#26242#26080#32467#31639#25968#25454#26174#31034'>'
                OptionsView.ColumnAutoWidth = True
                OptionsView.DataRowHeight = 23
                OptionsView.Footer = True
                OptionsView.GroupByBox = False
                OptionsView.HeaderHeight = 23
                OptionsView.Indicator = True
                OptionsView.IndicatorWidth = 14
                Styles.Content = DM.cxStyle6
                object tvGroupColumn1: TcxGridDBColumn
                  Caption = #24207#21495
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.Alignment.Horz = taCenter
                  OnGetDisplayText = tvGroupColumn1GetDisplayText
                  Options.Filtering = False
                  Options.Focusing = False
                  Options.Sorting = False
                  Width = 43
                end
                object tvGroupColumn2: TcxGridDBColumn
                  Caption = #39033#30446#21517#31216
                  DataBinding.FieldName = 'ProjectName'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 72
                end
                object tvGroupColumn3: TcxGridDBColumn
                  Caption = #20184#27454#21333#20301
                  DataBinding.FieldName = 'PaymentType'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 82
                end
                object tvGroupColumn4: TcxGridDBColumn
                  Caption = #20184#27454#20154
                  DataBinding.FieldName = 'Drawee'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 75
                end
                object tvGroupColumn6: TcxGridDBColumn
                  Caption = #37329#39069
                  DataBinding.FieldName = 't'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 62
                end
                object tvGroupColumn5: TcxGridDBColumn
                  Width = 157
                end
              end
              object lv3: TcxGridLevel
                GridView = tvGroup
              end
            end
            object RzToolbar4: TRzToolbar
              Left = 0
              Top = 0
              Width = 507
              Height = 29
              Images = DM.cxImageList1
              BorderInner = fsNone
              BorderOuter = fsGroove
              BorderSides = [sdTop]
              BorderWidth = 0
              GradientColorStyle = gcsCustom
              TabOrder = 1
              VisualStyle = vsGradient
              ToolbarControls = (
                RzSpacer29
                RzToolButton26)
              object RzSpacer29: TRzSpacer
                Left = 4
                Top = 2
              end
              object RzToolButton26: TRzToolButton
                Left = 12
                Top = 2
                Width = 80
                DropDownMenu = PopupMenu1
                ImageIndex = 5
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                ToolStyle = tsDropDown
                Caption = #25171#21360
              end
            end
          end
          object RzPanel20: TRzPanel
            Left = 518
            Top = 1
            Width = 423
            Height = 279
            Align = alRight
            BorderOuter = fsNone
            TabOrder = 1
            object Grid3: TcxGrid
              Left = 0
              Top = 0
              Width = 423
              Height = 279
              Align = alClient
              TabOrder = 0
              object tvDebt: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = DataDebt
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Kind = skSum
                    OnGetText = tvDebtTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                    Column = cxGridDBColumn2
                  end
                  item
                    Kind = skCount
                    Column = cxGridDBColumn6
                  end>
                DataController.Summary.SummaryGroups = <>
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.NoDataToDisplayInfoText = '<'#26242#26080#32467#31639#25968#25454#26174#31034'>'
                OptionsView.ColumnAutoWidth = True
                OptionsView.DataRowHeight = 23
                OptionsView.Footer = True
                OptionsView.GroupByBox = False
                OptionsView.HeaderHeight = 23
                OptionsView.Indicator = True
                OptionsView.IndicatorWidth = 14
                Styles.Content = DM.cxStyle6
                object cxGridDBColumn1: TcxGridDBColumn
                  Caption = #24207#21495
                  OnGetDisplayText = tvGroupColumn1GetDisplayText
                  Options.Filtering = False
                  Options.Focusing = False
                  Options.Sorting = False
                  Width = 40
                end
                object tvDebtColumn1: TcxGridDBColumn
                  Caption = #29366#24577
                  DataBinding.FieldName = 'status'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                  Width = 40
                end
                object tvDebtColumn3: TcxGridDBColumn
                  Caption = #24448#26469#21333#20301
                  DataBinding.FieldName = 'ParentName'
                  Width = 100
                end
                object tvDebtColumn2: TcxGridDBColumn
                  Caption = #24037#31243#21517#31216
                  DataBinding.FieldName = 'ProjectName'
                  Width = 100
                end
                object cxGridDBColumn3: TcxGridDBColumn
                  Caption = #20538#21153#31867#22411
                  DataBinding.FieldName = 'MoneyTypes'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 70
                end
                object cxGridDBColumn2: TcxGridDBColumn
                  Caption = #20538#21153#37329#39069
                  DataBinding.FieldName = 'SumMoney'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 61
                end
                object cxGridDBColumn6: TcxGridDBColumn
                  Caption = #22791#27880
                  DataBinding.FieldName = 'remarks'
                  PropertiesClassName = 'TcxMemoProperties'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 131
                end
              end
              object lv4: TcxGridLevel
                GridView = tvDebt
              end
            end
          end
        end
        object Grid1: TcxGrid
          Left = 0
          Top = 54
          Width = 942
          Height = 250
          Align = alClient
          TabOrder = 2
          object tvRecord: TcxGridDBTableView
            PopupMenu = pm2
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvRecordEditing
            OnEditKeyDown = tvRecordEditKeyDown
            DataController.DataSource = DataRecord
            DataController.KeyFieldNames = 'Numbers'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                OnGetText = tvRecordTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                Column = tvRecordColumn16
              end
              item
                Kind = skCount
                Column = tvRecordColumn17
              end
              item
                Kind = skCount
                Column = tvRecordColumn1
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.DataRowHeight = 23
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 14
            Styles.OnGetContentStyle = tvRecordStylesGetContentStyle
            OnColumnSizeChanged = tvRecordColumnSizeChanged
            object tvRecordColumn1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvRecordColumn1GetDisplayText
              Width = 40
            end
            object tvRecordColumn2: TcxGridDBColumn
              Caption = #29366#24577
              DataBinding.FieldName = 'status'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Width = 40
            end
            object tvRecordColumn3: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'Numbers'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 80
            end
            object tvRecordColumn4: TcxGridDBColumn
              Caption = #20132#26131#26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Width = 70
            end
            object tvRecordColumn5: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 100
            end
            object tvRecordColumn6: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              Width = 80
            end
            object tvRecordColumn7: TcxGridDBColumn
              Caption = #39033#30446#21333#20301
              DataBinding.FieldName = 'ProjectDepartment'
              Width = 80
            end
            object tvRecordColumn8: TcxGridDBColumn
              Caption = #39033#30446#36127#36131#20154
              DataBinding.FieldName = 'ProjectManager'
              Width = 80
            end
            object tvRecordColumn9: TcxGridDBColumn
              Caption = #25910#27454#21333#20301
              DataBinding.FieldName = 'CollectMoneyCompany'
              RepositoryItem = DM.CompanyList
              Width = 80
            end
            object tvRecordColumn10: TcxGridDBColumn
              Caption = #20132#26131#26041#24335
              DataBinding.FieldName = 'DealType'
              RepositoryItem = DM.MarketTypeBox
              Width = 80
            end
            object tvRecordColumn11: TcxGridDBColumn
              Caption = #20184#27454#21333#20301
              DataBinding.FieldName = 'PaymentCompany'
              Width = 80
            end
            object tvRecordColumn12: TcxGridDBColumn
              Caption = #25910#27454#20154
              DataBinding.FieldName = 'Payee'
              Width = 80
            end
            object tvRecordColumn13: TcxGridDBColumn
              Caption = #25910#27454#24080#21495
              DataBinding.FieldName = 'PayeeAccountNumber'
              PropertiesClassName = 'TcxSpinEditProperties'
              Width = 80
            end
            object tvRecordColumn14: TcxGridDBColumn
              Caption = #20184#27454#20154
              DataBinding.FieldName = 'Drawee'
              Width = 80
            end
            object tvRecordColumn15: TcxGridDBColumn
              Caption = #20184#27454#24080#21495
              DataBinding.FieldName = 'DraweeAccountNumber'
              PropertiesClassName = 'TcxSpinEditProperties'
              Width = 80
            end
            object tvRecordColumn16: TcxGridDBColumn
              Caption = #32467#31639#37329#39069
              DataBinding.FieldName = 'SettleAccountsMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.OnValidate = tvRecordColumn16PropertiesValidate
              OnCustomDrawCell = tvRecordColumn16CustomDrawCell
              Width = 80
            end
            object tvRecordColumn17: TcxGridDBColumn
              Caption = #22823#20889#37329#39069
              DataBinding.FieldName = 'SettleAccountslarge'
            end
            object tvRecordColumn18: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'remarks'
            end
          end
          object tvView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DataView
            DataController.DetailKeyFieldNames = 'parent'
            DataController.MasterKeyFieldNames = 'Numbers'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            object tvViewColumn1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvViewColumn1GetDisplayText
              Width = 40
            end
            object tvViewColumn2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'Numbers'
              Width = 60
            end
            object tvViewColumn3: TcxGridDBColumn
              Caption = #39033#30446#21517#31216
              DataBinding.FieldName = 'appropriation'
              Width = 60
            end
            object tvViewColumn16: TcxGridDBColumn
              Caption = #36153#29992#31185#30446
              DataBinding.FieldName = 'ProjectName'
              Width = 60
            end
            object tvViewColumn4: TcxGridDBColumn
              Caption = #25968#37327
              DataBinding.FieldName = 'fine'
              Width = 60
            end
            object tvViewColumn5: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'Armourforwood'
              Width = 60
            end
            object tvViewColumn6: TcxGridDBColumn
              Caption = #23454#25320#27454#39033
              DataBinding.FieldName = 'Realmoney'
              Width = 60
            end
            object tvViewColumn7: TcxGridDBColumn
              Caption = #31246#37329
              DataBinding.FieldName = 'tax'
              Width = 60
            end
            object tvViewColumn8: TcxGridDBColumn
              Caption = #27700#30005
              DataBinding.FieldName = 'hydropower'
              Width = 60
            end
            object tvViewColumn9: TcxGridDBColumn
              Caption = #21457#31080
              DataBinding.FieldName = 'invoice'
              Width = 60
            end
            object tvViewColumn10: TcxGridDBColumn
              Caption = #22686#39033
              DataBinding.FieldName = 'increase'
              Width = 60
            end
            object tvViewColumn11: TcxGridDBColumn
              Caption = #23427#39033
              DataBinding.FieldName = 'LtIsa'
              Width = 60
            end
            object tvViewColumn12: TcxGridDBColumn
              Caption = #22870#21169
              DataBinding.FieldName = 'reward'
              Width = 60
            end
            object tvViewColumn13: TcxGridDBColumn
              Caption = #35745#31639#20844#24335
              DataBinding.FieldName = 'DesignFormulas'
              Width = 60
            end
            object tvViewColumn14: TcxGridDBColumn
              Caption = #37329#39069
              DataBinding.FieldName = 'receiptsMoney'
              Width = 60
            end
            object tvViewColumn15: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              Width = 60
            end
          end
          object lv1: TcxGridLevel
            GridView = tvRecord
            object lv2: TcxGridLevel
              GridView = tvView
            end
          end
        end
      end
    end
  end
  object cxSplitter3: TcxSplitter
    Left = 260
    Top = 0
    Width = 8
    Height = 666
    HotZoneClassName = 'TcxXPTaskBarStyle'
    HotZone.SizePercent = 61
    AutoSnap = True
    ResizeUpdate = True
  end
  object DataRecord: TDataSource
    DataSet = ADORecord
    Left = 184
    Top = 376
  end
  object DataMaster: TDataSource
    DataSet = Master
    Left = 104
    Top = 376
  end
  object Print: TPopupMenu
    Images = DM.cxImageList1
    Left = 504
    Top = 192
    object N1: TMenuItem
      Caption = #30452#25509#25171#21360
      ImageIndex = 6
      OnClick = N1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Execl1: TMenuItem
      Caption = #23548#20986'Execl'
      OnClick = Execl1Click
    end
  end
  object DataView: TDataSource
    DataSet = ADOView
    Left = 184
    Top = 496
  end
  object DataGroup: TDataSource
    DataSet = ADOGroup
    Left = 104
    Top = 488
  end
  object BMDThread1: TBMDThread
    UpdateEnabled = False
    Left = 648
    Top = 200
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = MasterAfterOpen
    AfterClose = MasterAfterClose
    AfterInsert = MasterAfterInsert
    Left = 104
    Top = 320
  end
  object ADORecord: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = ADORecordAfterOpen
    AfterClose = ADORecordAfterClose
    AfterInsert = ADORecordAfterInsert
    Left = 184
    Top = 320
  end
  object ADOGroup: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 104
    Top = 440
  end
  object ADOView: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 184
    Top = 440
  end
  object DataDebt: TDataSource
    DataSet = ADODebt
    Left = 352
    Top = 216
  end
  object ADODebt: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 352
    Top = 168
  end
  object pm1: TPopupMenu
    Images = DM.cxImageList1
    Left = 80
    Top = 152
    object N22: TMenuItem
      Caption = #26032#22686'&'
      Enabled = False
      ImageIndex = 37
      OnClick = N22Click
    end
    object N2: TMenuItem
      Caption = #32534#36753'&'
      Enabled = False
      ImageIndex = 0
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #20445#23384'&'
      Enabled = False
      ImageIndex = 3
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #21024#38500'&'
      Enabled = False
      ImageIndex = 51
      OnClick = N4Click
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N12: TMenuItem
      Caption = #21047#26032'&'
      ImageIndex = 11
      OnClick = RzToolButton21Click
    end
    object N13: TMenuItem
      Caption = #38468#20214'&'
      Enabled = False
      ImageIndex = 55
      OnClick = N13Click
    end
    object N16: TMenuItem
      Caption = '-'
    end
    object N17: TMenuItem
      Caption = #20923#32467'&'
      Enabled = False
      ImageIndex = 47
      OnClick = N17Click
    end
    object N18: TMenuItem
      Caption = #35299#20923'&'
      Enabled = False
      ImageIndex = 49
      OnClick = N18Click
    end
  end
  object pm2: TPopupMenu
    Images = DM.cxImageList1
    Left = 112
    Top = 152
    object N23: TMenuItem
      Caption = #26032#22686'&'
      Enabled = False
      ImageIndex = 37
      OnClick = N23Click
    end
    object N6: TMenuItem
      Caption = #32534#36753'&'
      Enabled = False
      ImageIndex = 0
      OnClick = N6Click
    end
    object N7: TMenuItem
      Caption = #20445#23384'&'
      Enabled = False
      ImageIndex = 3
      OnClick = N7Click
    end
    object N8: TMenuItem
      Caption = #21024#38500'&'
      Enabled = False
      ImageIndex = 51
      OnClick = N8Click
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object N10: TMenuItem
      Caption = #32467#31639'&'
      Enabled = False
      ImageIndex = 12
      OnClick = N10Click
    end
    object N14: TMenuItem
      Caption = #21047#26032'&'
      ImageIndex = 11
      OnClick = RzToolButton21Click
    end
    object N15: TMenuItem
      Caption = #38468#20214'&'
      Enabled = False
      ImageIndex = 55
      OnClick = N15Click
    end
    object N19: TMenuItem
      Caption = '-'
    end
    object N20: TMenuItem
      Caption = #20923#32467'&'
      Enabled = False
      ImageIndex = 47
      OnClick = N20Click
    end
    object N21: TMenuItem
      Caption = #35299#20923'&'
      Enabled = False
      ImageIndex = 49
      OnClick = N21Click
    end
  end
  object pmFrozen: TPopupMenu
    Images = DM.cxImageList1
    Left = 736
    Top = 200
    object MenuItem16: TMenuItem
      Caption = #24050#20923#32467'&'
      ImageIndex = 47
      OnClick = MenuItem16Click
    end
    object MenuItem17: TMenuItem
      Caption = #26410#20923#32467'&'
      ImageIndex = 49
      OnClick = MenuItem16Click
    end
  end
  object PopupMenu1: TPopupMenu
    Images = DM.cxImageList1
    Left = 512
    Top = 248
    object Excel1: TMenuItem
      Caption = #36755#20986'Excel'
      OnClick = Excel1Click
    end
    object MenuItem1: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
      OnClick = MenuItem1Click
    end
  end
  object ds1: TDataSource
    DataSet = dxMemData1
    Left = 536
    Top = 528
  end
  object dxMemData1: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 536
    Top = 472
    object dxMemData1c: TStringField
      FieldName = 'c'
      Size = 255
    end
    object dxMemData1t: TCurrencyField
      FieldName = 't'
    end
    object dxMemData1d: TStringField
      FieldName = 'd'
      Size = 255
    end
  end
end
