unit ufrmSystemConfig;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, dxBarBuiltInMenu, cxPC, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, cxContainer, cxEdit, cxGroupBox, cxLabel, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDropDownEdit, dxGDIPlusClasses, Vcl.ExtCtrls,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ComCtrls, cxListView,System.IniFiles,
  cxListBox;

type
  TfrmSystemConfig = class(TForm)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxSpinEdit2: TcxSpinEdit;
    cxSpinEdit3: TcxSpinEdit;
    cxLabel4: TcxLabel;
    cxComboBox1: TcxComboBox;
    cxLabel5: TcxLabel;
    cxComboBox2: TcxComboBox;
    Image1: TImage;
    cxLabel6: TcxLabel;
    cxSpinEdit4: TcxSpinEdit;
    cxLabel7: TcxLabel;
    Image2: TImage;
    cxLabel8: TcxLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxTabSheet3: TcxTabSheet;
    cxGroupBox3: TcxGroupBox;
    cxLabel9: TcxLabel;
    cxComboBox3: TcxComboBox;
    cxGroupBox4: TcxGroupBox;
    cxComboBox4: TcxComboBox;
    cxLabel10: TcxLabel;
    cxGroupBox5: TcxGroupBox;
    cxLabel11: TcxLabel;
    cxComboBox5: TcxComboBox;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSystemConfig: TfrmSystemConfig;

implementation

uses
   Vcl.Printers,uDataModule;

{$R *.dfm}

procedure TfrmSystemConfig.cxButton1Click(Sender: TObject);
var
  lvIniFile : TIniFile;
begin
  lvIniFile := TIniFile.Create('');
  try

  finally
    lvIniFile.Free;
  end;
end;

procedure TfrmSystemConfig.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmSystemConfig.FormCreate(Sender: TObject);
begin
  Self.cxComboBox3.Properties.Items.Add('Ԥ��');
  Self.cxComboBox3.Properties.Items.Assign(Printer.Printers);
  Self.cxComboBox4.Properties.Items.Assign(Printer.Printers);
end;

end.
