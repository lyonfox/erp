object frmMakingsPost: TfrmMakingsPost
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = #21830#21697#24405#20837
  ClientHeight = 312
  ClientWidth = 659
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object RzToolbar1: TRzToolbar
    Left = 0
    Top = 0
    Width = 659
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsGroove
    BorderSides = [sdTop, sdBottom]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 0
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer1
      RzToolButton1
      RzSpacer2
      RzToolButton2
      RzSpacer3
      RzToolButton4)
    object RzSpacer1: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 12
      Top = 2
      Width = 63
      SelectionColorStop = 16119543
      ImageIndex = 37
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #26032#22686
      OnClick = RzToolButton1Click
    end
    object RzSpacer2: TRzSpacer
      Left = 75
      Top = 2
    end
    object RzToolButton2: TRzToolButton
      Left = 83
      Top = 2
      Width = 63
      SelectionColorStop = 16119543
      ImageIndex = 3
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #20445#23384
      OnClick = RzToolButton2Click
    end
    object RzSpacer3: TRzSpacer
      Left = 146
      Top = 2
    end
    object RzToolButton4: TRzToolButton
      Left = 154
      Top = 2
      Width = 63
      SelectionColorStop = 16119543
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986
      OnClick = RzToolButton4Click
    end
  end
  object grp1: TGroupBox
    Left = 8
    Top = 33
    Width = 642
    Height = 270
    Caption = #22522#26412#20449#24687
    TabOrder = 1
    object cxLabel1: TcxLabel
      Left = 24
      Top = 40
      Caption = #21830#21697#21517#31216#65306
    end
    object cxDBTextEdit1: TcxDBTextEdit
      Left = 88
      Top = 39
      DataBinding.DataField = 'MakingCaption'
      DataBinding.DataSource = DataSource
      TabOrder = 0
      Width = 193
    end
    object cxLabel2: TcxLabel
      Left = 354
      Top = 81
      Caption = #26465#12288#12288#30721#65306
    end
    object cxDBTextEdit2: TcxDBTextEdit
      Left = 420
      Top = 80
      DataBinding.DataField = 'BarCode'
      DataBinding.DataSource = DataSource
      TabOrder = 7
      Width = 193
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 122
      Caption = #35268#12288#12288#26684#65306
    end
    object cxDBTextEdit3: TcxDBTextEdit
      Left = 88
      Top = 121
      DataBinding.DataField = 'spec'
      DataBinding.DataSource = DataSource
      TabOrder = 2
      Width = 193
    end
    object cxLabel4: TcxLabel
      Left = 350
      Top = 40
      Caption = #32534#21495'/'#36135#21495#65306
    end
    object cxDBTextEdit4: TcxDBTextEdit
      Left = 420
      Top = 39
      DataBinding.DataField = 'GoodsNo'
      DataBinding.DataSource = DataSource
      TabOrder = 6
      Width = 193
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 81
      Caption = #22411#12288#12288#21495#65306
    end
    object cxDBTextEdit5: TcxDBTextEdit
      Left = 88
      Top = 80
      DataBinding.DataField = 'ModelIndex'
      DataBinding.DataSource = DataSource
      TabOrder = 1
      Width = 193
    end
    object cxLabel6: TcxLabel
      Left = 354
      Top = 122
      Caption = #21697#12288#12288#29260#65306
    end
    object cxDBTextEdit6: TcxDBTextEdit
      Left = 420
      Top = 121
      DataBinding.DataField = 'Brand'
      DataBinding.DataSource = DataSource
      TabOrder = 3
      Width = 193
    end
    object cxLabel7: TcxLabel
      Left = 24
      Top = 163
      Caption = #21378#12288#12288#23478#65306
    end
    object cxLabel8: TcxLabel
      Left = 354
      Top = 163
      Caption = #39068#12288#12288#33394#65306
    end
    object cxLabel9: TcxLabel
      Left = 24
      Top = 204
      Caption = #36827#12288#12288#20215#65306
    end
    object cxLabel10: TcxLabel
      Left = 354
      Top = 204
      Caption = #21806#12288#12288#20215#65306
    end
    object cxDBTextEdit7: TcxDBTextEdit
      Left = 88
      Top = 162
      DataBinding.DataField = 'Manufactor'
      DataBinding.DataSource = DataSource
      TabOrder = 4
      Width = 193
    end
    object cxDBTextEdit8: TcxDBTextEdit
      Left = 420
      Top = 162
      DataBinding.DataField = 'NoColour'
      DataBinding.DataSource = DataSource
      TabOrder = 5
      Width = 193
    end
    object cxDBCurrencyEdit2: TcxDBCurrencyEdit
      Left = 420
      Top = 203
      DataBinding.DataField = 'UnitPrice'
      DataBinding.DataSource = DataSource
      TabOrder = 9
      Width = 193
    end
  end
  object cxDBCurrencyEdit1: TcxDBCurrencyEdit
    Left = 95
    Top = 236
    DataBinding.DataField = 'BuyingPrice'
    DataBinding.DataSource = DataSource
    TabOrder = 2
    Width = 193
  end
  object dsMaster: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = dsMasterAfterInsert
    Left = 320
    Top = 152
  end
  object DataSource: TDataSource
    DataSet = dsMaster
    Left = 320
    Top = 208
  end
end
