unit ufrmCalc;

interface

uses
  Windows, Messages,System.StrUtils, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, Vcl.Menus, Vcl.ComCtrls, cxListView, cxLabel, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxMemo, cxListBox;

type
  TfrmCalc = class(TForm)
    cxMemo1: TcxMemo;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxLabel1: TcxLabel;
    cb_decimal: TComboBox;
    cxLabel2: TcxLabel;
    cxListBox1: TcxListBox;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCalc: TfrmCalc;

implementation

uses
   UnitExpCalc,System.Math;

{$R *.dfm}

procedure TfrmCalc.cxButton1Click(Sender: TObject);
var
  szItemPosition : string;
  szItemUnit : string;
  szItemType : string; //规格
  szItemValue: Single;
  szCode   : string;
  szParent : string;

  str, infoStr, signParamStr, showItemStr: string;
  f1: double;
  i, len, decN, signP: integer;
  r : string;
  s : string;


begin
      str := Self.cxMemo1.Text;
      if str <> '' then
      begin
        len := Length(str);
        for i := 1 to len do
        begin
          if ord(str[i]) in [10, 13] then str[i] := ' ';    // 先去掉回车符

          if (ord(str[i]) = 163) and ((i + 1) <= len) then  // 替换全角的括号
          begin
            if ord(str[i + 1]) = 168 then
            begin
              str[i] := '(';
              str[i + 1] := ' ';
            end;

            if ord(str[i + 1]) = 169 then
            begin
              str[i] := ')';
              str[i + 1] := ' ';
            end;
          end;  // if
          if (ord(str[i]) = 163) and ((i + 1) <= len) then  // 替换全角的逗号
            if ord(str[i + 1]) = 172 then
            begin
              str[i] := ',';
              str[i + 1] := ' ';
            end;
        end;   // for
        str :=  StringReplace(str, ' ', '', [rfReplaceAll]);
        // ********************************
        InitSignParam();      // 清除前面自定义的参数，恢复常数 pi, e
        // 表达式中以 @@之后的 表示符号参数标记内容 格式为 'a=1,b=2.1,c=3'
        signP := pos('@@', str);
        if signP > 0 then
        begin
          signParamStr := midStr(str, signP + 2, Length(str) - signP - 1);
          signParamStr := StringReplace(signParamStr, ' ', '', [rfReplaceAll]);
          AddSignParam(signParamStr);
          str := leftStr(str, signP - 1);
        end;

         // ********************************
        if CalcExp(str, f1, infoStr) then
        begin

          case cb_decimal.ItemIndex of  //保留小数
            0: decN := 0;
            1: decN := -1;
            2: decN := -2;
            3: decN := -3;
            4: decN := -4;
            5: decN := -5;
            6: decN := -6;
            7: decN := -7;
            8: decN := -8;
            9: decN := 100;
          else
            decN := 100;
          end;

          if decN <= 0 then
          begin
            r := floatToStr(RoundTo(f1, decN))  //结果
          end
          else
          begin
            r := floatToStr(f1);  //结果
          end;

          if signP > 0 then
             showItemStr := str + '@@' + signParamStr + '  =  ' + r //结果
          else
             showItemStr := str + ' = ' + r;

          Self.cxTextEdit1.Text := showItemStr;
          Self.cxListBox1.Items.Add(showItemStr);
            //计算成功可以写入了
            szItemPosition := Self.cxTextEdit1.Text;
            {
            szItemType     := Self.cxTextEdit2.Text;
            szItemUnit     := Self.cxComboBox2.Text;
            }
        end
        else
        begin

          if signP > 0 then
             showItemStr := str + '@@' + signParamStr + '  =  出错' + infoStr
          else
            showItemStr := str + ' = 出错: ' + infoStr;

          ShowMessage(showItemStr);

        end;
      end
      else
        Application.MessageBox('输入为空，请输入表达式！','提示',MB_ICONWARNING);

end;

end.
