object frmBranch: TfrmBranch
  Left = 0
  Top = 0
  Caption = #25903#27454#31649#29702'['#24223#24323']'
  ClientHeight = 624
  ClientWidth = 1674
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object Panel18: TPanel
    Left = 0
    Top = 0
    Width = 257
    Height = 624
    Align = alLeft
    BevelOuter = bvNone
    Color = 16250613
    Ctl3D = True
    ParentBackground = False
    ParentCtl3D = False
    TabOrder = 0
    object TreeView1: TTreeView
      AlignWithMargins = True
      Left = 3
      Top = 32
      Width = 251
      Height = 203
      Align = alTop
      DragMode = dmAutomatic
      HideSelection = False
      Images = DM.il1
      Indent = 19
      PopupMenu = PopupMenu3
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      OnClick = TreeView1Click
      OnCustomDrawItem = TreeView1CustomDrawItem
      OnDragDrop = TreeView1DragDrop
      OnDragOver = TreeView1DragOver
      OnKeyDown = TreeView1KeyDown
    end
    object RzToolbar3: TRzToolbar
      Left = 0
      Top = 0
      Width = 257
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 1
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer16
        RzToolButton1
        RzSpacer17
        RzToolButton2
        RzSpacer18
        RzToolButton3)
      object RzSpacer16: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzToolButton1: TRzToolButton
        Left = 12
        Top = 2
        Width = 80
        ImageIndex = 1
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #26032#24314#24037#31243
        OnClick = RzToolButton1Click
      end
      object RzSpacer17: TRzSpacer
        Left = 92
        Top = 2
      end
      object RzToolButton2: TRzToolButton
        Left = 100
        Top = 2
        Width = 70
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
        OnClick = RzToolButton1Click
      end
      object RzSpacer18: TRzSpacer
        Left = 170
        Top = 2
      end
      object RzToolButton3: TRzToolButton
        Left = 178
        Top = 2
        Width = 70
        ImageIndex = 2
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton1Click
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 0
      Top = 238
      Width = 257
      Height = 8
      HotZoneClassName = 'TcxMediaPlayer8Style'
      HotZone.SizePercent = 61
      AlignSplitter = salTop
      AutoSnap = True
      ResizeUpdate = True
      Control = TreeView1
    end
    object GridProjectNameGrid: TcxGrid
      Left = 0
      Top = 269
      Width = 257
      Height = 355
      Align = alClient
      TabOrder = 3
      object tvProjectView: TcxGridDBTableView
        OnDblClick = tvProjectViewDblClick
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DataProjectName
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.DataRowHeight = 28
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 23
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 20
        object ProjectCol1: TcxGridDBColumn
          Caption = #24207#21495
          OnGetDisplayText = ProjectCol1GetDisplayText
          HeaderAlignmentHorz = taCenter
          Options.Focusing = False
          Width = 39
        end
        object ProjectCol2: TcxGridDBColumn
          Caption = #24037#31243#21517#31216
          DataBinding.FieldName = 'ProjectName'
          HeaderAlignmentHorz = taCenter
          Width = 145
        end
      end
      object lvProject: TcxGridLevel
        GridView = tvProjectView
      end
    end
    object RzPanel1: TRzPanel
      Left = 0
      Top = 246
      Width = 257
      Height = 23
      Align = alTop
      BorderOuter = fsNone
      Caption = #39033#30446#37096#24037#31243
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
  end
  object TRzPanel
    Left = 265
    Top = 0
    Width = 1409
    Height = 624
    Align = alClient
    BorderOuter = fsNone
    Caption = 'PageClient'
    TabOrder = 1
    VisualStyle = vsClassic
    object RzPanel2: TRzPanel
      Left = 0
      Top = 0
      Width = 1409
      Height = 38
      Align = alTop
      BorderOuter = fsFlat
      BorderSides = [sdRight, sdBottom]
      BorderHighlight = clBtnFace
      BorderShadow = clBtnFace
      Color = clBlack
      FlatColor = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clFuchsia
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object RzPanel4: TRzPanel
        Left = 1004
        Top = 0
        Width = 404
        Height = 37
        Align = alClient
        BorderOuter = fsNone
        BorderSides = [sdRight, sdBottom]
        BorderHighlight = clBtnFace
        BorderShadow = clBtnFace
        Caption = #20313#39069
        Color = clBlack
        FlatColor = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object RzPanel5: TRzPanel
          Left = 105
          Top = 0
          Width = 299
          Height = 37
          Align = alClient
          BorderOuter = fsFlat
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #20313#39069':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object RzPanel6: TRzPanel
            Left = 0
            Top = 0
            Width = 298
            Height = 18
            Align = alTop
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #23567#20889': 0'
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clLime
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object RzPanel7: TRzPanel
            Left = 0
            Top = 18
            Width = 298
            Height = 18
            Align = alClient
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #22823#20889':'#38646#20803#25972
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clLime
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object RzPanel16: TRzPanel
          Left = 0
          Top = 0
          Width = 105
          Height = 37
          Align = alLeft
          BorderOuter = fsFlat
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #36134#30446#20313#39069':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object RzPanel21: TRzPanel
        Left = 502
        Top = 0
        Width = 502
        Height = 37
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight, sdBottom]
        BorderHighlight = clBtnFace
        BorderShadow = clBtnFace
        Caption = #25320#27454#24635#39069
        Color = clBlack
        FlatColor = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        object RzPanel22: TRzPanel
          Left = 0
          Top = 0
          Width = 105
          Height = 36
          Align = alLeft
          BorderOuter = fsFlat
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #20132#26131#24635#39069':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object RzPanel23: TRzPanel
          Left = 105
          Top = 0
          Width = 396
          Height = 36
          Align = alClient
          BorderOuter = fsNone
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #25320#27454#24635#39069':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          object RzPanel24: TRzPanel
            Left = 0
            Top = 0
            Width = 396
            Height = 18
            Align = alTop
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #23567#20889': 0'
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object RzPanel25: TRzPanel
            Left = 0
            Top = 18
            Width = 396
            Height = 18
            Align = alClient
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #22823#20889':'#38646#20803#25972
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
      end
      object RzPanel26: TRzPanel
        Left = 0
        Top = 0
        Width = 502
        Height = 37
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight, sdBottom]
        BorderHighlight = clBtnFace
        BorderShadow = clBtnFace
        Color = clBlack
        FlatColor = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        object RzPanel27: TRzPanel
          Left = 110
          Top = 0
          Width = 391
          Height = 36
          Align = alClient
          Alignment = taRightJustify
          BorderOuter = fsNone
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object RzPanel28: TRzPanel
            Left = 0
            Top = 18
            Width = 391
            Height = 18
            Align = alTop
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #22823#20889': '#38646#20803#25972
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clLime
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object RzPanel29: TRzPanel
            Left = 0
            Top = 0
            Width = 391
            Height = 18
            Align = alTop
            BorderOuter = fsFlat
            BorderSides = [sdRight, sdBottom]
            BorderHighlight = clBtnFace
            BorderShadow = clBtnFace
            Caption = #23567#20889': 0'
            Color = clBlack
            FlatColor = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clLime
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object RzPanel30: TRzPanel
          Left = 0
          Top = 0
          Width = 110
          Height = 36
          Align = alLeft
          BorderOuter = fsFlat
          BorderSides = [sdRight, sdBottom]
          BorderHighlight = clBtnFace
          BorderShadow = clBtnFace
          Caption = #21512#21516#24635#20215':'
          Color = clBlack
          FlatColor = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object RzPageControl3: TRzPageControl
      Left = 0
      Top = 38
      Width = 1409
      Height = 586
      Hint = ''
      ActivePage = TabSheet13
      Align = alClient
      BackgroundColor = 16250613
      BoldCurrentTab = True
      Color = 16119543
      UseColoredTabs = True
      ParentBackgroundColor = False
      ParentColor = False
      SortTabMenu = False
      ShowShadow = False
      TabOverlap = -5
      TabHeight = 28
      TabIndex = 0
      TabOrder = 1
      TabStyle = tsSquareCorners
      TabWidth = 80
      OnTabClick = RzPageControl3TabClick
      FixedDimension = 28
      object TabSheet13: TRzTabSheet
        Color = 16250613
        Caption = #24080#30446#24635#20215
        object RzPageControl4: TRzPageControl
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 1401
          Height = 547
          Hint = ''
          ActivePage = t1
          SaveResources = True
          Align = alClient
          ShowShadow = False
          TabOverlap = -5
          TabHeight = 25
          TabIndex = 0
          TabOrder = 0
          TabStyle = tsSquareCorners
          TabWidth = 100
          OnTabClick = RzPageControl4TabClick
          FixedDimension = 25
          object t1: TRzTabSheet
            Color = 16250613
            Caption = #39033#30446#37096#32771#21220
            object Splitter3: TSplitter
              Left = 0
              Top = 307
              Width = 1399
              Height = 10
              Cursor = crVSplit
              Align = alBottom
              Color = 16250613
              ParentColor = False
              ExplicitTop = 29
            end
            object RzToolbar1: TRzToolbar
              Left = 0
              Top = 0
              Width = 1399
              Height = 29
              AutoStyle = False
              Images = DM.cxImageList1
              BorderInner = fsNone
              BorderOuter = fsNone
              BorderSides = [sdTop]
              BorderWidth = 0
              GradientColorStyle = gcsCustom
              TabOrder = 0
              VisualStyle = vsGradient
              ToolbarControls = (
                RzSpacer2
                RzToolButton4
                RzSpacer3
                RzToolButton5
                RzSpacer4
                RzToolButton6
                RzSpacer22
                RzToolButton21
                RzSpacer6
                RzToolButton15
                RzSpacer7
                RzToolButton19
                RzSpacer14
                cxLabel6
                Date1
                RzSpacer23
                cxLabel8
                Date2
                RzSpacer24
                RzToolButton22
                RzToolButton17
                RzSpacer20
                RzToolButton18
                RzSpacer21
                Lt1
                RzSpacer25)
              object RzSpacer2: TRzSpacer
                Left = 4
                Top = 2
              end
              object RzToolButton4: TRzToolButton
                Left = 12
                Top = 2
                Width = 70
                SelectionColorStop = 16250613
                SelectionFrameColor = clGreen
                ImageIndex = 1
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #28155#21152
                OnClick = RzToolButton4Click
              end
              object RzSpacer3: TRzSpacer
                Left = 82
                Top = 2
              end
              object RzToolButton5: TRzToolButton
                Left = 90
                Top = 2
                Width = 70
                SelectionColorStop = 16250613
                ImageIndex = 0
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #32534#36753
                OnClick = RzToolButton4Click
              end
              object RzSpacer4: TRzSpacer
                Left = 160
                Top = 2
              end
              object RzToolButton6: TRzToolButton
                Left = 168
                Top = 2
                Width = 70
                SelectionColorStop = 16250613
                ImageIndex = 2
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #21024#38500
                OnClick = RzToolButton4Click
              end
              object RzSpacer6: TRzSpacer
                Left = 322
                Top = 2
              end
              object RzToolButton17: TRzToolButton
                Left = 822
                Top = 2
                Width = 70
                SelectionColorStop = 16250613
                DropDownMenu = Print
                ImageIndex = 5
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                ToolStyle = tsDropDown
                Caption = #25253#34920
              end
              object RzSpacer7: TRzSpacer
                Left = 400
                Top = 2
              end
              object RzToolButton18: TRzToolButton
                Left = 900
                Top = 2
                Width = 70
                SelectionColorStop = 16250613
                ImageIndex = 8
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #36864#20986
                OnClick = RzToolButton12Click
              end
              object RzSpacer14: TRzSpacer
                Left = 478
                Top = 2
              end
              object RzToolButton15: TRzToolButton
                Left = 330
                Top = 2
                Width = 70
                SelectionColorStop = 16250613
                ImageIndex = 25
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #32771#21220#34920
                OnClick = RzToolButton15Click
              end
              object RzSpacer20: TRzSpacer
                Left = 892
                Top = 2
              end
              object RzToolButton19: TRzToolButton
                Left = 408
                Top = 2
                Width = 70
                SelectionColorStop = 16250613
                ImageIndex = 27
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #21512#21516
                OnClick = RzToolButton19Click
              end
              object RzSpacer21: TRzSpacer
                Left = 970
                Top = 2
                Width = 99
              end
              object RzSpacer22: TRzSpacer
                Left = 238
                Top = 2
              end
              object RzToolButton21: TRzToolButton
                Left = 246
                Top = 2
                Width = 76
                ImageIndex = 4
                ShowCaption = True
                UseToolbarShowCaption = False
                Caption = #34920#26684#35774#32622
                OnClick = RzToolButton21Click
              end
              object RzSpacer23: TRzSpacer
                Left = 632
                Top = 2
              end
              object RzToolButton22: TRzToolButton
                Left = 794
                Top = 2
                Width = 28
                SelectionColorStop = 16250613
                ImageIndex = 10
                ShowCaption = True
                UseToolbarShowCaption = False
                OnClick = RzToolButton22Click
              end
              object RzSpacer24: TRzSpacer
                Left = 786
                Top = 2
              end
              object RzSpacer25: TRzSpacer
                Left = 1089
                Top = 2
              end
              object Lt1: TcxLabel
                Left = 1069
                Top = 5
                Caption = 't1 '
                Transparent = True
              end
              object cxLabel6: TcxLabel
                Left = 486
                Top = 5
                Caption = #36215#22987#26085#26399':'
                Transparent = True
              end
              object Date1: TcxDateEdit
                Left = 542
                Top = 3
                TabOrder = 2
                Width = 90
              end
              object cxLabel8: TcxLabel
                Left = 640
                Top = 5
                Caption = #32467#26463#26085#26399':'
                Transparent = True
              end
              object Date2: TcxDateEdit
                Left = 696
                Top = 3
                TabOrder = 4
                Width = 90
              end
            end
            object WorkGrid: TcxGrid
              Left = 0
              Top = 29
              Width = 1399
              Height = 278
              Align = alClient
              TabOrder = 1
              object tvWorkView: TcxGridDBTableView
                OnDblClick = tvWorkViewDblClick
                OnKeyDown = tvWorkViewKeyDown
                OnMouseEnter = tvWorkViewMouseEnter
                OnMouseLeave = tvWorkViewMouseLeave
                Navigator.Buttons.CustomButtons = <>
                OnCellClick = tvWorkViewCellClick
                OnEditing = tvWorkViewEditing
                DataController.DataSource = DataWork
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    OnGetText = tvWorkViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                    Column = WorkCol13
                  end>
                DataController.Summary.SummaryGroups = <>
                FilterRow.InfoText = #21333#20987#26597#35810#31579#36873
                FilterRow.Visible = True
                OptionsBehavior.FocusCellOnTab = True
                OptionsBehavior.FocusFirstCellOnNewRecord = True
                OptionsBehavior.GoToNextCellOnEnter = True
                OptionsBehavior.FocusCellOnCycle = True
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsSelection.InvertSelect = False
                OptionsSelection.MultiSelect = True
                OptionsSelection.CellMultiSelect = True
                OptionsView.NoDataToDisplayInfoText = '<'#26242#26080#32771#21220#25968#25454#26174#31034'>'
                OptionsView.DataRowHeight = 28
                OptionsView.Footer = True
                OptionsView.GroupByBox = False
                OptionsView.HeaderHeight = 28
                OptionsView.Indicator = True
                OptionsView.IndicatorWidth = 20
                Styles.OnGetContentStyle = tvWorkViewStylesGetContentStyle
                OnColumnSizeChanged = tvWorkViewColumnSizeChanged
                object WorkCol1: TcxGridDBColumn
                  Caption = #24207#21495
                  OnGetDisplayText = WorkCol1GetDisplayText
                  Options.Filtering = False
                  Options.Focusing = False
                  Options.Sorting = False
                  Width = 35
                end
                object WorkCol15: TcxGridDBColumn
                  Caption = #29366#24577
                  DataBinding.FieldName = 'status'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                  Properties.Alignment = taCenter
                  Properties.MultiLine = True
                  Properties.OnChange = WorkCol15PropertiesChange
                  HeaderAlignmentHorz = taCenter
                  Options.Filtering = False
                  Width = 60
                end
                object WorkCol2: TcxGridDBColumn
                  Caption = #32534#21495
                  DataBinding.FieldName = 'numbers'
                  Options.Filtering = False
                  Width = 110
                end
                object WorkCol3: TcxGridDBColumn
                  Caption = #24405#20837#26085#26399
                  DataBinding.FieldName = 'InputDate'
                  PropertiesClassName = 'TcxDateEditProperties'
                  Width = 90
                end
                object WorkCol4: TcxGridDBColumn
                  Caption = #24037#31243#21517#31216
                  DataBinding.FieldName = 'ProjectName'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 130
                end
                object WorkCol5: TcxGridDBColumn
                  Caption = #31614#21040#21517#31216
                  DataBinding.FieldName = 'SignName'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 130
                end
                object WorkCol6: TcxGridDBColumn
                  Caption = #20844#21496#21517#31216
                  DataBinding.FieldName = 'CompanyName'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 130
                end
                object WorkCol7: TcxGridDBColumn
                  Caption = #24320#22987#26085#26399
                  DataBinding.FieldName = 'StartDate'
                  PropertiesClassName = 'TcxDateEditProperties'
                  Width = 90
                end
                object WorkCol8: TcxGridDBColumn
                  Caption = #32467#26463#26085#26399
                  DataBinding.FieldName = 'EndDate'
                  PropertiesClassName = 'TcxDateEditProperties'
                  Width = 90
                end
                object WorkCol9: TcxGridDBColumn
                  Caption = #21152#29677#24635#25968
                  DataBinding.FieldName = 'SumOvertime'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Focusing = False
                  Width = 75
                end
                object WorkCol10: TcxGridDBColumn
                  Caption = #21152#29677#21333#20215'('#20803')'
                  DataBinding.FieldName = 'PriceOvertime'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Focusing = False
                  Width = 80
                end
                object tvWorkViewColumn1: TcxGridDBColumn
                  Caption = #21152#29677#24635#20215'('#20803')'
                  DataBinding.FieldName = 'OverSumMoney'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Focusing = False
                  Width = 99
                end
                object WorkCol11: TcxGridDBColumn
                  Caption = #21333#20215
                  DataBinding.FieldName = 'WorkPrice'
                  Options.Focusing = False
                  Width = 70
                end
                object WorkCol12: TcxGridDBColumn
                  Caption = #22825#25968
                  DataBinding.FieldName = 'WorkDays'
                  Options.Focusing = False
                  Width = 50
                end
                object WorkCol13: TcxGridDBColumn
                  Caption = #24635#20215
                  DataBinding.FieldName = 'SumMoney'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Styles.Content = DM.SumMoney
                  Width = 110
                end
                object WorkCol14: TcxGridDBColumn
                  Caption = #22791#27880
                  DataBinding.FieldName = 'remarks'
                  Width = 320
                end
              end
              object WorkLv: TcxGridLevel
                GridView = tvWorkView
              end
            end
            object WorkGroup: TcxGrid
              Left = 0
              Top = 317
              Width = 1399
              Height = 200
              Align = alBottom
              TabOrder = 2
              object tvGroupView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = DataWorkGroup
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = cxgrdbclmnGroupViewColumn1
                  end
                  item
                    Kind = skSum
                    Column = tvGroupViewCol5
                  end
                  item
                    Kind = skSum
                    Column = tvGroupViewCol4
                  end>
                DataController.Summary.SummaryGroups = <>
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.InvertSelect = False
                OptionsSelection.MultiSelect = True
                OptionsSelection.CellMultiSelect = True
                OptionsView.DataRowHeight = 28
                OptionsView.Footer = True
                OptionsView.GroupByBox = False
                OptionsView.HeaderHeight = 23
                OptionsView.Indicator = True
                OptionsView.IndicatorWidth = 20
                Styles.Content = DM.cxStyle112
                object tvGroupViewCol1: TcxGridDBColumn
                  Caption = #24207#21495
                  OnGetDisplayText = tvGroupViewCol1GetDisplayText
                  HeaderAlignmentHorz = taCenter
                  Options.Filtering = False
                  Options.Sorting = False
                end
                object tvGroupViewCol2: TcxGridDBColumn
                  Caption = #24037#31243#21517#31216
                  DataBinding.FieldName = 'ProjectName'
                  HeaderAlignmentHorz = taCenter
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 207
                end
                object tvGroupViewCol4: TcxGridDBColumn
                  Caption = #22825#25968'/'#24037#26085'('#24635')'
                  DataBinding.FieldName = 'w'
                  PropertiesClassName = 'TcxSpinEditProperties'
                  Properties.DisplayFormat = '0('#22825')'
                  HeaderAlignmentHorz = taCenter
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 98
                end
                object tvGroupViewCol5: TcxGridDBColumn
                  Caption = #21152#29677'/'#24037#26102'('#24635')'
                  DataBinding.FieldName = 'O'
                  PropertiesClassName = 'TcxSpinEditProperties'
                  Properties.DisplayFormat = '0('#22825')'
                  HeaderAlignmentHorz = taCenter
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 91
                end
                object cxgrdbclmnGroupViewColumn1: TcxGridDBColumn
                  Caption = #24635#39069
                  DataBinding.FieldName = 't'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  HeaderAlignmentHorz = taCenter
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 175
                end
              end
              object WorkGroupLv: TcxGridLevel
                GridView = tvGroupView
              end
            end
          end
          object t2: TRzTabSheet
            Color = 16250613
            Caption = #21512#21516#26448#26009#20379#24212#21830
            object Splitter4: TSplitter
              Left = 0
              Top = 356
              Width = 1399
              Height = 10
              Cursor = crVSplit
              Align = alBottom
              Color = 16250613
              ParentColor = False
              ExplicitLeft = 1
              ExplicitTop = 335
            end
            object Panel3: TPanel
              Left = 0
              Top = 0
              Width = 1399
              Height = 32
              Align = alTop
              BevelOuter = bvNone
              Caption = #21512#21516#26448#26009#20379#24212#21830
              Color = 16250613
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentBackground = False
              ParentFont = False
              TabOrder = 0
              object RzBitBtn9: TRzBitBtn
                Left = 12
                Top = -2
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #28155#21152
                Color = 15791348
                HotTrack = True
                TabOrder = 0
                OnClick = RzBitBtn9Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80909
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8E8E8E8E8E8091010
                  09E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E809101010
                  1009E8E8E8E8E8E8E8E8E8E881ACACACAC81E8E8E8E8E8E8E8E8E80910101010
                  101009E8E8E8E8E8E8E8E881ACACACACACAC81E8E8E8E8E8E8E8E80910100909
                  10101009E8E8E8E8E8E8E881ACAC8181ACACAC81E8E8E8E8E8E8E8091009E8E8
                  0910101009E8E8E8E8E8E881AC81E8E881ACACAC81E8E8E8E8E8E80909E8E8E8
                  E80910101009E8E8E8E8E88181E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8
                  E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8
                  E8E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E809101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E80909E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn10: TRzBitBtn
                Left = 85
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #32534#36753
                Color = 15791348
                HotTrack = True
                TabOrder = 1
                OnClick = RzBitBtn10Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000520B0000520B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E81710101010
                  1010101010E8E8E8E8E8E8AC818181818181818181E8E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  E3E3E3E3E310E8E8E8E8AC81E3E3E3E3E3E3E3E3E381E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  ACACACACAC10E8E8E8E8AC81E3E3E3E3ACACACACAC81E8E8E8E81710D7D7D7D7
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7D7E3E3E3E3E381E8E8E8E81710E3E3E3AC
                  ACACACACAC10E8E8E8E8AC81E3E3E3ACACACACACAC81E8E8E8E81710D7D7D7E3
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7E3E3E3E3E3E381E8E8E8E81710E3E3ACAC
                  10101010101010101010AC81E3E3ACAC818181818181818181811710D7D7E310
                  17101010101010101010AC81D7D7E381AC8181818181818181811710E3AC1717
                  171717101010101010E8AC81E3ACACACACACAC818181818181E81710D7175F5F
                  1717171717101010E8E8AC81D7ACE3E3ACACACACAC818181E8E81710175F5F5F
                  5F5F1717171710E8E8E8AC81ACE3E3E3E3E3ACACACAC81E8E8E817175F5F5F5F
                  5F5F5F5F1710E8E8E8E8ACACE3E3E3E3E3E3E3E3AC81E8E8E8E8E81781D781D7
                  81D781D781D7E8E8E8E8E8AC81D781D781D781D781D7E8E8E8E8E8E881AC81AC
                  81AC81AC81E8E8E8E8E8E8E881AC81AC81AC81AC81E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn11: TRzBitBtn
                Left = 158
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #21024#38500
                Color = 15791348
                HotTrack = True
                TabOrder = 2
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
                  10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn13: TRzBitBtn
                Left = 621
                Top = 4
                Width = 65
                Height = 28
                FrameColor = 7617536
                Caption = 'Excel'
                Color = 15791348
                HotTrack = True
                TabOrder = 3
                OnClick = RzBitBtn13Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000B30B0000B30B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7ACAC
                  ACACD7D7D7D7D75EE8E8E8E881E8ACACACACE8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D709D7D7D75EE8E8E8E881E8E8E8E8E8E881E8E8E881E8E8E8E85ED7ACAC
                  D7D70909D7D7D75EE8E8E8E881E8ACACE8E88181E8E8E881E8E8E8E85ED7D7D7
                  D7090909095ED75EE8E8E8E881E8E8E8E881818181ACE881E8E8E8E85ED7ACAC
                  D7D70909D709D75EE8E8E8E881E8ACACE8E88181E881E881E8E8E8E85ED7D7D7
                  D7D7D709D709D75EE8E8E8E881E8E8E8E8E8E881E881E881E8E8E8E85ED7ACAC
                  ACD7D7D7D709D75EE8E8E8E881E8ACACACE8E8E8E881E881E8E8E8E85ED7D7D7
                  D7D7D7D7D709D75EE8E8E8E881E8E8E8E8E8E8E8E881E881E8E8E8E85ED7D7D7
                  09090909095ED75EE8E8E8E881E8E8E88181818181ACE881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn14: TRzBitBtn
                Left = 692
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #25171#21360
                Color = 15791348
                HotTrack = True
                TabOrder = 4
                OnClick = RzBitBtn14Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000730E0000730E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909
                  09090909090909E8E8E8E8E88181818181818181818181E8E8E8E85E89898989
                  89898989895E5E09E8E8E8E2ACACACACACACACACACE2E281E8E85E5E5E5E5E5E
                  5E5E5E5E5E5E095E09E8E2E2E2E2E2E2E2E2E2E2E2E281E281E85ED789898989
                  8989898989895E0909E8E2E8ACACACACACACACACACACE28181E85ED789898989
                  181289B490895E5E09E8E2E8ACACACACE281ACE281ACE2E281E85ED7D7D7D7D7
                  D7D7D7D7D7D75E5E5E09E2E8E8E8E8E8E8E8E8E8E8E8E2E2E2815ED789898989
                  8989898989895E5E5E09E2E8ACACACACACACACACACACE2E2E281E85E5E5E5E5E
                  5E5E5E5E5E89895E5E09E8E2E2E2E2E2E2E2E2E2E2ACACE2E281E8E85ED7D7D7
                  D7D7D7D7D75E89895E09E8E8E2E8E8E8E8E8E8E8E8E2ACACE281E8E8E85ED7E3
                  E3E3E3E3D75E5E5E09E8E8E8E8E2E8ACACACACACE8E2E2E281E8E8E8E85ED7D7
                  D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85ED7
                  E3E3E3E3E3D75EE8E8E8E8E8E8E8E2E8ACACACACACE8E2E8E8E8E8E8E8E85ED7
                  D7D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85E
                  5E5E5E5E5E5E5E5EE8E8E8E8E8E8E8E2E2E2E2E2E2E2E2E2E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn21: TRzBitBtn
                Left = 771
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #20851#38381
                Color = 15791348
                HotTrack = True
                TabOrder = 5
                OnClick = RzBitBtn21Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000420B0000420B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
                  D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn112: TRzBitBtn
                Left = 231
                Top = 4
                Width = 80
                Height = 28
                FrameColor = 7617536
                Caption = #27719#24635#34920
                Color = 15791348
                HotTrack = True
                TabOrder = 6
                OnClick = RzBitBtn112Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000E30E0000E30E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E89C9C9C
                  9C9C9C9C9C9C9CE8E8E8E8E8E881818181818181818181E8E8E8E8E89CC6C6C6
                  C6C69CC69C9C9C9CE8E8E8E881E2E2E2E2E281E281818181E8E8E8E89CCCC6C6
                  C6C6C69CC69C9C9CE8E8E8E881ACE2E2E2E2E281E2818181E8E8E8E89CC6CCC6
                  C6C6C6C69CC69C9CE8E8E8E881E2ACE2E2E2E2E281E28181E8E8E8E89CCCC6CC
                  C6C6C6C6C69CC69CE8E8E8E881ACE2ACE2E2E2E2E281E281E8E8E8E89CCCCCC6
                  CCC6C6C6C6C69C9CE8E8E8E881ACACE2ACE2E2E2E2E28181E8E8E8E89CCFCCCC
                  C6CCC6C6C6C6C69CE8E8E8E881E3ACACE2ACE2E2E2E2E281E8E8E8E89CCFCFCC
                  CCC6CCC6C6C6C69CE8E8E8E881E3E3ACACE2ACE2E2E2E281E8E8E8E8E89C9C9C
                  9C9C9C9C9C9C9CE8E8E8E8E8E881818181818181818181E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn119: TRzBitBtn
                Left = 317
                Top = 4
                Width = 80
                Height = 28
                FrameColor = 7617536
                Caption = #21512#21516
                Color = 15791348
                HotTrack = True
                TabOrder = 7
                OnClick = RzBitBtn119Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000E30E0000E30E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E86C6C6C
                  6C6C6C6C6C6C6CE8E8E8E8E8E881818181818181818181E8E8E8E8E86C909090
                  90906C906C6C6C6CE8E8E8E881E2E2E2E2E281E281818181E8E8E8E86CB49090
                  9090906C906C6C6CE8E8E8E881ACE2E2E2E2E281E2818181E8E8E8E86C90B490
                  909090906C906C6CE8E8E8E881E2ACE2E2E2E2E281E28181E8E8E8E86CB490B4
                  90909090906C906CE8E8E8E881ACE2ACE2E2E2E2E281E281E8E8E8E86CB4B490
                  B490909090906C6CE8E8E8E881ACACE2ACE2E2E2E2E28181E8E8E8E86CC9B4B4
                  90B490909090906CE8E8E8E881E3ACACE2ACE2E2E2E2E281E8E8E8E86CC9C9B4
                  B490B4909090906CE8E8E8E881E3E3ACACE2ACE2E2E2E281E8E8E8E8E86C6C6C
                  6C6C6C6C6C6C6CE8E8E8E8E8E881818181818181818181E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn122: TRzBitBtn
                Left = 403
                Top = 4
                Width = 72
                Height = 28
                FrameColor = 7617536
                Caption = #26597#35810
                Color = 15791348
                HotTrack = True
                TabOrder = 8
                OnClick = RzBitBtn122Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                  E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                  E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                  E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                  E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                  81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                  7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                  E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                  8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                  88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                  89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                  E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                  E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                  88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                  82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                  AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                  E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                NumGlyphs = 2
              end
            end
            object cxGroupBox5: TcxGroupBox
              Left = 0
              Top = 32
              Align = alTop
              Caption = #26597#35810
              TabOrder = 1
              Visible = False
              Height = 55
              Width = 1399
              object Label22: TLabel
                Left = 24
                Top = 24
                Width = 52
                Height = 14
                Caption = #26597#35810#21517#31216':'
              end
              object Label23: TLabel
                Left = 335
                Top = 25
                Width = 52
                Height = 14
                Caption = #36215#22987#26085#26399':'
              end
              object Label24: TLabel
                Left = 484
                Top = 25
                Width = 52
                Height = 14
                Caption = #32467#26463#26085#26399':'
              end
              object cxTextEdit6: TcxTextEdit
                Left = 82
                Top = 23
                TabOrder = 0
                TextHint = #24037#31243#21517#31216','#20379#36135#21333#20301
                OnKeyDown = cxTextEdit6KeyDown
                Width = 247
              end
              object cxDateEdit7: TcxDateEdit
                Left = 393
                Top = 23
                TabOrder = 1
                Width = 85
              end
              object cxDateEdit8: TcxDateEdit
                Left = 542
                Top = 23
                TabOrder = 2
                Width = 96
              end
              object cxButton7: TcxButton
                Left = 644
                Top = 19
                Width = 64
                Height = 30
                Caption = #26597#35810
                OptionsImage.Glyph.Data = {
                  36040000424D3604000000000000360000002800000010000000100000000100
                  2000000000000004000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000020000000B000000120000000C00000003000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000200000010071334970F276AFF0A193B970000000B000000000000
                  00007B5043B8AB705CFFAB6F5AFFAB705CFFAA6F5BFFAA6E59FFA96F5AFFBE91
                  82FFC9ACA3FF5F617FFF417CB9FF70C7FFFF265198FF00000010000000000000
                  0000AD735FFFFDFBF9FFFBF5F2FFF7F2EEFFF3EDE9FFEFE9E5FFECE5E1FFE6DE
                  DAFF707E9FFF4C83BCFF83CFFFFF5694CEFF142B4D930000000A000000000000
                  0000B07762FFFDFBFAFFF7F3F0FFE2D8D2FFA5816CFF8E5E42FF8C5D41FF7A5E
                  54FF577EA6FF92D4FAFF619CD0FF727F9BFF0000000E00000002000000000000
                  0000B07966FFFBF9F9FFE1D5CEFF936346FFC8A37FFFEFD7B2FFF0DAB8FFC7A6
                  88FF895D43FF6891B2FF849DB9FFCCB0A7FF0000000200000000000000000000
                  0000B37C69FFFAF8F7FFAD8975FFC7A07BFFF7D39CFFF5CD93FFF7D39BFFF9DD
                  B2FFC7A688FF84695DFFE8E2DEFFC29888FF0000000000000000000000000000
                  0000B67F6CFFF9F8F7FF98694CFFF1D4A7FFFAE5C0FFFBEACAFFF7D6A0FFF6D3
                  9BFFF2DBBBFF8F5D42FFF0E9E7FFB27A66FF0000000000000000000000000000
                  0000B98371FFFAF9F8FF9D6E51FFF2D4A5FFFDF6E2FFFDF3DCFFFBEACAFFF5CE
                  92FFF1DAB5FF936245FFF2EDE9FFB47D6AFF0000000000000000000000000000
                  0000BC8877FFFCFCFBFFB99783FFCDA77EFFF9E0B5FFFEF7E5FFFBE5C1FFF6D4
                  9DFFCAA681FFAF8C77FFF5F1EEFFB6806DFF0000000000000000000000000000
                  0000BF8C7AFFFDFDFCFFEDE4DFFFA87A5DFFCEA77FFFEFD2A3FFEFD2A5FFCCA7
                  80FFA17356FFE4DAD4FFFAF6F3FFB98371FF0000000000000000000000000000
                  0000C18F7FFFFEFEFEFFFDFCFBFFEDE4DFFFBE9C87FFAA7E62FFA97D60FFBB98
                  82FFEADFD8FFFBF8F6FFFDF9F8FFBD8774FF0000000000000000000000000000
                  0000C49382FFFFFEFEFFFEFEFDFFFBF6F4FFFAF5F3FFF9F3F0FFF9F3F0FFFAF2
                  F0FFFAF4F0FFFDFBF9FFFDFBF9FFBF8C7BFF0000000000000000000000000000
                  0000C79985FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFFEFC
                  FCFFFEFCFBFFFEFCFAFFFDFCFAFFC28F7FFF0000000000000000000000000000
                  0000C99A89FFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
                  FDFFFEFEFDFFFEFDFDFFFEFDFDFFC49382FF0000000000000000000000000000
                  0000967467BDCA9C8BFFCA9C8BFFC99C8AFFC99B89FFC99B8AFFCA9A88FFC89A
                  88FFC99987FFC79887FFC89886FF927163BD0000000000000000}
                TabOrder = 3
                OnClick = cxButton7Click
              end
            end
            object DBGridEh1: TDBGridEh
              Left = 0
              Top = 87
              Width = 1399
              Height = 269
              Align = alClient
              ColumnDefValues.Layout = tlCenter
              DataGrouping.Footers = <
                item
                  ColumnItems = <
                    item
                    end
                    item
                    end
                    item
                    end>
                  Visible = False
                end>
              DataSource = DataMaterial
              DrawMemoText = True
              DynProps = <>
              EvenRowColor = clWindow
              FixedColor = 16119543
              Flat = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              FooterRowCount = 1
              FooterParams.Color = clYellow
              FooterParams.Font.Charset = DEFAULT_CHARSET
              FooterParams.Font.Color = clRed
              FooterParams.Font.Height = -11
              FooterParams.Font.Name = 'Tahoma'
              FooterParams.Font.Style = []
              FooterParams.ParentFont = False
              FooterParams.VertLines = False
              IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
              IndicatorTitle.ShowDropDownSign = True
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghHotTrack, dghExtendVertLines]
              ParentFont = False
              PopupMenu = PopupMenu1
              ReadOnly = True
              RowDetailPanel.Active = True
              RowDetailPanel.Height = 600
              RowDetailPanel.Color = clBtnFace
              RowHeight = 30
              RowSizingAllowed = True
              SumList.Active = True
              TabOrder = 2
              TitleParams.RowHeight = 8
              TitleParams.RowLines = 1
              OnKeyDown = DBGridEh1KeyDown
              OnRowDetailPanelShow = DBGridEh1RowDetailPanelShow
              Columns = <
                item
                  Alignment = taCenter
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'numbers'
                  Footer.Value = #21512#35745':'
                  Footer.ValueType = fvtStaticText
                  Footers = <>
                  Title.Alignment = taCenter
                  Title.Caption = #32534#21495
                  Title.TitleButton = True
                  Width = 133
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'caption'
                  Footers = <>
                  Title.Caption = #24037#31243#21517#31216
                  Title.TitleButton = True
                  Width = 89
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'content'
                  Footers = <>
                  Title.Caption = #20379#36135#21333#20301
                  Title.TitleButton = True
                  Width = 123
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'PurchaseCompany'
                  Footers = <>
                  Title.Caption = #36141#36135#21333#20301
                  Width = 81
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'pay_time'
                  Footers = <>
                  Title.Caption = #26085#26399
                  Title.TitleButton = True
                  Width = 70
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'End_Time'
                  Footers = <>
                  Title.Caption = #32467#26463#26085#26399
                  Title.TitleButton = True
                  Visible = False
                  Width = 72
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'truck'
                  Footers = <>
                  Title.Caption = #36710
                  Title.TitleButton = True
                  Width = 28
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Price'
                  Footers = <>
                  Title.Caption = #21333#20215
                  Title.TitleButton = True
                  Width = 89
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'num'
                  Footer.FieldName = 'num'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #25968#37327
                  Title.TitleButton = True
                  Width = 63
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Units'
                  Footers = <>
                  Title.Caption = #21333#20301
                  Width = 40
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 't'
                  Footer.DisplayFormat = #65509'#,##.#### '
                  Footer.FieldName = 't'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #24635#20215
                  Title.TitleButton = True
                  Width = 133
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'remarks'
                  Footers = <>
                  Title.Caption = #22791#27880
                  Title.TitleButton = True
                  Width = 278
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object RzPanel8: TRzPanel
                  Left = 0
                  Top = 0
                  Width = 1117
                  Height = 41
                  Align = alTop
                  BorderOuter = fsFlat
                  GradientColorStyle = gcsCustom
                  TabOrder = 0
                  VisualStyle = vsGradient
                  object RzLabel2: TRzLabel
                    Left = 105
                    Top = 12
                    Width = 28
                    Height = 13
                    Caption = #26597#35810':'
                    Color = 16250613
                    ParentColor = False
                    Transparent = True
                    WordWrap = True
                  end
                  object RzEdit7: TRzEdit
                    Left = 139
                    Top = 10
                    Width = 209
                    Height = 21
                    Text = ''
                    TabOrder = 0
                  end
                  object RzBitBtn64: TRzBitBtn
                    Left = 354
                    Top = 7
                    Width = 62
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25628#32034
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 1
                    OnClick = RzBitBtn64Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                      E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                      E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                      E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                      E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                      81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                      7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                      E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                      8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                      88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                      89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                      E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                      E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                      88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                      82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                      AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                      E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn65: TRzBitBtn
                    Left = 422
                    Top = 7
                    Width = 84
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25171#24320#30446#24405
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 2
                    OnClick = RzBitBtn65Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
                      787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
                      CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
                      D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
                      D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
                      D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
                      D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
                      D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
                      A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
                      D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
                      A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
                      E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn66: TRzBitBtn
                    Left = 9
                    Top = 7
                    Height = 28
                    FrameColor = 7617536
                    Caption = #19978#19968#32423
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 3
                    OnClick = RzBitBtn66Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000530B0000530B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E80910101010
                      101010101010101009E8E881ACACACACACACACACACACACAC81E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                end
                object RzFileListBox2: TRzFileListBox
                  Left = 0
                  Top = 41
                  Width = 1117
                  Height = 137
                  Align = alClient
                  FileType = [ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ItemHeight = 18
                  ParentFont = False
                  TabOrder = 1
                  AllowOpen = True
                  FrameVisible = True
                  FramingPreference = fpCustomFraming
                end
              end
            end
            object cxGrid3: TcxGrid
              Left = 0
              Top = 366
              Width = 1399
              Height = 151
              Align = alBottom
              TabOrder = 3
              object cxGrid3DBTableView1: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsView.DataRowHeight = 28
                OptionsView.GroupByBox = False
                OptionsView.HeaderHeight = 23
                OptionsView.Indicator = True
                OptionsView.IndicatorWidth = 20
                object cxGrid3DBTableView1Column1: TcxGridDBColumn
                  Caption = #24207#21495
                  HeaderAlignmentHorz = taCenter
                  Options.Filtering = False
                  Options.Focusing = False
                  Options.Sorting = False
                  Width = 40
                end
                object cxGrid3DBTableView1Column2: TcxGridDBColumn
                  Caption = #24037#31243#21517#31216
                  HeaderAlignmentHorz = taCenter
                  Width = 205
                end
                object cxGrid3DBTableView1Column3: TcxGridDBColumn
                  Caption = #25968#37327
                  HeaderAlignmentHorz = taCenter
                  Width = 84
                end
                object cxGrid3DBTableView1Column4: TcxGridDBColumn
                  Caption = #24635#39069
                  HeaderAlignmentHorz = taCenter
                  Width = 197
                end
              end
              object cxGrid3Level1: TcxGridLevel
                GridView = cxGrid3DBTableView1
              end
            end
          end
          object t3: TRzTabSheet
            Color = 16250613
            Caption = #30780#20379#24212#21333#20301
            object Panel4: TPanel
              Left = 0
              Top = 0
              Width = 1399
              Height = 32
              Align = alTop
              BevelOuter = bvNone
              Caption = #30780#20379#24212#21333#20301
              Color = 16250613
              ParentBackground = False
              TabOrder = 0
              object RzBitBtn8: TRzBitBtn
                Left = 12
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #28155#21152
                Color = 15791348
                HotTrack = True
                TabOrder = 0
                OnClick = RzBitBtn8Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80909
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8E8E8E8E8E8091010
                  09E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E809101010
                  1009E8E8E8E8E8E8E8E8E8E881ACACACAC81E8E8E8E8E8E8E8E8E80910101010
                  101009E8E8E8E8E8E8E8E881ACACACACACAC81E8E8E8E8E8E8E8E80910100909
                  10101009E8E8E8E8E8E8E881ACAC8181ACACAC81E8E8E8E8E8E8E8091009E8E8
                  0910101009E8E8E8E8E8E881AC81E8E881ACACAC81E8E8E8E8E8E80909E8E8E8
                  E80910101009E8E8E8E8E88181E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8
                  E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8
                  E8E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E809101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E80909E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn12: TRzBitBtn
                Left = 85
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #32534#36753
                Color = 15791348
                HotTrack = True
                TabOrder = 1
                OnClick = RzBitBtn8Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000520B0000520B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E81710101010
                  1010101010E8E8E8E8E8E8AC818181818181818181E8E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  E3E3E3E3E310E8E8E8E8AC81E3E3E3E3E3E3E3E3E381E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  ACACACACAC10E8E8E8E8AC81E3E3E3E3ACACACACAC81E8E8E8E81710D7D7D7D7
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7D7E3E3E3E3E381E8E8E8E81710E3E3E3AC
                  ACACACACAC10E8E8E8E8AC81E3E3E3ACACACACACAC81E8E8E8E81710D7D7D7E3
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7E3E3E3E3E3E381E8E8E8E81710E3E3ACAC
                  10101010101010101010AC81E3E3ACAC818181818181818181811710D7D7E310
                  17101010101010101010AC81D7D7E381AC8181818181818181811710E3AC1717
                  171717101010101010E8AC81E3ACACACACACAC818181818181E81710D7175F5F
                  1717171717101010E8E8AC81D7ACE3E3ACACACACAC818181E8E81710175F5F5F
                  5F5F1717171710E8E8E8AC81ACE3E3E3E3E3ACACACAC81E8E8E817175F5F5F5F
                  5F5F5F5F1710E8E8E8E8ACACE3E3E3E3E3E3E3E3AC81E8E8E8E8E81781D781D7
                  81D781D781D7E8E8E8E8E8AC81D781D781D781D781D7E8E8E8E8E8E881AC81AC
                  81AC81AC81E8E8E8E8E8E8E881AC81AC81AC81AC81E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn15: TRzBitBtn
                Left = 158
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #21024#38500
                Color = 15791348
                HotTrack = True
                TabOrder = 2
                OnClick = RzBitBtn15Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
                  10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn19: TRzBitBtn
                Left = 646
                Top = 4
                Width = 65
                Height = 28
                FrameColor = 7617536
                Caption = 'Excel'
                Color = 15791348
                HotTrack = True
                TabOrder = 3
                OnClick = RzBitBtn13Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000B30B0000B30B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7ACAC
                  ACACD7D7D7D7D75EE8E8E8E881E8ACACACACE8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D709D7D7D75EE8E8E8E881E8E8E8E8E8E881E8E8E881E8E8E8E85ED7ACAC
                  D7D70909D7D7D75EE8E8E8E881E8ACACE8E88181E8E8E881E8E8E8E85ED7D7D7
                  D7090909095ED75EE8E8E8E881E8E8E8E881818181ACE881E8E8E8E85ED7ACAC
                  D7D70909D709D75EE8E8E8E881E8ACACE8E88181E881E881E8E8E8E85ED7D7D7
                  D7D7D709D709D75EE8E8E8E881E8E8E8E8E8E881E881E881E8E8E8E85ED7ACAC
                  ACD7D7D7D709D75EE8E8E8E881E8ACACACE8E8E8E881E881E8E8E8E85ED7D7D7
                  D7D7D7D7D709D75EE8E8E8E881E8E8E8E8E8E8E8E881E881E8E8E8E85ED7D7D7
                  09090909095ED75EE8E8E8E881E8E8E88181818181ACE881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn20: TRzBitBtn
                Left = 721
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #25171#21360
                Color = 15791348
                HotTrack = True
                TabOrder = 4
                OnClick = RzBitBtn14Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000730E0000730E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909
                  09090909090909E8E8E8E8E88181818181818181818181E8E8E8E85E89898989
                  89898989895E5E09E8E8E8E2ACACACACACACACACACE2E281E8E85E5E5E5E5E5E
                  5E5E5E5E5E5E095E09E8E2E2E2E2E2E2E2E2E2E2E2E281E281E85ED789898989
                  8989898989895E0909E8E2E8ACACACACACACACACACACE28181E85ED789898989
                  181289B490895E5E09E8E2E8ACACACACE281ACE281ACE2E281E85ED7D7D7D7D7
                  D7D7D7D7D7D75E5E5E09E2E8E8E8E8E8E8E8E8E8E8E8E2E2E2815ED789898989
                  8989898989895E5E5E09E2E8ACACACACACACACACACACE2E2E281E85E5E5E5E5E
                  5E5E5E5E5E89895E5E09E8E2E2E2E2E2E2E2E2E2E2ACACE2E281E8E85ED7D7D7
                  D7D7D7D7D75E89895E09E8E8E2E8E8E8E8E8E8E8E8E2ACACE281E8E8E85ED7E3
                  E3E3E3E3D75E5E5E09E8E8E8E8E2E8ACACACACACE8E2E2E281E8E8E8E85ED7D7
                  D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85ED7
                  E3E3E3E3E3D75EE8E8E8E8E8E8E8E2E8ACACACACACE8E2E8E8E8E8E8E8E85ED7
                  D7D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85E
                  5E5E5E5E5E5E5E5EE8E8E8E8E8E8E8E2E2E2E2E2E2E2E2E2E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn23: TRzBitBtn
                Left = 800
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #20851#38381
                Color = 15791348
                HotTrack = True
                TabOrder = 5
                OnClick = RzBitBtn21Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000420B0000420B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
                  D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn113: TRzBitBtn
                Left = 231
                Top = 4
                Width = 80
                Height = 28
                FrameColor = 7617536
                Caption = #27719#24635#34920
                Color = 15791348
                HotTrack = True
                TabOrder = 6
                OnClick = RzBitBtn113Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000E30E0000E30E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E89C9C9C
                  9C9C9C9C9C9C9CE8E8E8E8E8E881818181818181818181E8E8E8E8E89CC6C6C6
                  C6C69CC69C9C9C9CE8E8E8E881E2E2E2E2E281E281818181E8E8E8E89CCCC6C6
                  C6C6C69CC69C9C9CE8E8E8E881ACE2E2E2E2E281E2818181E8E8E8E89CC6CCC6
                  C6C6C6C69CC69C9CE8E8E8E881E2ACE2E2E2E2E281E28181E8E8E8E89CCCC6CC
                  C6C6C6C6C69CC69CE8E8E8E881ACE2ACE2E2E2E2E281E281E8E8E8E89CCCCCC6
                  CCC6C6C6C6C69C9CE8E8E8E881ACACE2ACE2E2E2E2E28181E8E8E8E89CCFCCCC
                  C6CCC6C6C6C6C69CE8E8E8E881E3ACACE2ACE2E2E2E2E281E8E8E8E89CCFCFCC
                  CCC6CCC6C6C6C69CE8E8E8E881E3E3ACACE2ACE2E2E2E281E8E8E8E8E89C9C9C
                  9C9C9C9C9C9C9CE8E8E8E8E8E881818181818181818181E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn118: TRzBitBtn
                Left = 317
                Top = 4
                Width = 80
                Height = 28
                FrameColor = 7617536
                Caption = #21512#21516
                Color = 15791348
                HotTrack = True
                TabOrder = 7
                OnClick = RzBitBtn118Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000E30E0000E30E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E86C6C6C
                  6C6C6C6C6C6C6CE8E8E8E8E8E881818181818181818181E8E8E8E8E86C909090
                  90906C906C6C6C6CE8E8E8E881E2E2E2E2E281E281818181E8E8E8E86CB49090
                  9090906C906C6C6CE8E8E8E881ACE2E2E2E2E281E2818181E8E8E8E86C90B490
                  909090906C906C6CE8E8E8E881E2ACE2E2E2E2E281E28181E8E8E8E86CB490B4
                  90909090906C906CE8E8E8E881ACE2ACE2E2E2E2E281E281E8E8E8E86CB4B490
                  B490909090906C6CE8E8E8E881ACACE2ACE2E2E2E2E28181E8E8E8E86CC9B4B4
                  90B490909090906CE8E8E8E881E3ACACE2ACE2E2E2E2E281E8E8E8E86CC9C9B4
                  B490B4909090906CE8E8E8E881E3E3ACACE2ACE2E2E2E281E8E8E8E8E86C6C6C
                  6C6C6C6C6C6C6CE8E8E8E8E8E881818181818181818181E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn123: TRzBitBtn
                Left = 403
                Top = 4
                Width = 72
                Height = 28
                FrameColor = 7617536
                Caption = #26597#35810
                Color = 15791348
                HotTrack = True
                TabOrder = 8
                OnClick = RzBitBtn123Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                  E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                  E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                  E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                  E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                  81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                  7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                  E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                  8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                  88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                  89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                  E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                  E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                  88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                  82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                  AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                  E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                NumGlyphs = 2
              end
            end
            object cxGroupBox6: TcxGroupBox
              Left = 0
              Top = 32
              Align = alTop
              Caption = #26597#35810
              TabOrder = 1
              Visible = False
              Height = 57
              Width = 1399
              object Label25: TLabel
                Left = 24
                Top = 24
                Width = 52
                Height = 14
                Caption = #26597#35810#21517#31216':'
              end
              object Label26: TLabel
                Left = 335
                Top = 25
                Width = 52
                Height = 14
                Caption = #36215#22987#26085#26399':'
              end
              object Label27: TLabel
                Left = 484
                Top = 25
                Width = 52
                Height = 14
                Caption = #32467#26463#26085#26399':'
              end
              object cxTextEdit8: TcxTextEdit
                Left = 82
                Top = 22
                TabOrder = 0
                TextHint = #24037#31243#21517#31216','#20379#24212#21333#20301','#30780#24378#24230
                OnKeyDown = cxTextEdit8KeyDown
                Width = 247
              end
              object cxDateEdit9: TcxDateEdit
                Left = 393
                Top = 23
                TabOrder = 1
                Width = 85
              end
              object cxDateEdit10: TcxDateEdit
                Left = 542
                Top = 23
                TabOrder = 2
                Width = 96
              end
              object cxButton8: TcxButton
                Left = 644
                Top = 19
                Width = 64
                Height = 30
                Caption = #26597#35810
                OptionsImage.Glyph.Data = {
                  36040000424D3604000000000000360000002800000010000000100000000100
                  2000000000000004000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000020000000B000000120000000C00000003000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000200000010071334970F276AFF0A193B970000000B000000000000
                  00007B5043B8AB705CFFAB6F5AFFAB705CFFAA6F5BFFAA6E59FFA96F5AFFBE91
                  82FFC9ACA3FF5F617FFF417CB9FF70C7FFFF265198FF00000010000000000000
                  0000AD735FFFFDFBF9FFFBF5F2FFF7F2EEFFF3EDE9FFEFE9E5FFECE5E1FFE6DE
                  DAFF707E9FFF4C83BCFF83CFFFFF5694CEFF142B4D930000000A000000000000
                  0000B07762FFFDFBFAFFF7F3F0FFE2D8D2FFA5816CFF8E5E42FF8C5D41FF7A5E
                  54FF577EA6FF92D4FAFF619CD0FF727F9BFF0000000E00000002000000000000
                  0000B07966FFFBF9F9FFE1D5CEFF936346FFC8A37FFFEFD7B2FFF0DAB8FFC7A6
                  88FF895D43FF6891B2FF849DB9FFCCB0A7FF0000000200000000000000000000
                  0000B37C69FFFAF8F7FFAD8975FFC7A07BFFF7D39CFFF5CD93FFF7D39BFFF9DD
                  B2FFC7A688FF84695DFFE8E2DEFFC29888FF0000000000000000000000000000
                  0000B67F6CFFF9F8F7FF98694CFFF1D4A7FFFAE5C0FFFBEACAFFF7D6A0FFF6D3
                  9BFFF2DBBBFF8F5D42FFF0E9E7FFB27A66FF0000000000000000000000000000
                  0000B98371FFFAF9F8FF9D6E51FFF2D4A5FFFDF6E2FFFDF3DCFFFBEACAFFF5CE
                  92FFF1DAB5FF936245FFF2EDE9FFB47D6AFF0000000000000000000000000000
                  0000BC8877FFFCFCFBFFB99783FFCDA77EFFF9E0B5FFFEF7E5FFFBE5C1FFF6D4
                  9DFFCAA681FFAF8C77FFF5F1EEFFB6806DFF0000000000000000000000000000
                  0000BF8C7AFFFDFDFCFFEDE4DFFFA87A5DFFCEA77FFFEFD2A3FFEFD2A5FFCCA7
                  80FFA17356FFE4DAD4FFFAF6F3FFB98371FF0000000000000000000000000000
                  0000C18F7FFFFEFEFEFFFDFCFBFFEDE4DFFFBE9C87FFAA7E62FFA97D60FFBB98
                  82FFEADFD8FFFBF8F6FFFDF9F8FFBD8774FF0000000000000000000000000000
                  0000C49382FFFFFEFEFFFEFEFDFFFBF6F4FFFAF5F3FFF9F3F0FFF9F3F0FFFAF2
                  F0FFFAF4F0FFFDFBF9FFFDFBF9FFBF8C7BFF0000000000000000000000000000
                  0000C79985FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFFEFC
                  FCFFFEFCFBFFFEFCFAFFFDFCFAFFC28F7FFF0000000000000000000000000000
                  0000C99A89FFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
                  FDFFFEFEFDFFFEFDFDFFFEFDFDFFC49382FF0000000000000000000000000000
                  0000967467BDCA9C8BFFCA9C8BFFC99C8AFFC99B89FFC99B8AFFCA9A88FFC89A
                  88FFC99987FFC79887FFC89886FF927163BD0000000000000000}
                TabOrder = 3
                OnClick = cxButton8Click
              end
            end
            object DBGridEh4: TDBGridEh
              Left = 0
              Top = 89
              Width = 1399
              Height = 428
              Align = alClient
              ColumnDefValues.Layout = tlCenter
              DataGrouping.Footers = <
                item
                  ColumnItems = <
                    item
                    end
                    item
                    end
                    item
                    end>
                  Visible = False
                end>
              DataSource = DataConcrete
              DrawMemoText = True
              DynProps = <>
              FixedColor = 16250613
              Flat = True
              FooterRowCount = 1
              FooterParams.Color = clYellow
              FooterParams.Font.Charset = DEFAULT_CHARSET
              FooterParams.Font.Color = clRed
              FooterParams.Font.Height = -11
              FooterParams.Font.Name = 'Tahoma'
              FooterParams.Font.Style = []
              FooterParams.ParentFont = False
              FooterParams.VertLines = False
              IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghHotTrack, dghExtendVertLines]
              PopupMenu = PopupMenu1
              ReadOnly = True
              RowDetailPanel.Active = True
              RowDetailPanel.Height = 600
              RowDetailPanel.Color = clBtnFace
              RowHeight = 30
              RowSizingAllowed = True
              SumList.Active = True
              TabOrder = 2
              TitleParams.MultiTitle = True
              TitleParams.RowHeight = 8
              TitleParams.RowLines = 1
              OnDblClick = DBGridEh4DblClick
              OnKeyDown = DBGridEh4KeyDown
              OnRowDetailPanelShow = DBGridEh4RowDetailPanelShow
              Columns = <
                item
                  Alignment = taCenter
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Numbers'
                  Footer.Value = #21512#35745':'
                  Footer.ValueType = fvtStaticText
                  Footers = <>
                  Title.Caption = #32534#21495
                  Title.TitleButton = True
                  Width = 110
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chProjectName'
                  Footers = <>
                  Title.Caption = #24037#31243#21517#31216
                  Width = 90
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chSupplyUnit'
                  Footers = <>
                  Title.Caption = #20379#24212#21333#20301
                  Width = 80
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chDate'
                  Footers = <>
                  Title.Caption = #26085#26399
                  Title.TitleButton = True
                  Width = 85
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'PurchaseCompany'
                  Footers = <>
                  Title.Caption = #36141#36135#21333#20301
                  Width = 80
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Deliver'
                  Footers = <>
                  HighlightRequired = True
                  Title.Caption = #36755#36865#26041#24335'|'#21517#31216
                  Title.TitleButton = True
                  Width = 52
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Deliver_UnitPrice'
                  Footers = <>
                  Title.Caption = #36755#36865#26041#24335'|'#21333#20215
                  Title.TitleButton = True
                  Width = 48
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Concrete'
                  Footers = <>
                  Title.Caption = #30780#24378#24230'|'#21517#31216
                  Title.TitleButton = True
                  Width = 49
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Concrete_UnitPrice'
                  Footers = <>
                  Title.Caption = #30780#24378#24230'|'#21333#20215
                  Title.TitleButton = True
                  Width = 49
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Permeability'
                  Footers = <>
                  Title.Caption = #25239#28183'|'#31561#32423
                  Title.TitleButton = True
                  Width = 53
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Permeability_Unitprice'
                  Footers = <>
                  Title.Caption = #25239#28183'|'#21333#20215
                  Title.TitleButton = True
                  Width = 49
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chAttach'
                  Footers = <>
                  Title.Caption = #20854#20182#25216#26415#35201#27714'|'#21517#31216
                  Title.TitleButton = True
                  Width = 46
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chAttach_UnitPrice'
                  Footers = <>
                  Title.Caption = #20854#20182#25216#26415#35201#27714'|'#21333#20215
                  Title.TitleButton = True
                  Width = 55
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chAdditive'
                  Footers = <>
                  Title.Caption = #22806#21152#21058'|'#21517#31216
                  Title.TitleButton = True
                  Width = 46
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chAdditive_UnitPrice'
                  Footers = <>
                  Title.Caption = #22806#21152#21058'|'#21333#20215
                  Title.TitleButton = True
                  Width = 54
                end
                item
                  Alignment = taCenter
                  CellButtons = <>
                  DisplayFormat = '0.##'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Stere'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #26041#37327'(m3)'
                  Title.TitleButton = True
                  Width = 60
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'UnitPrice'
                  Footers = <>
                  Title.Caption = #21333#20215
                  Title.TitleButton = True
                  Title.ToolTips = True
                  Width = 82
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 't'
                  Footer.DisplayFormat = #65509'#,##.#### '
                  Footer.FieldName = 't'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #24635#20215
                  Title.TitleButton = True
                  Width = 97
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chPosition'
                  Footers = <>
                  Title.Caption = #27975#31569#37096#20301
                  Title.TitleButton = True
                  Width = 180
                end
                item
                  CellButtons = <>
                  DisplayFormat = 'HH:mm:ss'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'pouringTime'
                  Footers = <>
                  Title.Caption = #26412#27425#27975#31569#26102#38388
                  Title.TitleButton = True
                  Width = 110
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'WagonNumber'
                  Footers = <>
                  Title.Caption = #36710#21495
                  Title.TitleButton = True
                  Width = 60
                end
                item
                  CellButtons = <>
                  DisplayFormat = 'HH:mm:ss'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'leaveTime'
                  Footers = <>
                  Title.Caption = #31163#22330#26102#38388
                  Title.TitleButton = True
                  Width = 81
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Sign'
                  Footers = <>
                  Title.Caption = #24037#22320#31614#23383
                  Title.TitleButton = True
                  Width = 73
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'remarks'
                  Footers = <>
                  Title.Caption = #22791#27880
                  Title.TitleButton = True
                  Width = 130
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object RzPanel9: TRzPanel
                  Left = 0
                  Top = 0
                  Width = 1353
                  Height = 41
                  Align = alTop
                  BorderOuter = fsFlat
                  GradientColorStyle = gcsCustom
                  TabOrder = 0
                  VisualStyle = vsGradient
                  object RzLabel3: TRzLabel
                    Left = 105
                    Top = 12
                    Width = 28
                    Height = 14
                    Caption = #26597#35810':'
                    Color = 16250613
                    ParentColor = False
                    Transparent = True
                    WordWrap = True
                  end
                  object RzEdit8: TRzEdit
                    Left = 139
                    Top = 10
                    Width = 209
                    Height = 22
                    Text = ''
                    TabOrder = 0
                  end
                  object RzBitBtn67: TRzBitBtn
                    Left = 354
                    Top = 7
                    Width = 62
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25628#32034
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 1
                    OnClick = RzBitBtn67Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                      E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                      E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                      E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                      E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                      81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                      7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                      E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                      8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                      88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                      89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                      E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                      E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                      88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                      82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                      AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                      E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn68: TRzBitBtn
                    Left = 422
                    Top = 7
                    Width = 84
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25171#24320#30446#24405
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 2
                    OnClick = RzBitBtn68Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
                      787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
                      CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
                      D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
                      D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
                      D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
                      D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
                      D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
                      A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
                      D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
                      A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
                      E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn69: TRzBitBtn
                    Left = 9
                    Top = 7
                    Height = 28
                    FrameColor = 7617536
                    Caption = #19978#19968#32423
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 3
                    OnClick = RzBitBtn69Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000530B0000530B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E80910101010
                      101010101010101009E8E881ACACACACACACACACACACACAC81E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                end
                object RzFileListBox3: TRzFileListBox
                  Left = 0
                  Top = 41
                  Width = 1353
                  Height = 271
                  Align = alClient
                  FileType = [ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ItemHeight = 18
                  ParentFont = False
                  TabOrder = 1
                  AllowOpen = True
                  FrameVisible = True
                  FramingPreference = fpCustomFraming
                end
              end
            end
          end
          object t4: TRzTabSheet
            Color = 16250613
            Caption = #26426#26800#22303#30707#26041
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 1399
              Height = 32
              Align = alTop
              BevelOuter = bvNone
              Caption = #26426#26800#22303#30707#26041
              Color = 16250613
              ParentBackground = False
              TabOrder = 0
              object RzBitBtn30: TRzBitBtn
                Left = 12
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #28155#21152
                Color = 15791348
                HotTrack = True
                TabOrder = 0
                OnClick = RzBitBtn30Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80909
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8E8E8E8E8E8091010
                  09E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E809101010
                  1009E8E8E8E8E8E8E8E8E8E881ACACACAC81E8E8E8E8E8E8E8E8E80910101010
                  101009E8E8E8E8E8E8E8E881ACACACACACAC81E8E8E8E8E8E8E8E80910100909
                  10101009E8E8E8E8E8E8E881ACAC8181ACACAC81E8E8E8E8E8E8E8091009E8E8
                  0910101009E8E8E8E8E8E881AC81E8E881ACACAC81E8E8E8E8E8E80909E8E8E8
                  E80910101009E8E8E8E8E88181E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8
                  E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8
                  E8E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E809101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E80909E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn31: TRzBitBtn
                Left = 85
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #32534#36753
                Color = 15791348
                HotTrack = True
                TabOrder = 1
                OnClick = RzBitBtn31Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000520B0000520B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E81710101010
                  1010101010E8E8E8E8E8E8AC818181818181818181E8E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  E3E3E3E3E310E8E8E8E8AC81E3E3E3E3E3E3E3E3E381E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  ACACACACAC10E8E8E8E8AC81E3E3E3E3ACACACACAC81E8E8E8E81710D7D7D7D7
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7D7E3E3E3E3E381E8E8E8E81710E3E3E3AC
                  ACACACACAC10E8E8E8E8AC81E3E3E3ACACACACACAC81E8E8E8E81710D7D7D7E3
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7E3E3E3E3E3E381E8E8E8E81710E3E3ACAC
                  10101010101010101010AC81E3E3ACAC818181818181818181811710D7D7E310
                  17101010101010101010AC81D7D7E381AC8181818181818181811710E3AC1717
                  171717101010101010E8AC81E3ACACACACACAC818181818181E81710D7175F5F
                  1717171717101010E8E8AC81D7ACE3E3ACACACACAC818181E8E81710175F5F5F
                  5F5F1717171710E8E8E8AC81ACE3E3E3E3E3ACACACAC81E8E8E817175F5F5F5F
                  5F5F5F5F1710E8E8E8E8ACACE3E3E3E3E3E3E3E3AC81E8E8E8E8E81781D781D7
                  81D781D781D7E8E8E8E8E8AC81D781D781D781D781D7E8E8E8E8E8E881AC81AC
                  81AC81AC81E8E8E8E8E8E8E881AC81AC81AC81AC81E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn32: TRzBitBtn
                Left = 158
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #21024#38500
                Color = 15791348
                HotTrack = True
                TabOrder = 2
                OnClick = RzBitBtn32Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
                  10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn33: TRzBitBtn
                Left = 642
                Top = 4
                Width = 65
                Height = 30
                FrameColor = 7617536
                Caption = 'Excel'
                Color = 15791348
                HotTrack = True
                TabOrder = 3
                OnClick = RzBitBtn13Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000B30B0000B30B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7ACAC
                  ACACD7D7D7D7D75EE8E8E8E881E8ACACACACE8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D709D7D7D75EE8E8E8E881E8E8E8E8E8E881E8E8E881E8E8E8E85ED7ACAC
                  D7D70909D7D7D75EE8E8E8E881E8ACACE8E88181E8E8E881E8E8E8E85ED7D7D7
                  D7090909095ED75EE8E8E8E881E8E8E8E881818181ACE881E8E8E8E85ED7ACAC
                  D7D70909D709D75EE8E8E8E881E8ACACE8E88181E881E881E8E8E8E85ED7D7D7
                  D7D7D709D709D75EE8E8E8E881E8E8E8E8E8E881E881E881E8E8E8E85ED7ACAC
                  ACD7D7D7D709D75EE8E8E8E881E8ACACACE8E8E8E881E881E8E8E8E85ED7D7D7
                  D7D7D7D7D709D75EE8E8E8E881E8E8E8E8E8E8E8E881E881E8E8E8E85ED7D7D7
                  09090909095ED75EE8E8E8E881E8E8E88181818181ACE881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn34: TRzBitBtn
                Left = 713
                Top = 4
                Width = 73
                Height = 30
                FrameColor = 7617536
                Caption = #25171#21360
                Color = 15791348
                HotTrack = True
                TabOrder = 4
                OnClick = RzBitBtn14Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000730E0000730E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909
                  09090909090909E8E8E8E8E88181818181818181818181E8E8E8E85E89898989
                  89898989895E5E09E8E8E8E2ACACACACACACACACACE2E281E8E85E5E5E5E5E5E
                  5E5E5E5E5E5E095E09E8E2E2E2E2E2E2E2E2E2E2E2E281E281E85ED789898989
                  8989898989895E0909E8E2E8ACACACACACACACACACACE28181E85ED789898989
                  181289B490895E5E09E8E2E8ACACACACE281ACE281ACE2E281E85ED7D7D7D7D7
                  D7D7D7D7D7D75E5E5E09E2E8E8E8E8E8E8E8E8E8E8E8E2E2E2815ED789898989
                  8989898989895E5E5E09E2E8ACACACACACACACACACACE2E2E281E85E5E5E5E5E
                  5E5E5E5E5E89895E5E09E8E2E2E2E2E2E2E2E2E2E2ACACE2E281E8E85ED7D7D7
                  D7D7D7D7D75E89895E09E8E8E2E8E8E8E8E8E8E8E8E2ACACE281E8E8E85ED7E3
                  E3E3E3E3D75E5E5E09E8E8E8E8E2E8ACACACACACE8E2E2E281E8E8E8E85ED7D7
                  D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85ED7
                  E3E3E3E3E3D75EE8E8E8E8E8E8E8E2E8ACACACACACE8E2E8E8E8E8E8E8E85ED7
                  D7D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85E
                  5E5E5E5E5E5E5E5EE8E8E8E8E8E8E8E2E2E2E2E2E2E2E2E2E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn35: TRzBitBtn
                Left = 796
                Top = 4
                Width = 73
                Height = 30
                FrameColor = 7617536
                Caption = #20851#38381
                Color = 15791348
                HotTrack = True
                TabOrder = 5
                OnClick = RzBitBtn21Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000420B0000420B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
                  D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn114: TRzBitBtn
                Left = 231
                Top = 4
                Width = 80
                Height = 28
                FrameColor = 7617536
                Caption = #27719#24635#34920
                Color = 15791348
                HotTrack = True
                TabOrder = 6
                OnClick = RzBitBtn114Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000E30E0000E30E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E89C9C9C
                  9C9C9C9C9C9C9CE8E8E8E8E8E881818181818181818181E8E8E8E8E89CC6C6C6
                  C6C69CC69C9C9C9CE8E8E8E881E2E2E2E2E281E281818181E8E8E8E89CCCC6C6
                  C6C6C69CC69C9C9CE8E8E8E881ACE2E2E2E2E281E2818181E8E8E8E89CC6CCC6
                  C6C6C6C69CC69C9CE8E8E8E881E2ACE2E2E2E2E281E28181E8E8E8E89CCCC6CC
                  C6C6C6C6C69CC69CE8E8E8E881ACE2ACE2E2E2E2E281E281E8E8E8E89CCCCCC6
                  CCC6C6C6C6C69C9CE8E8E8E881ACACE2ACE2E2E2E2E28181E8E8E8E89CCFCCCC
                  C6CCC6C6C6C6C69CE8E8E8E881E3ACACE2ACE2E2E2E2E281E8E8E8E89CCFCFCC
                  CCC6CCC6C6C6C69CE8E8E8E881E3E3ACACE2ACE2E2E2E281E8E8E8E8E89C9C9C
                  9C9C9C9C9C9C9CE8E8E8E8E8E881818181818181818181E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn117: TRzBitBtn
                Left = 317
                Top = 4
                Width = 80
                Height = 28
                FrameColor = 7617536
                Caption = #21512#21516
                Color = 15791348
                HotTrack = True
                TabOrder = 7
                OnClick = RzBitBtn117Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000E30E0000E30E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E86C6C6C
                  6C6C6C6C6C6C6CE8E8E8E8E8E881818181818181818181E8E8E8E8E86C909090
                  90906C906C6C6C6CE8E8E8E881E2E2E2E2E281E281818181E8E8E8E86CB49090
                  9090906C906C6C6CE8E8E8E881ACE2E2E2E2E281E2818181E8E8E8E86C90B490
                  909090906C906C6CE8E8E8E881E2ACE2E2E2E2E281E28181E8E8E8E86CB490B4
                  90909090906C906CE8E8E8E881ACE2ACE2E2E2E2E281E281E8E8E8E86CB4B490
                  B490909090906C6CE8E8E8E881ACACE2ACE2E2E2E2E28181E8E8E8E86CC9B4B4
                  90B490909090906CE8E8E8E881E3ACACE2ACE2E2E2E2E281E8E8E8E86CC9C9B4
                  B490B4909090906CE8E8E8E881E3E3ACACE2ACE2E2E2E281E8E8E8E8E86C6C6C
                  6C6C6C6C6C6C6CE8E8E8E8E8E881818181818181818181E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn124: TRzBitBtn
                Left = 403
                Top = 4
                Width = 72
                Height = 28
                FrameColor = 7617536
                Caption = #26597#35810
                Color = 15791348
                HotTrack = True
                TabOrder = 8
                OnClick = RzBitBtn124Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                  E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                  E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                  E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                  E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                  81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                  7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                  E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                  8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                  88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                  89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                  E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                  E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                  88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                  82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                  AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                  E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                NumGlyphs = 2
              end
            end
            object cxGroupBox7: TcxGroupBox
              Left = 0
              Top = 32
              Align = alTop
              Caption = #26597#35810
              TabOrder = 1
              Visible = False
              Height = 54
              Width = 1399
              object Label28: TLabel
                Left = 24
                Top = 20
                Width = 52
                Height = 14
                Caption = #26597#35810#21517#31216':'
              end
              object Label29: TLabel
                Left = 487
                Top = 20
                Width = 52
                Height = 14
                Caption = #36215#22987#26085#26399':'
              end
              object Label30: TLabel
                Left = 636
                Top = 20
                Width = 52
                Height = 14
                Caption = #32467#26463#26085#26399':'
              end
              object Label46: TLabel
                Left = 335
                Top = 20
                Width = 52
                Height = 14
                Caption = #26426#26800#31867#21035':'
              end
              object cxTextEdit9: TcxTextEdit
                Left = 82
                Top = 18
                TabOrder = 0
                TextHint = #24037#31243#21517#31216','#24448#26469#21333#20301
                OnKeyDown = cxTextEdit9KeyDown
                Width = 247
              end
              object cxDateEdit11: TcxDateEdit
                Left = 545
                Top = 18
                TabOrder = 1
                Width = 85
              end
              object cxDateEdit12: TcxDateEdit
                Left = 694
                Top = 18
                TabOrder = 2
                Width = 96
              end
              object cxButton9: TcxButton
                Left = 796
                Top = 14
                Width = 64
                Height = 30
                Caption = #26597#35810
                OptionsImage.Glyph.Data = {
                  36040000424D3604000000000000360000002800000010000000100000000100
                  2000000000000004000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000020000000B000000120000000C00000003000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000200000010071334970F276AFF0A193B970000000B000000000000
                  00007B5043B8AB705CFFAB6F5AFFAB705CFFAA6F5BFFAA6E59FFA96F5AFFBE91
                  82FFC9ACA3FF5F617FFF417CB9FF70C7FFFF265198FF00000010000000000000
                  0000AD735FFFFDFBF9FFFBF5F2FFF7F2EEFFF3EDE9FFEFE9E5FFECE5E1FFE6DE
                  DAFF707E9FFF4C83BCFF83CFFFFF5694CEFF142B4D930000000A000000000000
                  0000B07762FFFDFBFAFFF7F3F0FFE2D8D2FFA5816CFF8E5E42FF8C5D41FF7A5E
                  54FF577EA6FF92D4FAFF619CD0FF727F9BFF0000000E00000002000000000000
                  0000B07966FFFBF9F9FFE1D5CEFF936346FFC8A37FFFEFD7B2FFF0DAB8FFC7A6
                  88FF895D43FF6891B2FF849DB9FFCCB0A7FF0000000200000000000000000000
                  0000B37C69FFFAF8F7FFAD8975FFC7A07BFFF7D39CFFF5CD93FFF7D39BFFF9DD
                  B2FFC7A688FF84695DFFE8E2DEFFC29888FF0000000000000000000000000000
                  0000B67F6CFFF9F8F7FF98694CFFF1D4A7FFFAE5C0FFFBEACAFFF7D6A0FFF6D3
                  9BFFF2DBBBFF8F5D42FFF0E9E7FFB27A66FF0000000000000000000000000000
                  0000B98371FFFAF9F8FF9D6E51FFF2D4A5FFFDF6E2FFFDF3DCFFFBEACAFFF5CE
                  92FFF1DAB5FF936245FFF2EDE9FFB47D6AFF0000000000000000000000000000
                  0000BC8877FFFCFCFBFFB99783FFCDA77EFFF9E0B5FFFEF7E5FFFBE5C1FFF6D4
                  9DFFCAA681FFAF8C77FFF5F1EEFFB6806DFF0000000000000000000000000000
                  0000BF8C7AFFFDFDFCFFEDE4DFFFA87A5DFFCEA77FFFEFD2A3FFEFD2A5FFCCA7
                  80FFA17356FFE4DAD4FFFAF6F3FFB98371FF0000000000000000000000000000
                  0000C18F7FFFFEFEFEFFFDFCFBFFEDE4DFFFBE9C87FFAA7E62FFA97D60FFBB98
                  82FFEADFD8FFFBF8F6FFFDF9F8FFBD8774FF0000000000000000000000000000
                  0000C49382FFFFFEFEFFFEFEFDFFFBF6F4FFFAF5F3FFF9F3F0FFF9F3F0FFFAF2
                  F0FFFAF4F0FFFDFBF9FFFDFBF9FFBF8C7BFF0000000000000000000000000000
                  0000C79985FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFFEFC
                  FCFFFEFCFBFFFEFCFAFFFDFCFAFFC28F7FFF0000000000000000000000000000
                  0000C99A89FFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
                  FDFFFEFEFDFFFEFDFDFFFEFDFDFFC49382FF0000000000000000000000000000
                  0000967467BDCA9C8BFFCA9C8BFFC99C8AFFC99B89FFC99B8AFFCA9A88FFC89A
                  88FFC99987FFC79887FFC89886FF927163BD0000000000000000}
                TabOrder = 3
                OnClick = cxButton9Click
              end
              object cxComboBox4: TcxComboBox
                Left = 393
                Top = 18
                Properties.OnCloseUp = cxComboBox4PropertiesCloseUp
                TabOrder = 4
                Width = 88
              end
            end
            object DBGridEh6: TDBGridEh
              Left = 0
              Top = 86
              Width = 1399
              Height = 431
              Align = alClient
              ColumnDefValues.Layout = tlCenter
              DataGrouping.Footers = <
                item
                  ColumnItems = <
                    item
                    end
                    item
                    end
                    item
                    end>
                  Visible = False
                end>
              DataSource = DataMechanics
              DrawMemoText = True
              DynProps = <>
              FixedColor = 16250613
              Flat = True
              FooterRowCount = 1
              FooterParams.Color = clYellow
              FooterParams.Font.Charset = DEFAULT_CHARSET
              FooterParams.Font.Color = clRed
              FooterParams.Font.Height = -11
              FooterParams.Font.Name = 'Tahoma'
              FooterParams.Font.Style = []
              FooterParams.ParentFont = False
              FooterParams.VertLines = False
              IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghHotTrack, dghExtendVertLines]
              PopupMenu = PopupMenu1
              ReadOnly = True
              RowDetailPanel.Active = True
              RowDetailPanel.Height = 600
              RowDetailPanel.Color = clBtnFace
              RowHeight = 30
              RowSizingAllowed = True
              SumList.Active = True
              TabOrder = 2
              TitleParams.MultiTitle = True
              TitleParams.RowHeight = 8
              TitleParams.RowLines = 1
              OnKeyDown = DBGridEh1KeyDown
              OnRowDetailPanelShow = DBGridEh6RowDetailPanelShow
              Columns = <
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'numbers'
                  Footer.Value = #21512#35745':'
                  Footer.ValueType = fvtStaticText
                  Footers = <>
                  Title.Caption = #32534#21495
                  Width = 110
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Caption'
                  Footers = <>
                  Title.Caption = #24037#31243#21517#31216
                  Width = 96
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Content'
                  Footers = <>
                  Title.Caption = #24448#26469#21333#20301
                  Width = 109
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'WorkDate'
                  Footers = <>
                  Title.Caption = #26085#26399
                  Width = 63
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'BuildCompany'
                  Footers = <>
                  Title.Caption = #26045#24037#21333#20301
                  Width = 80
                end
                item
                  CellButtons = <>
                  DisplayFormat = 'yyyy-MM-dd'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'start_date'
                  Footers = <>
                  Title.Caption = #35745#26102#24320#22987'|'#26085#26399
                end
                item
                  CellButtons = <>
                  DisplayFormat = 'hh:nn:ss'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'start_time'
                  Footers = <>
                  Title.Caption = #35745#26102#24320#22987'|'#26102#38388
                  Width = 59
                end
                item
                  CellButtons = <>
                  DisplayFormat = 'yyyy-MM-dd'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'end_date'
                  Footers = <>
                  Title.Caption = #35745#26102#32467#26463'|'#26085#26399
                end
                item
                  CellButtons = <>
                  DisplayFormat = 'hh:nn:ss'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'end_time'
                  Footers = <>
                  Title.Caption = #35745#26102#32467#26463'|'#26102#38388
                  Width = 59
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'addtime'
                  Footers = <>
                  Title.Caption = #35745#26102#24635#25968
                  Width = 73
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'addprice'
                  Footers = <>
                  Title.Caption = #35745#26102#21333#20215
                  Width = 74
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'mechanicstype'
                  Footers = <>
                  Title.Caption = #26426#26800#31867#21035
                  Width = 39
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Price'
                  Footers = <>
                  Title.Caption = #21333#20215
                  Width = 72
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Units'
                  Footers = <>
                  Title.Caption = #21333#20301
                  Width = 70
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'daynum'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #25968#20540
                  Width = 65
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'SumMoney'
                  Footer.DisplayFormat = #65509'#,0.##'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #24635#20215
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'remarks'
                  Footers = <>
                  Title.Caption = #22791#27880
                  Width = 251
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object RzPanel11: TRzPanel
                  Left = 0
                  Top = 0
                  Width = 1353
                  Height = 41
                  Align = alTop
                  BorderOuter = fsFlat
                  GradientColorStyle = gcsCustom
                  TabOrder = 0
                  VisualStyle = vsGradient
                  object RzLabel4: TRzLabel
                    Left = 105
                    Top = 12
                    Width = 28
                    Height = 14
                    Caption = #26597#35810':'
                    Color = 16250613
                    ParentColor = False
                    Transparent = True
                    WordWrap = True
                  end
                  object RzEdit9: TRzEdit
                    Left = 139
                    Top = 10
                    Width = 209
                    Height = 22
                    Text = ''
                    TabOrder = 0
                  end
                  object RzBitBtn70: TRzBitBtn
                    Left = 354
                    Top = 7
                    Width = 62
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25628#32034
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 1
                    OnClick = RzBitBtn70Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                      E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                      E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                      E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                      E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                      81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                      7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                      E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                      8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                      88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                      89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                      E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                      E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                      88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                      82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                      AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                      E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn71: TRzBitBtn
                    Left = 422
                    Top = 7
                    Width = 84
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25171#24320#30446#24405
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 2
                    OnClick = RzBitBtn71Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
                      787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
                      CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
                      D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
                      D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
                      D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
                      D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
                      D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
                      A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
                      D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
                      A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
                      E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn72: TRzBitBtn
                    Left = 9
                    Top = 7
                    Height = 28
                    FrameColor = 7617536
                    Caption = #19978#19968#32423
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 3
                    OnClick = RzBitBtn72Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000530B0000530B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E80910101010
                      101010101010101009E8E881ACACACACACACACACACACACAC81E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                end
                object RzFileListBox4: TRzFileListBox
                  Left = 0
                  Top = 41
                  Width = 1353
                  Height = 274
                  Align = alClient
                  FileType = [ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ItemHeight = 18
                  ParentFont = False
                  TabOrder = 1
                  AllowOpen = True
                  FrameVisible = True
                  FramingPreference = fpCustomFraming
                end
              end
            end
          end
          object t5: TRzTabSheet
            Color = 16250613
            Caption = #39033#30446#20998#21253#21333#20301
            object cxGroupBox8: TcxGroupBox
              Left = 16
              Top = 97
              Caption = #26597#35810
              TabOrder = 0
              Height = 57
              Width = 1154
              object Label31: TLabel
                Left = 24
                Top = 24
                Width = 52
                Height = 14
                Caption = #26597#35810#21517#31216':'
              end
              object Label32: TLabel
                Left = 335
                Top = 25
                Width = 52
                Height = 14
                Caption = #36215#22987#26085#26399':'
              end
              object Label33: TLabel
                Left = 484
                Top = 25
                Width = 52
                Height = 14
                Caption = #32467#26463#26085#26399':'
              end
              object cxTextEdit10: TcxTextEdit
                Left = 82
                Top = 22
                TabOrder = 0
                TextHint = #24037#31243#21517#31216','#20998#21253#21333#20301
                Width = 247
              end
              object cxDateEdit13: TcxDateEdit
                Left = 393
                Top = 23
                TabOrder = 1
                Width = 85
              end
              object cxDateEdit14: TcxDateEdit
                Left = 542
                Top = 23
                TabOrder = 2
                Width = 96
              end
              object cxButton10: TcxButton
                Left = 644
                Top = 19
                Width = 64
                Height = 30
                Caption = #26597#35810
                OptionsImage.Glyph.Data = {
                  36040000424D3604000000000000360000002800000010000000100000000100
                  2000000000000004000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000020000000B000000120000000C00000003000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000200000010071334970F276AFF0A193B970000000B000000000000
                  00007B5043B8AB705CFFAB6F5AFFAB705CFFAA6F5BFFAA6E59FFA96F5AFFBE91
                  82FFC9ACA3FF5F617FFF417CB9FF70C7FFFF265198FF00000010000000000000
                  0000AD735FFFFDFBF9FFFBF5F2FFF7F2EEFFF3EDE9FFEFE9E5FFECE5E1FFE6DE
                  DAFF707E9FFF4C83BCFF83CFFFFF5694CEFF142B4D930000000A000000000000
                  0000B07762FFFDFBFAFFF7F3F0FFE2D8D2FFA5816CFF8E5E42FF8C5D41FF7A5E
                  54FF577EA6FF92D4FAFF619CD0FF727F9BFF0000000E00000002000000000000
                  0000B07966FFFBF9F9FFE1D5CEFF936346FFC8A37FFFEFD7B2FFF0DAB8FFC7A6
                  88FF895D43FF6891B2FF849DB9FFCCB0A7FF0000000200000000000000000000
                  0000B37C69FFFAF8F7FFAD8975FFC7A07BFFF7D39CFFF5CD93FFF7D39BFFF9DD
                  B2FFC7A688FF84695DFFE8E2DEFFC29888FF0000000000000000000000000000
                  0000B67F6CFFF9F8F7FF98694CFFF1D4A7FFFAE5C0FFFBEACAFFF7D6A0FFF6D3
                  9BFFF2DBBBFF8F5D42FFF0E9E7FFB27A66FF0000000000000000000000000000
                  0000B98371FFFAF9F8FF9D6E51FFF2D4A5FFFDF6E2FFFDF3DCFFFBEACAFFF5CE
                  92FFF1DAB5FF936245FFF2EDE9FFB47D6AFF0000000000000000000000000000
                  0000BC8877FFFCFCFBFFB99783FFCDA77EFFF9E0B5FFFEF7E5FFFBE5C1FFF6D4
                  9DFFCAA681FFAF8C77FFF5F1EEFFB6806DFF0000000000000000000000000000
                  0000BF8C7AFFFDFDFCFFEDE4DFFFA87A5DFFCEA77FFFEFD2A3FFEFD2A5FFCCA7
                  80FFA17356FFE4DAD4FFFAF6F3FFB98371FF0000000000000000000000000000
                  0000C18F7FFFFEFEFEFFFDFCFBFFEDE4DFFFBE9C87FFAA7E62FFA97D60FFBB98
                  82FFEADFD8FFFBF8F6FFFDF9F8FFBD8774FF0000000000000000000000000000
                  0000C49382FFFFFEFEFFFEFEFDFFFBF6F4FFFAF5F3FFF9F3F0FFF9F3F0FFFAF2
                  F0FFFAF4F0FFFDFBF9FFFDFBF9FFBF8C7BFF0000000000000000000000000000
                  0000C79985FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFFEFC
                  FCFFFEFCFBFFFEFCFAFFFDFCFAFFC28F7FFF0000000000000000000000000000
                  0000C99A89FFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
                  FDFFFEFEFDFFFEFDFDFFFEFDFDFFC49382FF0000000000000000000000000000
                  0000967467BDCA9C8BFFCA9C8BFFC99C8AFFC99B89FFC99B8AFFCA9A88FFC89A
                  88FFC99987FFC79887FFC89886FF927163BD0000000000000000}
                TabOrder = 3
              end
            end
            object cxGrid4: TcxGrid
              Left = 0
              Top = 29
              Width = 1399
              Height = 488
              Align = alClient
              TabOrder = 1
              LookAndFeel.SkinName = 'Office2016Colorful'
              object tvEngineeringQuantity: TcxGridDBTableView
                OnKeyDown = tvEngineeringQuantityKeyDown
                Navigator.Buttons.CustomButtons = <>
                OnEditKeyDown = tvEngineeringQuantityEditKeyDown
                DataController.DataSource = DataQuantity
                DataController.KeyFieldNames = 'numbers'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = '0.##'
                    Kind = skSum
                    Column = tvEngineeringQuantityColumn8
                  end
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    OnGetText = tvEngineeringQuantityTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText
                    Column = tvEngineeringQuantityColumn11
                  end
                  item
                    Kind = skCount
                    Column = tvEngineeringQuantityColumn12
                  end>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.FocusCellOnTab = True
                OptionsBehavior.GoToNextCellOnEnter = True
                OptionsBehavior.FocusCellOnCycle = True
                OptionsCustomize.DataRowSizing = True
                OptionsCustomize.GroupRowSizing = True
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsSelection.InvertSelect = False
                OptionsSelection.MultiSelect = True
                OptionsSelection.CellMultiSelect = True
                OptionsView.CellEndEllipsis = True
                OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#39033#30446#20998#21253#35760#24405'>'
                OptionsView.CellAutoHeight = True
                OptionsView.DataRowHeight = 32
                OptionsView.Footer = True
                OptionsView.GroupByBox = False
                OptionsView.HeaderHeight = 25
                OptionsView.Indicator = True
                OptionsView.IndicatorWidth = 15
                OnColumnSizeChanged = tvEngineeringQuantityColumnSizeChanged
                object tvEngineeringQuantityColumn13: TcxGridDBColumn
                  Caption = #24207#21495
                  OnGetDisplayText = tvEngineeringQuantityColumn13GetDisplayText
                  MinWidth = 30
                  Options.Editing = False
                  Options.Filtering = False
                  Options.FilteringAddValueItems = False
                  Options.FilteringFilteredItemsList = False
                  Options.FilteringMRUItemsList = False
                  Options.FilteringPopup = False
                  Options.FilteringPopupMultiSelect = False
                  Options.FilteringWithFindPanel = False
                  Options.Focusing = False
                  Options.IgnoreTimeForFiltering = False
                  Options.IncSearch = False
                  Options.GroupFooters = False
                  Options.Grouping = False
                  Options.HorzSizing = False
                  Options.Moving = False
                  Options.Sorting = False
                  Width = 30
                end
                object tvEngineeringQuantityColumn1: TcxGridDBColumn
                  Caption = #32534#21495
                  DataBinding.FieldName = 'numbers'
                  Options.Editing = False
                  Options.Filtering = False
                  Width = 71
                end
                object tvEngineeringQuantityColumn2: TcxGridDBColumn
                  Caption = #26085#26399
                  DataBinding.FieldName = 'InputDate'
                  PropertiesClassName = 'TcxDateEditProperties'
                  Options.Filtering = False
                  Width = 92
                end
                object tvEngineeringQuantityColumn3: TcxGridDBColumn
                  Caption = #24037#31243#21517#31216
                  DataBinding.FieldName = 'ProjectName'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Filtering = False
                  Width = 120
                end
                object tvEngineeringQuantityColumn14: TcxGridDBColumn
                  Caption = #36164#36136#21333#20301
                  Width = 95
                end
                object tvEngineeringQuantityColumn15: TcxGridDBColumn
                  Caption = #39033#30446#37096
                  DataBinding.FieldName = 'ProjectDepartment'
                  Width = 117
                end
                object tvEngineeringQuantityColumn4: TcxGridDBColumn
                  Caption = #24037#31181
                  DataBinding.FieldName = 'WorkType'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Filtering = False
                  Width = 120
                end
                object tvEngineeringQuantityColumn5: TcxGridDBColumn
                  Caption = #20998#21253#20869#23481
                  DataBinding.FieldName = 'MeteringPosName'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Filtering = False
                  Width = 120
                end
                object tvEngineeringQuantityColumn6: TcxGridDBColumn
                  Caption = #20998#21253#21333#20301
                  DataBinding.FieldName = 'ContractCompany'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Filtering = False
                  Width = 120
                end
                object tvEngineeringQuantityColumn7: TcxGridDBColumn
                  Caption = #35745#37327#31867#21035
                  DataBinding.FieldName = 'MeteringType'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Filtering = False
                  Width = 120
                end
                object tvEngineeringQuantityColumn9: TcxGridDBColumn
                  Caption = #21333#20215
                  DataBinding.FieldName = 'UnitPrice'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  HeaderAlignmentHorz = taRightJustify
                  Options.Filtering = False
                  Width = 120
                end
                object tvEngineeringQuantityColumn10: TcxGridDBColumn
                  Caption = #21333#20301
                  DataBinding.FieldName = 'Company'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Filtering = False
                  Width = 87
                end
                object tvEngineeringQuantityColumn8: TcxGridDBColumn
                  Caption = #25968#37327
                  DataBinding.FieldName = 'MeteringCount'
                  PropertiesClassName = 'TcxSpinEditProperties'
                  Properties.ValueType = vtFloat
                  HeaderAlignmentHorz = taRightJustify
                  Options.Filtering = False
                  Width = 72
                end
                object tvEngineeringQuantityColumn11: TcxGridDBColumn
                  Caption = #24635#20215
                  DataBinding.FieldName = 'Total'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  HeaderAlignmentHorz = taRightJustify
                  Options.Filtering = False
                  Styles.Content = DM.SumMoney
                  Width = 120
                end
                object tvEngineeringQuantityColumn12: TcxGridDBColumn
                  Caption = #22791#27880
                  DataBinding.FieldName = 'remarks'
                  Options.Filtering = False
                  Width = 230
                end
              end
              object tvDetailedGrid: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = DataDetailed
                DataController.DetailKeyFieldNames = 'parent'
                DataController.MasterKeyFieldNames = 'numbers'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = #24635#25968':0.##'
                    Kind = skSum
                    Column = tvDetailedGridColumn5
                  end>
                DataController.Summary.SummaryGroups = <>
                OptionsCustomize.DataRowSizing = True
                OptionsView.CellEndEllipsis = True
                OptionsView.CellAutoHeight = True
                OptionsView.DataRowHeight = 32
                OptionsView.Footer = True
                OptionsView.GroupByBox = False
                OptionsView.HeaderHeight = 25
                OptionsView.Indicator = True
                object tvDetailedGridColumn1: TcxGridDBColumn
                  Caption = #32534#21495
                  DataBinding.FieldName = 'numbers'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 103
                end
                object tvDetailedGridColumn2: TcxGridDBColumn
                  Caption = #35745#31639#37096#20301
                  DataBinding.FieldName = 'ItemPosition'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 129
                end
                object tvDetailedGridColumn3: TcxGridDBColumn
                  Caption = #35268#26684#31867#21035
                  DataBinding.FieldName = 'ItemType'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 136
                end
                object tvDetailedGridColumn4: TcxGridDBColumn
                  Caption = #35745#31639#20844#24335
                  DataBinding.FieldName = 'ItemSpec'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 119
                end
                object tvDetailedGridColumn5: TcxGridDBColumn
                  Caption = #25968#37327
                  DataBinding.FieldName = 'ItemValue'
                  HeaderAlignmentHorz = taRightJustify
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 80
                end
                object tvDetailedGridColumn6: TcxGridDBColumn
                  Caption = #21333#20301
                  DataBinding.FieldName = 'ItemUnit'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 121
                end
                object tvDetailedGridColumn7: TcxGridDBColumn
                  Caption = #22791#27880
                  DataBinding.FieldName = 'remarks'
                  PropertiesClassName = 'TcxMemoProperties'
                  Width = 230
                end
                object tvDetailedGridColumn8: TcxGridDBColumn
                  Caption = #22791#27880'1'
                  DataBinding.FieldName = 'remarks1'
                  PropertiesClassName = 'TcxMemoProperties'
                  Options.Filtering = False
                  Options.Sorting = False
                  Width = 230
                end
              end
              object LvQuantity: TcxGridLevel
                GridView = tvEngineeringQuantity
                object LvDetailed: TcxGridLevel
                  GridView = tvDetailedGrid
                end
              end
            end
            object RzToolbar12: TRzToolbar
              Left = 0
              Top = 0
              Width = 1399
              Height = 29
              AutoStyle = False
              Images = DM.cxImageList1
              BorderInner = fsNone
              BorderOuter = fsNone
              BorderSides = [sdTop]
              BorderWidth = 0
              GradientColorStyle = gcsCustom
              TabOrder = 2
              VisualStyle = vsGradient
              ToolbarControls = (
                RzSpacer100
                RzToolButton75
                RzSpacer101
                RzToolButton76
                RzSpacer102
                RzToolButton77
                RzSpacer103
                RzToolButton78
                RzSpacer104
                RzToolButton83
                RzSpacer111
                RzToolButton79
                RzSpacer108
                RzToolButton82
                RzSpacer109
                RzToolButton20
                RzSpacer105
                cxLabel50
                Date5
                RzSpacer107
                cxLabel51
                Date6
                RzSpacer110
                RzToolButton81
                RzSpacer106
                RzToolButton80
                RzSpacer19)
              object RzSpacer100: TRzSpacer
                Left = 4
                Top = 2
              end
              object RzToolButton75: TRzToolButton
                Left = 12
                Top = 2
                Width = 65
                SelectionColorStop = 16250613
                ImageIndex = 1
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #28155#21152
                OnClick = RzToolButton75Click
              end
              object RzSpacer101: TRzSpacer
                Left = 77
                Top = 2
              end
              object RzToolButton76: TRzToolButton
                Left = 85
                Top = 2
                Width = 67
                SelectionColorStop = 16250613
                ImageIndex = 3
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #20445#23384
                OnClick = RzToolButton76Click
              end
              object RzSpacer102: TRzSpacer
                Left = 152
                Top = 2
              end
              object RzToolButton77: TRzToolButton
                Left = 160
                Top = 2
                Width = 65
                SelectionColorStop = 16250613
                ImageIndex = 2
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #21024#38500
                OnClick = RzToolButton75Click
              end
              object RzSpacer103: TRzSpacer
                Left = 225
                Top = 2
              end
              object RzToolButton78: TRzToolButton
                Left = 233
                Top = 2
                Width = 70
                SelectionColorStop = 16250613
                ImageIndex = 4
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #34920#26684#35774#32622
                OnClick = RzToolButton78Click
              end
              object RzToolButton79: TRzToolButton
                Left = 407
                Top = 2
                Width = 65
                SelectionColorStop = 16250613
                DropDownMenu = Print
                ImageIndex = 5
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                ToolStyle = tsDropDown
                Caption = #25253#34920
              end
              object RzSpacer104: TRzSpacer
                Left = 303
                Top = 2
              end
              object RzToolButton80: TRzToolButton
                Left = 959
                Top = 2
                Width = 65
                SelectionColorStop = 16250613
                ImageIndex = 8
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #36864#20986
                OnClick = RzToolButton80Click
              end
              object RzSpacer105: TRzSpacer
                Left = 610
                Top = 2
              end
              object RzSpacer106: TRzSpacer
                Left = 951
                Top = 2
              end
              object RzSpacer107: TRzSpacer
                Left = 764
                Top = 2
                Width = 5
              end
              object RzSpacer108: TRzSpacer
                Left = 472
                Top = 2
              end
              object RzToolButton81: TRzToolButton
                Left = 923
                Top = 2
                Width = 28
                SelectionColorStop = 16250613
                ImageIndex = 10
                ShowCaption = True
                UseToolbarShowCaption = False
              end
              object RzSpacer109: TRzSpacer
                Left = 550
                Top = 2
              end
              object RzToolButton82: TRzToolButton
                Left = 480
                Top = 2
                Width = 70
                SelectionColorStop = 16250613
                ImageIndex = 21
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #27719#24635#34920
                OnClick = RzToolButton82Click
              end
              object RzSpacer110: TRzSpacer
                Left = 915
                Top = 2
              end
              object RzSpacer111: TRzSpacer
                Left = 399
                Top = 2
              end
              object RzToolButton83: TRzToolButton
                Left = 311
                Top = 2
                Width = 88
                SelectionColorStop = 16250613
                ImageIndex = 16
                ShowCaption = True
                UseToolbarShowCaption = False
                Caption = #24037#31243#21517#32454#34920
                OnClick = RzToolButton83Click
              end
              object RzToolButton20: TRzToolButton
                Left = 558
                Top = 2
                Width = 52
                SelectionColorStop = 16250613
                ImageIndex = 21
                ShowCaption = True
                UseToolbarShowCaption = False
                Caption = #21512#21516
                OnClick = RzToolButton20Click
              end
              object RzSpacer19: TRzSpacer
                Left = 1024
                Top = 2
              end
              object cxLabel50: TcxLabel
                Left = 618
                Top = 5
                Caption = #36215#22987#26085#26399':'
                Transparent = True
              end
              object Date5: TcxDateEdit
                Left = 674
                Top = 3
                TabOrder = 1
                Width = 90
              end
              object cxLabel51: TcxLabel
                Left = 769
                Top = 5
                Caption = #32467#26463#26085#26399':'
                Transparent = True
              end
              object Date6: TcxDateEdit
                Left = 825
                Top = 3
                TabOrder = 3
                Width = 90
              end
            end
          end
          object t6: TRzTabSheet
            Color = 16250613
            Caption = #31199#36161#21333#20301
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 1399
              Height = 40
              Align = alTop
              BevelOuter = bvNone
              Caption = #31199#36161#21333#20301
              Color = 16250613
              ParentBackground = False
              TabOrder = 0
              object RzBitBtn42: TRzBitBtn
                Left = 37
                Top = 6
                Width = 67
                Height = 30
                FrameColor = 7617536
                Caption = #28155#21152
                Color = 15791348
                HotTrack = True
                TabOrder = 0
                OnClick = RzBitBtn42Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80909
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8E8E8E8E8E8091010
                  09E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E809101010
                  1009E8E8E8E8E8E8E8E8E8E881ACACACAC81E8E8E8E8E8E8E8E8E80910101010
                  101009E8E8E8E8E8E8E8E881ACACACACACAC81E8E8E8E8E8E8E8E80910100909
                  10101009E8E8E8E8E8E8E881ACAC8181ACACAC81E8E8E8E8E8E8E8091009E8E8
                  0910101009E8E8E8E8E8E881AC81E8E881ACACAC81E8E8E8E8E8E80909E8E8E8
                  E80910101009E8E8E8E8E88181E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8
                  E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8
                  E8E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E809101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E80909E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn43: TRzBitBtn
                Left = 110
                Top = 6
                Width = 67
                Height = 30
                FrameColor = 7617536
                Caption = #32534#36753
                Color = 15791348
                HotTrack = True
                TabOrder = 1
                OnClick = RzBitBtn42Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000520B0000520B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E81710101010
                  1010101010E8E8E8E8E8E8AC818181818181818181E8E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  E3E3E3E3E310E8E8E8E8AC81E3E3E3E3E3E3E3E3E381E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  ACACACACAC10E8E8E8E8AC81E3E3E3E3ACACACACAC81E8E8E8E81710D7D7D7D7
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7D7E3E3E3E3E381E8E8E8E81710E3E3E3AC
                  ACACACACAC10E8E8E8E8AC81E3E3E3ACACACACACAC81E8E8E8E81710D7D7D7E3
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7E3E3E3E3E3E381E8E8E8E81710E3E3ACAC
                  10101010101010101010AC81E3E3ACAC818181818181818181811710D7D7E310
                  17101010101010101010AC81D7D7E381AC8181818181818181811710E3AC1717
                  171717101010101010E8AC81E3ACACACACACAC818181818181E81710D7175F5F
                  1717171717101010E8E8AC81D7ACE3E3ACACACACAC818181E8E81710175F5F5F
                  5F5F1717171710E8E8E8AC81ACE3E3E3E3E3ACACACAC81E8E8E817175F5F5F5F
                  5F5F5F5F1710E8E8E8E8ACACE3E3E3E3E3E3E3E3AC81E8E8E8E8E81781D781D7
                  81D781D781D7E8E8E8E8E8AC81D781D781D781D781D7E8E8E8E8E8E881AC81AC
                  81AC81AC81E8E8E8E8E8E8E881AC81AC81AC81AC81E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn44: TRzBitBtn
                Left = 183
                Top = 6
                Width = 67
                Height = 30
                FrameColor = 7617536
                Caption = #21024#38500
                Color = 15791348
                HotTrack = True
                TabOrder = 2
                OnClick = RzBitBtn44Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
                  10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn45: TRzBitBtn
                Left = 650
                Top = 6
                Width = 65
                Height = 30
                FrameColor = 7617536
                Caption = 'Excel'
                Color = 15791348
                HotTrack = True
                TabOrder = 3
                OnClick = RzBitBtn13Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000B30B0000B30B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7ACAC
                  ACACD7D7D7D7D75EE8E8E8E881E8ACACACACE8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D709D7D7D75EE8E8E8E881E8E8E8E8E8E881E8E8E881E8E8E8E85ED7ACAC
                  D7D70909D7D7D75EE8E8E8E881E8ACACE8E88181E8E8E881E8E8E8E85ED7D7D7
                  D7090909095ED75EE8E8E8E881E8E8E8E881818181ACE881E8E8E8E85ED7ACAC
                  D7D70909D709D75EE8E8E8E881E8ACACE8E88181E881E881E8E8E8E85ED7D7D7
                  D7D7D709D709D75EE8E8E8E881E8E8E8E8E8E881E881E881E8E8E8E85ED7ACAC
                  ACD7D7D7D709D75EE8E8E8E881E8ACACACE8E8E8E881E881E8E8E8E85ED7D7D7
                  D7D7D7D7D709D75EE8E8E8E881E8E8E8E8E8E8E8E881E881E8E8E8E85ED7D7D7
                  09090909095ED75EE8E8E8E881E8E8E88181818181ACE881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn46: TRzBitBtn
                Left = 721
                Top = 6
                Width = 73
                Height = 30
                FrameColor = 7617536
                Caption = #25171#21360
                Color = 15791348
                HotTrack = True
                TabOrder = 4
                OnClick = RzBitBtn14Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000730E0000730E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909
                  09090909090909E8E8E8E8E88181818181818181818181E8E8E8E85E89898989
                  89898989895E5E09E8E8E8E2ACACACACACACACACACE2E281E8E85E5E5E5E5E5E
                  5E5E5E5E5E5E095E09E8E2E2E2E2E2E2E2E2E2E2E2E281E281E85ED789898989
                  8989898989895E0909E8E2E8ACACACACACACACACACACE28181E85ED789898989
                  181289B490895E5E09E8E2E8ACACACACE281ACE281ACE2E281E85ED7D7D7D7D7
                  D7D7D7D7D7D75E5E5E09E2E8E8E8E8E8E8E8E8E8E8E8E2E2E2815ED789898989
                  8989898989895E5E5E09E2E8ACACACACACACACACACACE2E2E281E85E5E5E5E5E
                  5E5E5E5E5E89895E5E09E8E2E2E2E2E2E2E2E2E2E2ACACE2E281E8E85ED7D7D7
                  D7D7D7D7D75E89895E09E8E8E2E8E8E8E8E8E8E8E8E2ACACE281E8E8E85ED7E3
                  E3E3E3E3D75E5E5E09E8E8E8E8E2E8ACACACACACE8E2E2E281E8E8E8E85ED7D7
                  D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85ED7
                  E3E3E3E3E3D75EE8E8E8E8E8E8E8E2E8ACACACACACE8E2E8E8E8E8E8E8E85ED7
                  D7D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85E
                  5E5E5E5E5E5E5E5EE8E8E8E8E8E8E8E2E2E2E2E2E2E2E2E2E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn47: TRzBitBtn
                Left = 804
                Top = 6
                Width = 73
                Height = 30
                FrameColor = 7617536
                Caption = #20851#38381
                Color = 15791348
                HotTrack = True
                TabOrder = 5
                OnClick = RzBitBtn21Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000420B0000420B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
                  D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
                NumGlyphs = 2
              end
            end
            object cxPageControl1: TcxPageControl
              Left = 0
              Top = 40
              Width = 1399
              Height = 477
              Align = alClient
              TabOrder = 1
              Properties.ActivePage = cxTabSheet1
              Properties.CustomButtons.Buttons = <>
              Properties.TabHeight = 26
              Properties.TabWidth = 150
              ClientRectBottom = 473
              ClientRectLeft = 4
              ClientRectRight = 1395
              ClientRectTop = 32
              object cxTabSheet1: TcxTabSheet
                Caption = #31199#36161#21333#20301
                ImageIndex = 0
                object DBGridEh8: TDBGridEh
                  Left = 0
                  Top = 0
                  Width = 1391
                  Height = 378
                  Align = alClient
                  ColumnDefValues.Layout = tlCenter
                  DataGrouping.Footers = <
                    item
                      ColumnItems = <
                        item
                        end
                        item
                        end
                        item
                        end>
                      Visible = False
                    end>
                  DataSource = DataLease
                  DrawMemoText = True
                  DynProps = <>
                  FixedColor = 16250613
                  Flat = True
                  FooterRowCount = 1
                  FooterParams.Color = clYellow
                  FooterParams.Font.Charset = DEFAULT_CHARSET
                  FooterParams.Font.Color = clRed
                  FooterParams.Font.Height = -11
                  FooterParams.Font.Name = 'Tahoma'
                  FooterParams.Font.Style = []
                  FooterParams.ParentFont = False
                  FooterParams.VertLines = False
                  IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                  OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghHotTrack, dghExtendVertLines]
                  PopupMenu = PopupMenu1
                  ReadOnly = True
                  RowDetailPanel.Active = True
                  RowDetailPanel.Height = 600
                  RowDetailPanel.Color = clBtnFace
                  RowHeight = 30
                  RowSizingAllowed = True
                  SumList.Active = True
                  TabOrder = 0
                  TitleParams.RowHeight = 8
                  TitleParams.RowLines = 1
                  OnKeyDown = DBGridEh1KeyDown
                  OnRowDetailPanelShow = DBGridEh8RowDetailPanelShow
                  Columns = <
                    item
                      Alignment = taCenter
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'numbers'
                      Footer.Value = #21512#35745':'
                      Footer.ValueType = fvtStaticText
                      Footers = <>
                      Title.Caption = #21333#21495
                      Title.TitleButton = True
                      Width = 71
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'caption'
                      Footers = <>
                      Title.Caption = #24037#31243#21517#31216
                      Title.TitleButton = True
                      Width = 89
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'content'
                      Footers = <>
                      Title.Caption = #31199#36161#21333#20301
                      Title.TitleButton = True
                      Width = 91
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      Footers = <>
                      Title.Caption = #26045#24037#21333#20301
                      Width = 80
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      Footers = <>
                      Title.Caption = #26426#26800#21517#31216
                      Width = 80
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'pay_time'
                      Footers = <>
                      Title.Caption = #26085#26399
                      Title.TitleButton = True
                      Width = 94
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'End_Time'
                      Footers = <>
                      Title.Caption = #32467#26463#26085#26399
                      Title.TitleButton = True
                      Visible = False
                      Width = 77
                    end
                    item
                      CellButtons = <>
                      DisplayFormat = #65509'0,0.00'
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'Price'
                      Footers = <>
                      Title.Caption = #21333#20215
                      Title.TitleButton = True
                      Width = 89
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'num'
                      Footer.FieldName = 'num'
                      Footer.ValueType = fvtSum
                      Footers = <>
                      Title.Caption = #25968#37327
                      Title.TitleButton = True
                      Width = 77
                    end
                    item
                      Alignment = taCenter
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'Units'
                      Footers = <>
                      Title.Caption = #21333#20301
                      Title.TitleButton = True
                      Width = 30
                    end
                    item
                      CellButtons = <>
                      DisplayFormat = #65509'0,0.00'
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 't'
                      Footer.DisplayFormat = #65509'#,0.##'
                      Footer.FieldName = 't'
                      Footer.ValueType = fvtSum
                      Footers = <>
                      Title.Caption = #24635#20215
                      Title.TitleButton = True
                      Width = 125
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'remarks'
                      Footers = <>
                      Title.Caption = #22791#27880
                      Title.TitleButton = True
                      Width = 278
                    end>
                  object RowDetailData: TRowDetailPanelControlEh
                    object RzPanel13: TRzPanel
                      Left = 0
                      Top = 0
                      Width = 1094
                      Height = 41
                      Align = alTop
                      BorderOuter = fsFlat
                      GradientColorStyle = gcsCustom
                      TabOrder = 0
                      VisualStyle = vsGradient
                      object RzLabel6: TRzLabel
                        Left = 105
                        Top = 12
                        Width = 28
                        Height = 14
                        Caption = #26597#35810':'
                        Color = 16250613
                        ParentColor = False
                        Transparent = True
                        WordWrap = True
                      end
                      object RzEdit13: TRzEdit
                        Left = 139
                        Top = 10
                        Width = 209
                        Height = 22
                        Text = ''
                        TabOrder = 0
                      end
                      object RzBitBtn76: TRzBitBtn
                        Left = 354
                        Top = 7
                        Width = 62
                        Height = 28
                        FrameColor = 7617536
                        Caption = #25628#32034
                        Color = 15791348
                        HotTrack = True
                        TabOrder = 1
                        OnClick = RzBitBtn76Click
                        Glyph.Data = {
                          36060000424D3606000000000000360400002800000020000000100000000100
                          08000000000000020000330B0000330B00000001000000000000000000003300
                          00006600000099000000CC000000FF0000000033000033330000663300009933
                          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                          000000990000339900006699000099990000CC990000FF99000000CC000033CC
                          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                          330000333300333333006633330099333300CC333300FF333300006633003366
                          33006666330099663300CC663300FF6633000099330033993300669933009999
                          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                          66006600660099006600CC006600FF0066000033660033336600663366009933
                          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                          660000996600339966006699660099996600CC996600FF99660000CC660033CC
                          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                          990000339900333399006633990099339900CC339900FF339900006699003366
                          99006666990099669900CC669900FF6699000099990033999900669999009999
                          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                          000000808000800000008000800080800000C0C0C00080808000191919004C4C
                          4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                          6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                          0000000000000000000000000000000000000000000000000000000000000000
                          0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                          E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                          E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                          E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                          E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                          81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                          7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                          E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                          8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                          88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                          89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                          E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                          E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                          88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                          82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                          AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                          E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                        NumGlyphs = 2
                      end
                      object RzBitBtn77: TRzBitBtn
                        Left = 422
                        Top = 7
                        Width = 84
                        Height = 28
                        FrameColor = 7617536
                        Caption = #25171#24320#30446#24405
                        Color = 15791348
                        HotTrack = True
                        TabOrder = 2
                        OnClick = RzBitBtn77Click
                        Glyph.Data = {
                          36060000424D3606000000000000360400002800000020000000100000000100
                          08000000000000020000330B0000330B00000001000000000000000000003300
                          00006600000099000000CC000000FF0000000033000033330000663300009933
                          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                          000000990000339900006699000099990000CC990000FF99000000CC000033CC
                          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                          330000333300333333006633330099333300CC333300FF333300006633003366
                          33006666330099663300CC663300FF6633000099330033993300669933009999
                          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                          66006600660099006600CC006600FF0066000033660033336600663366009933
                          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                          660000996600339966006699660099996600CC996600FF99660000CC660033CC
                          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                          990000339900333399006633990099339900CC339900FF339900006699003366
                          99006666990099669900CC669900FF6699000099990033999900669999009999
                          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                          000000808000800000008000800080800000C0C0C00080808000191919004C4C
                          4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                          6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                          0000000000000000000000000000000000000000000000000000000000000000
                          0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
                          787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
                          CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
                          D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
                          D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
                          D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
                          D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
                          D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
                          A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
                          D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
                          A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
                          E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                        NumGlyphs = 2
                      end
                      object RzBitBtn78: TRzBitBtn
                        Left = 9
                        Top = 7
                        Height = 28
                        FrameColor = 7617536
                        Caption = #19978#19968#32423
                        Color = 15791348
                        HotTrack = True
                        TabOrder = 3
                        OnClick = RzBitBtn78Click
                        Glyph.Data = {
                          36060000424D3606000000000000360400002800000020000000100000000100
                          08000000000000020000530B0000530B00000001000000000000000000003300
                          00006600000099000000CC000000FF0000000033000033330000663300009933
                          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                          000000990000339900006699000099990000CC990000FF99000000CC000033CC
                          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                          330000333300333333006633330099333300CC333300FF333300006633003366
                          33006666330099663300CC663300FF6633000099330033993300669933009999
                          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                          66006600660099006600CC006600FF0066000033660033336600663366009933
                          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                          660000996600339966006699660099996600CC996600FF99660000CC660033CC
                          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                          990000339900333399006633990099339900CC339900FF339900006699003366
                          99006666990099669900CC669900FF6699000099990033999900669999009999
                          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                          000000808000800000008000800080800000C0C0C00080808000191919004C4C
                          4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                          6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                          0000000000000000000000000000000000000000000000000000000000000000
                          0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E809
                          101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E80910
                          101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8091010
                          101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E809101010
                          101010101010101009E8E8E881ACACACACACACACACACACAC81E8E80910101010
                          101010101010101009E8E881ACACACACACACACACACACACAC81E8E8E809101010
                          101010101010101009E8E8E881ACACACACACACACACACACAC81E8E8E8E8091010
                          101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E8E8E80910
                          101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E809
                          101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E8E8E8
                          090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                        NumGlyphs = 2
                      end
                    end
                    object RzFileListBox6: TRzFileListBox
                      Left = 0
                      Top = 41
                      Width = 1094
                      Height = 260
                      Align = alClient
                      FileType = [ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive]
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ItemHeight = 18
                      ParentFont = False
                      TabOrder = 1
                      AllowOpen = True
                      FrameVisible = True
                      FramingPreference = fpCustomFraming
                    end
                  end
                end
                object cxGroupBox3: TcxGroupBox
                  Left = 0
                  Top = 378
                  Align = alBottom
                  Caption = #26597#35810
                  TabOrder = 1
                  Height = 63
                  Width = 1391
                  object cxLabel4: TcxLabel
                    Left = 478
                    Top = 24
                    Caption = #26085#26399
                  end
                  object cxDateEdit2: TcxDateEdit
                    Left = 512
                    Top = 24
                    Properties.OnCloseUp = cxDateEdit1PropertiesCloseUp
                    TabOrder = 1
                    TextHint = #36873#25321#26597#35810
                    Width = 111
                  end
                  object cxLabel5: TcxLabel
                    Left = 19
                    Top = 24
                    Caption = #26597#35810#21517#31216
                  end
                  object cxTextEdit4: TcxTextEdit
                    Left = 77
                    Top = 25
                    TabOrder = 3
                    TextHint = #24037#31243#21517#31216','#31199#36161#21333#20301','#26426#26800#21517#31216
                    OnKeyDown = cxTextEdit3KeyDown
                    Width = 395
                  end
                  object cxButton4: TcxButton
                    Left = 791
                    Top = 24
                    Width = 75
                    Height = 25
                    Hint = #27492#31867#26597#35810#26159','#20379#36151#21333#20301#12289#31867#21035#12289#26085#26399#19968#36215#26597#35810
                    Caption = #26597#35810
                    OptionsImage.Glyph.Data = {
                      36040000424D3604000000000000360000002800000010000000100000000100
                      200000000000000400000000000000000000000000000000000080584ABCB076
                      62FFAF7562FFB07461FFAF7460FFAF7460FFAF7360FFAE745FFFAE735FFFAE73
                      5FFFAE725EFFAD725EFF7D5143BD000000060000000000000000B57C69FFFAF5
                      F2FFF9F5F2FFF9F4F1FFF9F4F1FFF9F4F1FFF9F3F0FFF8F2F0FFF8F2EFFFF8F2
                      EEFFF8F2EEFFF7F1EDFFAE725FFF000000090000000000000000B77F6DFFFAF7
                      F4FF66564FFFF6EEEBFFBAAFA9FF66574FFFBAAEA9FFF5EEEAFFB9AFA9FF6656
                      4FFFBAAFA9FFF9F2EFFFAF7460FF000000080000000000000000B88270FFFBF8
                      F6FF695952FFF7EFECFF6A5952FFF7EFEBFF6A5952FFF7EFEBFF695A52FFF6EF
                      EBFF6A5952FFF9F5F1FFBE9182FF0000000F0000000D0000000ABA8572FFFCF9
                      F8FF6D5D55FFF7F1EDFF6C5C55FFF7F1ECFF6D5C55FFF7F0ECFF6D5D55FFF7F0
                      EBFF6C5C55FFF9F6F4FFCFB3AAFF0B14318029458CFA1D2F58A0BC8775FFFDFB
                      FAFF6F5E57FFF8F1EEFFBFB3AFFF6F5E57FFBEB3AEFFF7F1EDFFBEB3AEFF6F5E
                      57FFCAC2BEFFF2F1F0FF787590FF386BB2FF55AFF9FF325196F3BE8A78FFFDFC
                      FBFFF8F3F0FFF8F3EFFFF8F3EFFFF9F4F2FFF6F2F0FFF3F0EEFFEFECEBFFF1ED
                      ECFFEDEBE8FF8D9AB9FF4177BCFF64BCFFFF417AC3FF1726447EC08D7BFFFDFD
                      FCFFF8F4F0FFF9F3F0FFF9F6F4FFF1EDECFFBFA8A2FF9B756BFF8A5B4DFF9570
                      65FF867A85FF4C7FC1FF71C5FFFF4982C7FF142343810000000AC1907FFFFEFD
                      FDFFF9F4F2FFF9F6F3FFF2EFEDFFAC8A7FFFB79589FFE3D4CDFFF4ECE6FFE4D4
                      CDFFAF8E83FF837577FF5188C9FF1525457E0000000900000001C39281FFFEFE
                      FEFFF9F5F3FFF8F5F4FFC7B1AAFFBE9D93FFF1E4DDFFCDA384FFBD875CFFCFA7
                      86FFF0E3D8FFB29287FF84717AFF000000110000000100000000C49583FFFFFE
                      FEFFF9F6F4FFF6F4F3FFB28C80FFEDE2DEFFCC9B83FFD9955FFFF6B06DFFDBA2
                      69FFCDA382FFECE0DBFF7E584CE30000000A0000000000000000C59686FFFFFF
                      FFFFFAF7F4FFF5F4F3FFB3887AFFFAF6F5FFBC7E60FFF9D8C7FFFCDFCEFFF3AF
                      6CFFBE8961FFF9F4F0FF976D5EFA0000000A0000000000000000C79889FFFFFF
                      FFFFFFFFFFFFFAFAFAFFBD988CFFF0E7E3FFCB9A84FFDCAC96FFFCDFD0FFD894
                      5FFFCDA182FFEFE6E2FF8E695CE2000000080000000000000000947266BEC89A
                      8AFFC89A89FFDCC3BAFFC9ABA0FFCCAEA4FFF7EFEBFFCEA08AFFBC7C5CFFCEA2
                      8BFFF6EEE9FFC8AAA0FF513D3782000000040000000000000000000000000000
                      00010000000100000002020202098E7065C4CFB2A8FFF0E7E4FFFEFEFEFFEFE6
                      E4FFCEB2A8FF8A6B60C50202010A000000010000000000000000000000000000
                      00000000000000000000000000010202020856433D7898786DCFBA9185F99575
                      69CD54413B790202020900000001000000000000000000000000}
                    TabOrder = 4
                    OnClick = cxButton3Click
                  end
                  object cxLabel7: TcxLabel
                    Left = 631
                    Top = 24
                    Caption = #21046#21333#31867#21035
                  end
                  object cxComboBox5: TcxComboBox
                    Left = 689
                    Top = 23
                    Properties.Items.Strings = (
                      #20837#24211#21333
                      #20986#24211#21333)
                    Properties.OnCloseUp = cxComboBox3PropertiesCloseUp
                    TabOrder = 6
                    Text = #20837#24211#21333
                    TextHint = #21046#21333#31867#22411
                    Width = 89
                  end
                end
              end
              object cxTabSheet2: TcxTabSheet
                Caption = #26550#20307#31199#36161
                ImageIndex = 1
                object DBGridEh13: TDBGridEh
                  Left = 0
                  Top = 0
                  Width = 1391
                  Height = 378
                  Align = alClient
                  ColumnDefValues.Layout = tlCenter
                  DataGrouping.Footers = <
                    item
                      ColumnItems = <
                        item
                        end
                        item
                        end
                        item
                        end>
                      Visible = False
                    end>
                  DataSource = DataFrame
                  DrawMemoText = True
                  DynProps = <>
                  FixedColor = 16250613
                  Flat = True
                  FooterRowCount = 1
                  FooterParams.Color = clYellow
                  FooterParams.Font.Charset = DEFAULT_CHARSET
                  FooterParams.Font.Color = clRed
                  FooterParams.Font.Height = -11
                  FooterParams.Font.Name = 'Tahoma'
                  FooterParams.Font.Style = []
                  FooterParams.ParentFont = False
                  FooterParams.VertLines = False
                  IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                  OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghHotTrack, dghExtendVertLines]
                  PopupMenu = PopupMenu1
                  ReadOnly = True
                  RowDetailPanel.Active = True
                  RowDetailPanel.Height = 600
                  RowDetailPanel.Color = clBtnFace
                  RowHeight = 30
                  RowSizingAllowed = True
                  SumList.Active = True
                  TabOrder = 0
                  TitleParams.RowHeight = 8
                  TitleParams.RowLines = 1
                  OnDblClick = DBGridEh13DblClick
                  OnKeyDown = DBGridEh1KeyDown
                  OnRowDetailPanelShow = DBGridEh8RowDetailPanelShow
                  Columns = <
                    item
                      Alignment = taCenter
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'numbers'
                      Footer.Value = #21512#35745':'
                      Footer.ValueType = fvtStaticText
                      Footers = <>
                      Title.Caption = #21333#21495
                      Title.TitleButton = True
                      Width = 100
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'caption'
                      Footers = <>
                      Title.Caption = #24037#31243#21517#31216
                      Title.TitleButton = True
                      Width = 180
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'SupplyUnit'
                      Footers = <>
                      Title.Caption = #20379#36135#21333#20301
                      Width = 180
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      Footers = <>
                      Title.Caption = #36141#36135#21333#20301
                      Width = 80
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'Dates'
                      Footers = <>
                      Title.Caption = #26085#26399
                      Width = 80
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'UnitTypes'
                      Footers = <>
                      Title.Caption = #26448#26009#35268#26684
                      Width = 80
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'Unit'
                      Footers = <>
                      Title.Caption = #21333#20301
                      Width = 40
                    end
                    item
                      CellButtons = <>
                      Color = cl3DLight
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'Types'
                      Footers = <>
                      Title.Caption = #21046#21333#31867#21035
                      Width = 60
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'Delivery'
                      Footers = <>
                      Title.Caption = #36865#36135#20154
                      Width = 60
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'Num'
                      Footer.FieldName = 'Num'
                      Footer.ValueType = fvtSum
                      Footers = <>
                      Title.Caption = #25968#37327
                      Width = 60
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'Sign'
                      Footers = <>
                      Title.Caption = #31614#25910#20154
                      Width = 70
                    end
                    item
                      CellButtons = <>
                      DynProps = <>
                      EditButtons = <>
                      FieldName = 'remarks'
                      Footers = <>
                      Title.Caption = #22791#27880
                      Title.TitleButton = True
                      Width = 278
                    end>
                  object RowDetailData: TRowDetailPanelControlEh
                    object RzPanel19: TRzPanel
                      Left = 0
                      Top = 0
                      Width = 1259
                      Height = 41
                      Align = alTop
                      BorderOuter = fsFlat
                      GradientColorStyle = gcsCustom
                      TabOrder = 0
                      VisualStyle = vsGradient
                      object RzLabel11: TRzLabel
                        Left = 105
                        Top = 12
                        Width = 28
                        Height = 14
                        Caption = #26597#35810':'
                        Color = 16250613
                        ParentColor = False
                        Transparent = True
                        WordWrap = True
                      end
                      object RzEdit18: TRzEdit
                        Left = 139
                        Top = 10
                        Width = 209
                        Height = 22
                        Text = ''
                        TabOrder = 0
                      end
                      object RzBitBtn97: TRzBitBtn
                        Left = 354
                        Top = 7
                        Width = 62
                        Height = 28
                        FrameColor = 7617536
                        Caption = #25628#32034
                        Color = 15791348
                        HotTrack = True
                        TabOrder = 1
                        OnClick = RzBitBtn76Click
                        Glyph.Data = {
                          36060000424D3606000000000000360400002800000020000000100000000100
                          08000000000000020000330B0000330B00000001000000000000000000003300
                          00006600000099000000CC000000FF0000000033000033330000663300009933
                          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                          000000990000339900006699000099990000CC990000FF99000000CC000033CC
                          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                          330000333300333333006633330099333300CC333300FF333300006633003366
                          33006666330099663300CC663300FF6633000099330033993300669933009999
                          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                          66006600660099006600CC006600FF0066000033660033336600663366009933
                          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                          660000996600339966006699660099996600CC996600FF99660000CC660033CC
                          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                          990000339900333399006633990099339900CC339900FF339900006699003366
                          99006666990099669900CC669900FF6699000099990033999900669999009999
                          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                          000000808000800000008000800080800000C0C0C00080808000191919004C4C
                          4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                          6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                          0000000000000000000000000000000000000000000000000000000000000000
                          0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                          E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                          E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                          E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                          E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                          81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                          7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                          E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                          8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                          88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                          89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                          E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                          E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                          88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                          82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                          AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                          E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                        NumGlyphs = 2
                      end
                      object RzBitBtn98: TRzBitBtn
                        Left = 422
                        Top = 7
                        Width = 84
                        Height = 28
                        FrameColor = 7617536
                        Caption = #25171#24320#30446#24405
                        Color = 15791348
                        HotTrack = True
                        TabOrder = 2
                        OnClick = RzBitBtn77Click
                        Glyph.Data = {
                          36060000424D3606000000000000360400002800000020000000100000000100
                          08000000000000020000330B0000330B00000001000000000000000000003300
                          00006600000099000000CC000000FF0000000033000033330000663300009933
                          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                          000000990000339900006699000099990000CC990000FF99000000CC000033CC
                          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                          330000333300333333006633330099333300CC333300FF333300006633003366
                          33006666330099663300CC663300FF6633000099330033993300669933009999
                          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                          66006600660099006600CC006600FF0066000033660033336600663366009933
                          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                          660000996600339966006699660099996600CC996600FF99660000CC660033CC
                          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                          990000339900333399006633990099339900CC339900FF339900006699003366
                          99006666990099669900CC669900FF6699000099990033999900669999009999
                          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                          000000808000800000008000800080800000C0C0C00080808000191919004C4C
                          4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                          6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                          0000000000000000000000000000000000000000000000000000000000000000
                          0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
                          787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
                          CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
                          D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
                          D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
                          D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
                          D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
                          D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
                          A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
                          D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
                          A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
                          E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                        NumGlyphs = 2
                      end
                      object RzBitBtn99: TRzBitBtn
                        Left = 9
                        Top = 7
                        Height = 28
                        FrameColor = 7617536
                        Caption = #19978#19968#32423
                        Color = 15791348
                        HotTrack = True
                        TabOrder = 3
                        OnClick = RzBitBtn78Click
                        Glyph.Data = {
                          36060000424D3606000000000000360400002800000020000000100000000100
                          08000000000000020000530B0000530B00000001000000000000000000003300
                          00006600000099000000CC000000FF0000000033000033330000663300009933
                          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                          000000990000339900006699000099990000CC990000FF99000000CC000033CC
                          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                          330000333300333333006633330099333300CC333300FF333300006633003366
                          33006666330099663300CC663300FF6633000099330033993300669933009999
                          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                          66006600660099006600CC006600FF0066000033660033336600663366009933
                          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                          660000996600339966006699660099996600CC996600FF99660000CC660033CC
                          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                          990000339900333399006633990099339900CC339900FF339900006699003366
                          99006666990099669900CC669900FF6699000099990033999900669999009999
                          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                          000000808000800000008000800080800000C0C0C00080808000191919004C4C
                          4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                          6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                          0000000000000000000000000000000000000000000000000000000000000000
                          0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E809
                          101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E80910
                          101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8091010
                          101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E809101010
                          101010101010101009E8E8E881ACACACACACACACACACACAC81E8E80910101010
                          101010101010101009E8E881ACACACACACACACACACACACAC81E8E8E809101010
                          101010101010101009E8E8E881ACACACACACACACACACACAC81E8E8E8E8091010
                          101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E8E8E80910
                          101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E809
                          101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E8E8E8
                          090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                          E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                        NumGlyphs = 2
                      end
                    end
                    object RzFileListBox11: TRzFileListBox
                      Left = 0
                      Top = 41
                      Width = 1259
                      Height = 260
                      Align = alClient
                      FileType = [ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive]
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ItemHeight = 18
                      ParentFont = False
                      TabOrder = 1
                      AllowOpen = True
                      FrameVisible = True
                      FramingPreference = fpCustomFraming
                    end
                  end
                end
                object cxGroupBox1: TcxGroupBox
                  Left = 0
                  Top = 378
                  Align = alBottom
                  Caption = #26597#35810
                  TabOrder = 1
                  Height = 63
                  Width = 1391
                  object cxLabel1: TcxLabel
                    Left = 478
                    Top = 24
                    Caption = #26085#26399
                  end
                  object cxDateEdit1: TcxDateEdit
                    Left = 512
                    Top = 24
                    Properties.OnCloseUp = cxDateEdit1PropertiesCloseUp
                    TabOrder = 1
                    TextHint = #36873#25321#26597#35810
                    Width = 111
                  end
                  object cxLabel2: TcxLabel
                    Left = 19
                    Top = 24
                    Caption = #20379#36135#21333#20301
                  end
                  object cxLabel3: TcxLabel
                    Left = 328
                    Top = 24
                    Caption = #21333#20301#31867#21035
                  end
                  object cxComboBox1: TcxComboBox
                    Left = 386
                    Top = 24
                    Properties.OnCloseUp = cxComboBox1PropertiesCloseUp
                    TabOrder = 4
                    TextHint = #36873#25321#26597#35810
                    Width = 86
                  end
                  object cxTextEdit3: TcxTextEdit
                    Left = 77
                    Top = 25
                    TabOrder = 5
                    TextHint = #22238#36710#23601#21487#20197#26597#35810
                    OnKeyDown = cxTextEdit3KeyDown
                    Width = 244
                  end
                  object cxButton3: TcxButton
                    Left = 791
                    Top = 24
                    Width = 75
                    Height = 25
                    Hint = #27492#31867#26597#35810#26159','#20379#36151#21333#20301#12289#31867#21035#12289#26085#26399#19968#36215#26597#35810
                    Caption = #26597#35810
                    OptionsImage.Glyph.Data = {
                      36040000424D3604000000000000360000002800000010000000100000000100
                      200000000000000400000000000000000000000000000000000080584ABCB076
                      62FFAF7562FFB07461FFAF7460FFAF7460FFAF7360FFAE745FFFAE735FFFAE73
                      5FFFAE725EFFAD725EFF7D5143BD000000060000000000000000B57C69FFFAF5
                      F2FFF9F5F2FFF9F4F1FFF9F4F1FFF9F4F1FFF9F3F0FFF8F2F0FFF8F2EFFFF8F2
                      EEFFF8F2EEFFF7F1EDFFAE725FFF000000090000000000000000B77F6DFFFAF7
                      F4FF66564FFFF6EEEBFFBAAFA9FF66574FFFBAAEA9FFF5EEEAFFB9AFA9FF6656
                      4FFFBAAFA9FFF9F2EFFFAF7460FF000000080000000000000000B88270FFFBF8
                      F6FF695952FFF7EFECFF6A5952FFF7EFEBFF6A5952FFF7EFEBFF695A52FFF6EF
                      EBFF6A5952FFF9F5F1FFBE9182FF0000000F0000000D0000000ABA8572FFFCF9
                      F8FF6D5D55FFF7F1EDFF6C5C55FFF7F1ECFF6D5C55FFF7F0ECFF6D5D55FFF7F0
                      EBFF6C5C55FFF9F6F4FFCFB3AAFF0B14318029458CFA1D2F58A0BC8775FFFDFB
                      FAFF6F5E57FFF8F1EEFFBFB3AFFF6F5E57FFBEB3AEFFF7F1EDFFBEB3AEFF6F5E
                      57FFCAC2BEFFF2F1F0FF787590FF386BB2FF55AFF9FF325196F3BE8A78FFFDFC
                      FBFFF8F3F0FFF8F3EFFFF8F3EFFFF9F4F2FFF6F2F0FFF3F0EEFFEFECEBFFF1ED
                      ECFFEDEBE8FF8D9AB9FF4177BCFF64BCFFFF417AC3FF1726447EC08D7BFFFDFD
                      FCFFF8F4F0FFF9F3F0FFF9F6F4FFF1EDECFFBFA8A2FF9B756BFF8A5B4DFF9570
                      65FF867A85FF4C7FC1FF71C5FFFF4982C7FF142343810000000AC1907FFFFEFD
                      FDFFF9F4F2FFF9F6F3FFF2EFEDFFAC8A7FFFB79589FFE3D4CDFFF4ECE6FFE4D4
                      CDFFAF8E83FF837577FF5188C9FF1525457E0000000900000001C39281FFFEFE
                      FEFFF9F5F3FFF8F5F4FFC7B1AAFFBE9D93FFF1E4DDFFCDA384FFBD875CFFCFA7
                      86FFF0E3D8FFB29287FF84717AFF000000110000000100000000C49583FFFFFE
                      FEFFF9F6F4FFF6F4F3FFB28C80FFEDE2DEFFCC9B83FFD9955FFFF6B06DFFDBA2
                      69FFCDA382FFECE0DBFF7E584CE30000000A0000000000000000C59686FFFFFF
                      FFFFFAF7F4FFF5F4F3FFB3887AFFFAF6F5FFBC7E60FFF9D8C7FFFCDFCEFFF3AF
                      6CFFBE8961FFF9F4F0FF976D5EFA0000000A0000000000000000C79889FFFFFF
                      FFFFFFFFFFFFFAFAFAFFBD988CFFF0E7E3FFCB9A84FFDCAC96FFFCDFD0FFD894
                      5FFFCDA182FFEFE6E2FF8E695CE2000000080000000000000000947266BEC89A
                      8AFFC89A89FFDCC3BAFFC9ABA0FFCCAEA4FFF7EFEBFFCEA08AFFBC7C5CFFCEA2
                      8BFFF6EEE9FFC8AAA0FF513D3782000000040000000000000000000000000000
                      00010000000100000002020202098E7065C4CFB2A8FFF0E7E4FFFEFEFEFFEFE6
                      E4FFCEB2A8FF8A6B60C50202010A000000010000000000000000000000000000
                      00000000000000000000000000010202020856433D7898786DCFBA9185F99575
                      69CD54413B790202020900000001000000000000000000000000}
                    TabOrder = 6
                    OnClick = cxButton3Click
                  end
                  object cxLabel10: TcxLabel
                    Left = 631
                    Top = 24
                    Caption = #21046#21333#31867#21035
                  end
                  object cxComboBox3: TcxComboBox
                    Left = 689
                    Top = 23
                    Properties.Items.Strings = (
                      #20837#24211#21333
                      #20986#24211#21333)
                    Properties.OnCloseUp = cxComboBox3PropertiesCloseUp
                    TabOrder = 8
                    Text = #20837#24211#21333
                    TextHint = #21046#21333#31867#22411
                    Width = 89
                  end
                end
              end
            end
          end
          object t7: TRzTabSheet
            Color = 16250613
            Caption = #39033#30446#37096#36130#21153
            object Panel16: TPanel
              Left = 0
              Top = 0
              Width = 1399
              Height = 34
              Align = alTop
              BevelOuter = bvNone
              Caption = #39033#30446#37096#36130#21153
              Color = 16250613
              ParentBackground = False
              TabOrder = 0
              object RzBitBtn54: TRzBitBtn
                Left = 12
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #28155#21152
                Color = 15791348
                HotTrack = True
                TabOrder = 0
                OnClick = RzBitBtn54Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80909
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8E8E8E8E8E8091010
                  09E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E809101010
                  1009E8E8E8E8E8E8E8E8E8E881ACACACAC81E8E8E8E8E8E8E8E8E80910101010
                  101009E8E8E8E8E8E8E8E881ACACACACACAC81E8E8E8E8E8E8E8E80910100909
                  10101009E8E8E8E8E8E8E881ACAC8181ACACAC81E8E8E8E8E8E8E8091009E8E8
                  0910101009E8E8E8E8E8E881AC81E8E881ACACAC81E8E8E8E8E8E80909E8E8E8
                  E80910101009E8E8E8E8E88181E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8
                  E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8
                  E8E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E809101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E80909E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn55: TRzBitBtn
                Left = 85
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #32534#36753
                Color = 15791348
                HotTrack = True
                TabOrder = 1
                OnClick = RzBitBtn54Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000520B0000520B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E81710101010
                  1010101010E8E8E8E8E8E8AC818181818181818181E8E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  E3E3E3E3E310E8E8E8E8AC81E3E3E3E3E3E3E3E3E381E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  ACACACACAC10E8E8E8E8AC81E3E3E3E3ACACACACAC81E8E8E8E81710D7D7D7D7
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7D7E3E3E3E3E381E8E8E8E81710E3E3E3AC
                  ACACACACAC10E8E8E8E8AC81E3E3E3ACACACACACAC81E8E8E8E81710D7D7D7E3
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7E3E3E3E3E3E381E8E8E8E81710E3E3ACAC
                  10101010101010101010AC81E3E3ACAC818181818181818181811710D7D7E310
                  17101010101010101010AC81D7D7E381AC8181818181818181811710E3AC1717
                  171717101010101010E8AC81E3ACACACACACAC818181818181E81710D7175F5F
                  1717171717101010E8E8AC81D7ACE3E3ACACACACAC818181E8E81710175F5F5F
                  5F5F1717171710E8E8E8AC81ACE3E3E3E3E3ACACACAC81E8E8E817175F5F5F5F
                  5F5F5F5F1710E8E8E8E8ACACE3E3E3E3E3E3E3E3AC81E8E8E8E8E81781D781D7
                  81D781D781D7E8E8E8E8E8AC81D781D781D781D781D7E8E8E8E8E8E881AC81AC
                  81AC81AC81E8E8E8E8E8E8E881AC81AC81AC81AC81E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn56: TRzBitBtn
                Left = 158
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #21024#38500
                Color = 15791348
                HotTrack = True
                TabOrder = 2
                OnClick = RzBitBtn56Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
                  10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn57: TRzBitBtn
                Left = 388
                Top = 4
                Width = 65
                Height = 28
                FrameColor = 7617536
                Caption = 'Excel'
                Color = 15791348
                HotTrack = True
                TabOrder = 3
                OnClick = RzBitBtn13Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000B30B0000B30B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7ACAC
                  ACACD7D7D7D7D75EE8E8E8E881E8ACACACACE8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D709D7D7D75EE8E8E8E881E8E8E8E8E8E881E8E8E881E8E8E8E85ED7ACAC
                  D7D70909D7D7D75EE8E8E8E881E8ACACE8E88181E8E8E881E8E8E8E85ED7D7D7
                  D7090909095ED75EE8E8E8E881E8E8E8E881818181ACE881E8E8E8E85ED7ACAC
                  D7D70909D709D75EE8E8E8E881E8ACACE8E88181E881E881E8E8E8E85ED7D7D7
                  D7D7D709D709D75EE8E8E8E881E8E8E8E8E8E881E881E881E8E8E8E85ED7ACAC
                  ACD7D7D7D709D75EE8E8E8E881E8ACACACE8E8E8E881E881E8E8E8E85ED7D7D7
                  D7D7D7D7D709D75EE8E8E8E881E8E8E8E8E8E8E8E881E881E8E8E8E85ED7D7D7
                  09090909095ED75EE8E8E8E881E8E8E88181818181ACE881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn58: TRzBitBtn
                Left = 309
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #25171#21360
                Color = 15791348
                HotTrack = True
                TabOrder = 4
                OnClick = RzBitBtn14Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000730E0000730E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909
                  09090909090909E8E8E8E8E88181818181818181818181E8E8E8E85E89898989
                  89898989895E5E09E8E8E8E2ACACACACACACACACACE2E281E8E85E5E5E5E5E5E
                  5E5E5E5E5E5E095E09E8E2E2E2E2E2E2E2E2E2E2E2E281E281E85ED789898989
                  8989898989895E0909E8E2E8ACACACACACACACACACACE28181E85ED789898989
                  181289B490895E5E09E8E2E8ACACACACE281ACE281ACE2E281E85ED7D7D7D7D7
                  D7D7D7D7D7D75E5E5E09E2E8E8E8E8E8E8E8E8E8E8E8E2E2E2815ED789898989
                  8989898989895E5E5E09E2E8ACACACACACACACACACACE2E2E281E85E5E5E5E5E
                  5E5E5E5E5E89895E5E09E8E2E2E2E2E2E2E2E2E2E2ACACE2E281E8E85ED7D7D7
                  D7D7D7D7D75E89895E09E8E8E2E8E8E8E8E8E8E8E8E2ACACE281E8E8E85ED7E3
                  E3E3E3E3D75E5E5E09E8E8E8E8E2E8ACACACACACE8E2E2E281E8E8E8E85ED7D7
                  D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85ED7
                  E3E3E3E3E3D75EE8E8E8E8E8E8E8E2E8ACACACACACE8E2E8E8E8E8E8E8E85ED7
                  D7D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85E
                  5E5E5E5E5E5E5E5EE8E8E8E8E8E8E8E2E2E2E2E2E2E2E2E2E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn59: TRzBitBtn
                Left = 459
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #20851#38381
                Color = 15791348
                HotTrack = True
                TabOrder = 5
                OnClick = RzBitBtn21Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000420B0000420B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
                  D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn126: TRzBitBtn
                Left = 231
                Top = 4
                Width = 72
                Height = 28
                FrameColor = 7617536
                Caption = #26597#35810
                Color = 15791348
                HotTrack = True
                TabOrder = 6
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                  E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                  E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                  E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                  E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                  81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                  7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                  E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                  8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                  88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                  89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                  E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                  E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                  88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                  82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                  AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                  E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                NumGlyphs = 2
              end
            end
            object cxGroupBox9: TcxGroupBox
              Left = 0
              Top = 34
              Align = alTop
              Caption = #26597#35810
              TabOrder = 1
              Height = 57
              Width = 1399
              object Label34: TLabel
                Left = 24
                Top = 24
                Width = 52
                Height = 14
                Caption = #26597#35810#21517#31216':'
              end
              object Label35: TLabel
                Left = 335
                Top = 25
                Width = 52
                Height = 14
                Caption = #36215#22987#26085#26399':'
              end
              object Label36: TLabel
                Left = 484
                Top = 25
                Width = 52
                Height = 14
                Caption = #32467#26463#26085#26399':'
              end
              object cxTextEdit11: TcxTextEdit
                Left = 82
                Top = 22
                TabOrder = 0
                TextHint = #24037#31243#21517#31216','#39033#30446#37096#36130#21153
                OnKeyDown = cxTextEdit11KeyDown
                Width = 247
              end
              object cxDateEdit15: TcxDateEdit
                Left = 393
                Top = 23
                TabOrder = 1
                Width = 85
              end
              object cxDateEdit16: TcxDateEdit
                Left = 542
                Top = 23
                TabOrder = 2
                Width = 96
              end
              object cxButton11: TcxButton
                Left = 644
                Top = 19
                Width = 64
                Height = 30
                Caption = #26597#35810
                OptionsImage.Glyph.Data = {
                  36040000424D3604000000000000360000002800000010000000100000000100
                  2000000000000004000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000020000000B000000120000000C00000003000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000200000010071334970F276AFF0A193B970000000B000000000000
                  00007B5043B8AB705CFFAB6F5AFFAB705CFFAA6F5BFFAA6E59FFA96F5AFFBE91
                  82FFC9ACA3FF5F617FFF417CB9FF70C7FFFF265198FF00000010000000000000
                  0000AD735FFFFDFBF9FFFBF5F2FFF7F2EEFFF3EDE9FFEFE9E5FFECE5E1FFE6DE
                  DAFF707E9FFF4C83BCFF83CFFFFF5694CEFF142B4D930000000A000000000000
                  0000B07762FFFDFBFAFFF7F3F0FFE2D8D2FFA5816CFF8E5E42FF8C5D41FF7A5E
                  54FF577EA6FF92D4FAFF619CD0FF727F9BFF0000000E00000002000000000000
                  0000B07966FFFBF9F9FFE1D5CEFF936346FFC8A37FFFEFD7B2FFF0DAB8FFC7A6
                  88FF895D43FF6891B2FF849DB9FFCCB0A7FF0000000200000000000000000000
                  0000B37C69FFFAF8F7FFAD8975FFC7A07BFFF7D39CFFF5CD93FFF7D39BFFF9DD
                  B2FFC7A688FF84695DFFE8E2DEFFC29888FF0000000000000000000000000000
                  0000B67F6CFFF9F8F7FF98694CFFF1D4A7FFFAE5C0FFFBEACAFFF7D6A0FFF6D3
                  9BFFF2DBBBFF8F5D42FFF0E9E7FFB27A66FF0000000000000000000000000000
                  0000B98371FFFAF9F8FF9D6E51FFF2D4A5FFFDF6E2FFFDF3DCFFFBEACAFFF5CE
                  92FFF1DAB5FF936245FFF2EDE9FFB47D6AFF0000000000000000000000000000
                  0000BC8877FFFCFCFBFFB99783FFCDA77EFFF9E0B5FFFEF7E5FFFBE5C1FFF6D4
                  9DFFCAA681FFAF8C77FFF5F1EEFFB6806DFF0000000000000000000000000000
                  0000BF8C7AFFFDFDFCFFEDE4DFFFA87A5DFFCEA77FFFEFD2A3FFEFD2A5FFCCA7
                  80FFA17356FFE4DAD4FFFAF6F3FFB98371FF0000000000000000000000000000
                  0000C18F7FFFFEFEFEFFFDFCFBFFEDE4DFFFBE9C87FFAA7E62FFA97D60FFBB98
                  82FFEADFD8FFFBF8F6FFFDF9F8FFBD8774FF0000000000000000000000000000
                  0000C49382FFFFFEFEFFFEFEFDFFFBF6F4FFFAF5F3FFF9F3F0FFF9F3F0FFFAF2
                  F0FFFAF4F0FFFDFBF9FFFDFBF9FFBF8C7BFF0000000000000000000000000000
                  0000C79985FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFFEFC
                  FCFFFEFCFBFFFEFCFAFFFDFCFAFFC28F7FFF0000000000000000000000000000
                  0000C99A89FFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
                  FDFFFEFEFDFFFEFDFDFFFEFDFDFFC49382FF0000000000000000000000000000
                  0000967467BDCA9C8BFFCA9C8BFFC99C8AFFC99B89FFC99B8AFFCA9A88FFC89A
                  88FFC99987FFC79887FFC89886FF927163BD0000000000000000}
                TabOrder = 3
                OnClick = cxButton11Click
              end
            end
            object DBGridEh10: TDBGridEh
              Left = 0
              Top = 91
              Width = 1399
              Height = 426
              Align = alClient
              ColumnDefValues.Layout = tlCenter
              DataGrouping.Footers = <
                item
                  ColumnItems = <
                    item
                    end
                    item
                    end
                    item
                    end>
                  Visible = False
                end>
              DataSource = DataFinance
              DrawMemoText = True
              DynProps = <>
              FixedColor = 16250613
              Flat = True
              FooterRowCount = 1
              FooterParams.Color = clYellow
              FooterParams.Font.Charset = DEFAULT_CHARSET
              FooterParams.Font.Color = clRed
              FooterParams.Font.Height = -11
              FooterParams.Font.Name = 'Tahoma'
              FooterParams.Font.Style = []
              FooterParams.ParentFont = False
              FooterParams.VertLines = False
              IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghHotTrack, dghExtendVertLines]
              PopupMenu = PopupMenu1
              ReadOnly = True
              RowDetailPanel.Active = True
              RowDetailPanel.Height = 600
              RowDetailPanel.Color = clBtnFace
              RowHeight = 30
              RowSizingAllowed = True
              SumList.Active = True
              TabOrder = 2
              TitleParams.RowHeight = 8
              TitleParams.RowLines = 1
              OnKeyDown = DBGridEh1KeyDown
              OnRowDetailPanelShow = DBGridEh10RowDetailPanelShow
              Columns = <
                item
                  Alignment = taCenter
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'numbers'
                  Footer.Value = #21512#35745':'
                  Footer.ValueType = fvtStaticText
                  Footers = <>
                  Title.Alignment = taCenter
                  Title.Caption = #32534#21495
                  Title.TitleButton = True
                  Width = 110
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'caption'
                  Footers = <>
                  Title.Caption = #24037#31243#21517#31216
                  Title.TitleButton = True
                  Width = 89
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'content'
                  Footers = <>
                  Title.Caption = #39033#30446#37096#36130#21153
                  Title.TitleButton = True
                  Width = 123
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'pay_time'
                  Footers = <>
                  Title.Caption = #36215#22987#26085#26399
                  Title.TitleButton = True
                  Width = 95
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'End_Time'
                  Footers = <>
                  Title.Caption = #32467#26463#26085#26399
                  Title.TitleButton = True
                  Width = 91
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Price'
                  Footers = <>
                  Title.Caption = #21333#20215
                  Title.TitleButton = True
                  Width = 89
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'num'
                  Footer.FieldName = 'num'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #25968#37327
                  Title.TitleButton = True
                  Width = 63
                end
                item
                  Alignment = taCenter
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Units'
                  Footers = <>
                  Title.Caption = #21333#20301
                  Title.TitleButton = True
                  Width = 30
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 't'
                  Footer.DisplayFormat = #65509'#,0.##'
                  Footer.FieldName = 't'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #24635#20215
                  Title.TitleButton = True
                  Width = 133
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'remarks'
                  Footers = <>
                  Title.Caption = #22791#27880
                  Title.TitleButton = True
                  Width = 278
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object RzPanel14: TRzPanel
                  Left = 0
                  Top = 0
                  Width = 1090
                  Height = 41
                  Align = alTop
                  BorderOuter = fsFlat
                  GradientColorStyle = gcsCustom
                  TabOrder = 0
                  VisualStyle = vsGradient
                  object RzLabel7: TRzLabel
                    Left = 105
                    Top = 12
                    Width = 28
                    Height = 14
                    Caption = #26597#35810':'
                    Color = 16250613
                    ParentColor = False
                    Transparent = True
                    WordWrap = True
                  end
                  object RzEdit14: TRzEdit
                    Left = 139
                    Top = 10
                    Width = 209
                    Height = 22
                    Text = ''
                    TabOrder = 0
                  end
                  object RzBitBtn79: TRzBitBtn
                    Left = 354
                    Top = 7
                    Width = 62
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25628#32034
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 1
                    OnClick = RzBitBtn79Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                      E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                      E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                      E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                      E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                      81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                      7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                      E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                      8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                      88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                      89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                      E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                      E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                      88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                      82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                      AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                      E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn80: TRzBitBtn
                    Left = 422
                    Top = 7
                    Width = 84
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25171#24320#30446#24405
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 2
                    OnClick = RzBitBtn80Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
                      787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
                      CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
                      D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
                      D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
                      D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
                      D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
                      D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
                      A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
                      D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
                      A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
                      E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn81: TRzBitBtn
                    Left = 9
                    Top = 7
                    Height = 28
                    FrameColor = 7617536
                    Caption = #19978#19968#32423
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 3
                    OnClick = RzBitBtn81Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000530B0000530B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E80910101010
                      101010101010101009E8E881ACACACACACACACACACACACAC81E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                end
                object RzFileListBox7: TRzFileListBox
                  Left = 0
                  Top = 41
                  Width = 1090
                  Height = 308
                  Align = alClient
                  FileType = [ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ItemHeight = 18
                  ParentFont = False
                  TabOrder = 1
                  AllowOpen = True
                  FrameVisible = True
                  FramingPreference = fpCustomFraming
                end
              end
            end
          end
          object t8: TRzTabSheet
            Color = 16250613
            Caption = #29616#37329#20379#24212#21333#20301
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 1399
              Height = 32
              Align = alTop
              BevelOuter = bvNone
              Caption = #29616#37329#20379#24212#21333#20301
              Color = 16250613
              ParentBackground = False
              TabOrder = 0
              object RzBitBtn48: TRzBitBtn
                Left = 12
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #28155#21152
                Color = 15791348
                HotTrack = True
                TabOrder = 0
                OnClick = RzBitBtn48Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80909
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8E8E8E8E8E8091010
                  09E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E809101010
                  1009E8E8E8E8E8E8E8E8E8E881ACACACAC81E8E8E8E8E8E8E8E8E80910101010
                  101009E8E8E8E8E8E8E8E881ACACACACACAC81E8E8E8E8E8E8E8E80910100909
                  10101009E8E8E8E8E8E8E881ACAC8181ACACAC81E8E8E8E8E8E8E8091009E8E8
                  0910101009E8E8E8E8E8E881AC81E8E881ACACAC81E8E8E8E8E8E80909E8E8E8
                  E80910101009E8E8E8E8E88181E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8
                  E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8
                  E8E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E809101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E80909E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn49: TRzBitBtn
                Left = 85
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #32534#36753
                Color = 15791348
                HotTrack = True
                TabOrder = 1
                OnClick = RzBitBtn49Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000520B0000520B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E81710101010
                  1010101010E8E8E8E8E8E8AC818181818181818181E8E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  E3E3E3E3E310E8E8E8E8AC81E3E3E3E3E3E3E3E3E381E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  ACACACACAC10E8E8E8E8AC81E3E3E3E3ACACACACAC81E8E8E8E81710D7D7D7D7
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7D7E3E3E3E3E381E8E8E8E81710E3E3E3AC
                  ACACACACAC10E8E8E8E8AC81E3E3E3ACACACACACAC81E8E8E8E81710D7D7D7E3
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7E3E3E3E3E3E381E8E8E8E81710E3E3ACAC
                  10101010101010101010AC81E3E3ACAC818181818181818181811710D7D7E310
                  17101010101010101010AC81D7D7E381AC8181818181818181811710E3AC1717
                  171717101010101010E8AC81E3ACACACACACAC818181818181E81710D7175F5F
                  1717171717101010E8E8AC81D7ACE3E3ACACACACAC818181E8E81710175F5F5F
                  5F5F1717171710E8E8E8AC81ACE3E3E3E3E3ACACACAC81E8E8E817175F5F5F5F
                  5F5F5F5F1710E8E8E8E8ACACE3E3E3E3E3E3E3E3AC81E8E8E8E8E81781D781D7
                  81D781D781D7E8E8E8E8E8AC81D781D781D781D781D7E8E8E8E8E8E881AC81AC
                  81AC81AC81E8E8E8E8E8E8E881AC81AC81AC81AC81E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn50: TRzBitBtn
                Left = 158
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #21024#38500
                Color = 15791348
                HotTrack = True
                TabOrder = 2
                OnClick = RzBitBtn50Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
                  10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn51: TRzBitBtn
                Left = 309
                Top = 4
                Width = 65
                Height = 28
                FrameColor = 7617536
                Caption = 'Excel'
                Color = 15791348
                HotTrack = True
                TabOrder = 3
                OnClick = RzBitBtn13Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000B30B0000B30B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7ACAC
                  ACACD7D7D7D7D75EE8E8E8E881E8ACACACACE8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D709D7D7D75EE8E8E8E881E8E8E8E8E8E881E8E8E881E8E8E8E85ED7ACAC
                  D7D70909D7D7D75EE8E8E8E881E8ACACE8E88181E8E8E881E8E8E8E85ED7D7D7
                  D7090909095ED75EE8E8E8E881E8E8E8E881818181ACE881E8E8E8E85ED7ACAC
                  D7D70909D709D75EE8E8E8E881E8ACACE8E88181E881E881E8E8E8E85ED7D7D7
                  D7D7D709D709D75EE8E8E8E881E8E8E8E8E8E881E881E881E8E8E8E85ED7ACAC
                  ACD7D7D7D709D75EE8E8E8E881E8ACACACE8E8E8E881E881E8E8E8E85ED7D7D7
                  D7D7D7D7D709D75EE8E8E8E881E8E8E8E8E8E8E8E881E881E8E8E8E85ED7D7D7
                  09090909095ED75EE8E8E8E881E8E8E88181818181ACE881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn52: TRzBitBtn
                Left = 380
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #25171#21360
                Color = 15791348
                HotTrack = True
                TabOrder = 4
                OnClick = RzBitBtn14Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000730E0000730E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909
                  09090909090909E8E8E8E8E88181818181818181818181E8E8E8E85E89898989
                  89898989895E5E09E8E8E8E2ACACACACACACACACACE2E281E8E85E5E5E5E5E5E
                  5E5E5E5E5E5E095E09E8E2E2E2E2E2E2E2E2E2E2E2E281E281E85ED789898989
                  8989898989895E0909E8E2E8ACACACACACACACACACACE28181E85ED789898989
                  181289B490895E5E09E8E2E8ACACACACE281ACE281ACE2E281E85ED7D7D7D7D7
                  D7D7D7D7D7D75E5E5E09E2E8E8E8E8E8E8E8E8E8E8E8E2E2E2815ED789898989
                  8989898989895E5E5E09E2E8ACACACACACACACACACACE2E2E281E85E5E5E5E5E
                  5E5E5E5E5E89895E5E09E8E2E2E2E2E2E2E2E2E2E2ACACE2E281E8E85ED7D7D7
                  D7D7D7D7D75E89895E09E8E8E2E8E8E8E8E8E8E8E8E2ACACE281E8E8E85ED7E3
                  E3E3E3E3D75E5E5E09E8E8E8E8E2E8ACACACACACE8E2E2E281E8E8E8E85ED7D7
                  D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85ED7
                  E3E3E3E3E3D75EE8E8E8E8E8E8E8E2E8ACACACACACE8E2E8E8E8E8E8E8E85ED7
                  D7D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85E
                  5E5E5E5E5E5E5E5EE8E8E8E8E8E8E8E2E2E2E2E2E2E2E2E2E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn53: TRzBitBtn
                Left = 459
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #20851#38381
                Color = 15791348
                HotTrack = True
                TabOrder = 5
                OnClick = RzBitBtn21Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000420B0000420B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
                  D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn127: TRzBitBtn
                Left = 231
                Top = 4
                Width = 72
                Height = 28
                FrameColor = 7617536
                Caption = #26597#35810
                Color = 15791348
                HotTrack = True
                TabOrder = 6
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                  E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                  E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                  E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                  E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                  81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                  7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                  E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                  8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                  88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                  89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                  E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                  E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                  88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                  82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                  AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                  E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                NumGlyphs = 2
              end
            end
            object cxGroupBox10: TcxGroupBox
              Left = 0
              Top = 32
              Align = alTop
              Caption = #26597#35810
              TabOrder = 1
              Height = 57
              Width = 1399
              object Label37: TLabel
                Left = 24
                Top = 24
                Width = 52
                Height = 14
                Caption = #26597#35810#21517#31216':'
              end
              object Label38: TLabel
                Left = 335
                Top = 25
                Width = 52
                Height = 14
                Caption = #36215#22987#26085#26399':'
              end
              object Label39: TLabel
                Left = 484
                Top = 25
                Width = 52
                Height = 14
                Caption = #32467#26463#26085#26399':'
              end
              object cxTextEdit12: TcxTextEdit
                Left = 82
                Top = 21
                TabOrder = 0
                TextHint = #24037#31243#21517#31216','#20379#36135#21333#20301','#36141#36135#21333#20301
                OnKeyDown = cxTextEdit12KeyDown
                Width = 247
              end
              object cxDateEdit17: TcxDateEdit
                Left = 393
                Top = 23
                TabOrder = 1
                Width = 85
              end
              object cxDateEdit18: TcxDateEdit
                Left = 542
                Top = 23
                TabOrder = 2
                Width = 96
              end
              object cxButton12: TcxButton
                Left = 644
                Top = 19
                Width = 64
                Height = 30
                Caption = #26597#35810
                OptionsImage.Glyph.Data = {
                  36040000424D3604000000000000360000002800000010000000100000000100
                  2000000000000004000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000020000000B000000120000000C00000003000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000200000010071334970F276AFF0A193B970000000B000000000000
                  00007B5043B8AB705CFFAB6F5AFFAB705CFFAA6F5BFFAA6E59FFA96F5AFFBE91
                  82FFC9ACA3FF5F617FFF417CB9FF70C7FFFF265198FF00000010000000000000
                  0000AD735FFFFDFBF9FFFBF5F2FFF7F2EEFFF3EDE9FFEFE9E5FFECE5E1FFE6DE
                  DAFF707E9FFF4C83BCFF83CFFFFF5694CEFF142B4D930000000A000000000000
                  0000B07762FFFDFBFAFFF7F3F0FFE2D8D2FFA5816CFF8E5E42FF8C5D41FF7A5E
                  54FF577EA6FF92D4FAFF619CD0FF727F9BFF0000000E00000002000000000000
                  0000B07966FFFBF9F9FFE1D5CEFF936346FFC8A37FFFEFD7B2FFF0DAB8FFC7A6
                  88FF895D43FF6891B2FF849DB9FFCCB0A7FF0000000200000000000000000000
                  0000B37C69FFFAF8F7FFAD8975FFC7A07BFFF7D39CFFF5CD93FFF7D39BFFF9DD
                  B2FFC7A688FF84695DFFE8E2DEFFC29888FF0000000000000000000000000000
                  0000B67F6CFFF9F8F7FF98694CFFF1D4A7FFFAE5C0FFFBEACAFFF7D6A0FFF6D3
                  9BFFF2DBBBFF8F5D42FFF0E9E7FFB27A66FF0000000000000000000000000000
                  0000B98371FFFAF9F8FF9D6E51FFF2D4A5FFFDF6E2FFFDF3DCFFFBEACAFFF5CE
                  92FFF1DAB5FF936245FFF2EDE9FFB47D6AFF0000000000000000000000000000
                  0000BC8877FFFCFCFBFFB99783FFCDA77EFFF9E0B5FFFEF7E5FFFBE5C1FFF6D4
                  9DFFCAA681FFAF8C77FFF5F1EEFFB6806DFF0000000000000000000000000000
                  0000BF8C7AFFFDFDFCFFEDE4DFFFA87A5DFFCEA77FFFEFD2A3FFEFD2A5FFCCA7
                  80FFA17356FFE4DAD4FFFAF6F3FFB98371FF0000000000000000000000000000
                  0000C18F7FFFFEFEFEFFFDFCFBFFEDE4DFFFBE9C87FFAA7E62FFA97D60FFBB98
                  82FFEADFD8FFFBF8F6FFFDF9F8FFBD8774FF0000000000000000000000000000
                  0000C49382FFFFFEFEFFFEFEFDFFFBF6F4FFFAF5F3FFF9F3F0FFF9F3F0FFFAF2
                  F0FFFAF4F0FFFDFBF9FFFDFBF9FFBF8C7BFF0000000000000000000000000000
                  0000C79985FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFFEFC
                  FCFFFEFCFBFFFEFCFAFFFDFCFAFFC28F7FFF0000000000000000000000000000
                  0000C99A89FFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
                  FDFFFEFEFDFFFEFDFDFFFEFDFDFFC49382FF0000000000000000000000000000
                  0000967467BDCA9C8BFFCA9C8BFFC99C8AFFC99B89FFC99B8AFFCA9A88FFC89A
                  88FFC99987FFC79887FFC89886FF927163BD0000000000000000}
                TabOrder = 3
                OnClick = cxButton12Click
              end
            end
            object DBGridEh9: TDBGridEh
              Left = 0
              Top = 89
              Width = 1399
              Height = 428
              Align = alClient
              ColumnDefValues.Layout = tlCenter
              DataGrouping.Footers = <
                item
                  ColumnItems = <
                    item
                    end
                    item
                    end
                    item
                    end>
                  Visible = False
                end>
              DataSource = DataCash
              DrawMemoText = True
              DynProps = <>
              FixedColor = 16250613
              Flat = True
              FooterRowCount = 1
              FooterParams.Color = clYellow
              FooterParams.Font.Charset = DEFAULT_CHARSET
              FooterParams.Font.Color = clRed
              FooterParams.Font.Height = -11
              FooterParams.Font.Name = 'Tahoma'
              FooterParams.Font.Style = []
              FooterParams.ParentFont = False
              FooterParams.VertLines = False
              IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghHotTrack, dghExtendVertLines]
              PopupMenu = PopupMenu1
              ReadOnly = True
              RowDetailPanel.Active = True
              RowDetailPanel.Height = 600
              RowDetailPanel.Color = clBtnFace
              RowHeight = 30
              RowSizingAllowed = True
              SumList.Active = True
              TabOrder = 2
              TitleParams.RowHeight = 8
              TitleParams.RowLines = 1
              OnKeyDown = DBGridEh1KeyDown
              OnRowDetailPanelShow = DBGridEh9RowDetailPanelShow
              Columns = <
                item
                  Alignment = taCenter
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'numbers'
                  Footer.Value = #21512#35745':'
                  Footer.ValueType = fvtStaticText
                  Footers = <>
                  Title.Alignment = taCenter
                  Title.Caption = #32534#21495
                  Title.TitleButton = True
                  Width = 110
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'caption'
                  Footers = <>
                  Title.Caption = #24037#31243#21517#31216
                  Title.TitleButton = True
                  Width = 89
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'content'
                  Footers = <>
                  Title.Caption = #20379#36135#21333#20301
                  Title.TitleButton = True
                  Width = 123
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'PurchaseCompany'
                  Footers = <>
                  Title.Caption = #36141#36135#21333#20301
                  Width = 80
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'pay_time'
                  Footers = <>
                  Title.Caption = #26085#26399
                  Title.TitleButton = True
                  Width = 90
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'End_Time'
                  Footers = <>
                  Title.Caption = #32467#26463#26085#26399
                  Title.TitleButton = True
                  Visible = False
                  Width = 91
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Price'
                  Footers = <>
                  Title.Caption = #21333#20215
                  Title.TitleButton = True
                  Width = 89
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'num'
                  Footer.FieldName = 'num'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #25968#37327
                  Title.TitleButton = True
                  Width = 63
                end
                item
                  Alignment = taCenter
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Units'
                  Footers = <>
                  Title.Caption = #21333#20301
                  Title.TitleButton = True
                  Width = 30
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 't'
                  Footer.DisplayFormat = #65509'#,0.##'
                  Footer.FieldName = 't'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #24635#20215
                  Title.TitleButton = True
                  Width = 133
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'remarks'
                  Footers = <>
                  Title.Caption = #22791#27880
                  Title.TitleButton = True
                  Width = 278
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object RzPanel15: TRzPanel
                  Left = 0
                  Top = 0
                  Width = 1074
                  Height = 41
                  Align = alTop
                  BorderOuter = fsFlat
                  GradientColorStyle = gcsCustom
                  TabOrder = 0
                  VisualStyle = vsGradient
                  object RzLabel8: TRzLabel
                    Left = 105
                    Top = 12
                    Width = 28
                    Height = 14
                    Caption = #26597#35810':'
                    Color = 16250613
                    ParentColor = False
                    Transparent = True
                    WordWrap = True
                  end
                  object RzEdit15: TRzEdit
                    Left = 139
                    Top = 10
                    Width = 209
                    Height = 22
                    Text = ''
                    TabOrder = 0
                  end
                  object RzBitBtn82: TRzBitBtn
                    Left = 354
                    Top = 7
                    Width = 62
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25628#32034
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 1
                    OnClick = RzBitBtn82Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                      E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                      E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                      E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                      E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                      81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                      7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                      E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                      8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                      88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                      89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                      E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                      E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                      88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                      82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                      AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                      E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn83: TRzBitBtn
                    Left = 422
                    Top = 7
                    Width = 84
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25171#24320#30446#24405
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 2
                    OnClick = RzBitBtn83Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
                      787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
                      CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
                      D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
                      D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
                      D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
                      D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
                      D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
                      A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
                      D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
                      A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
                      E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn84: TRzBitBtn
                    Left = 9
                    Top = 7
                    Height = 28
                    FrameColor = 7617536
                    Caption = #19978#19968#32423
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 3
                    OnClick = RzBitBtn84Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000530B0000530B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E80910101010
                      101010101010101009E8E881ACACACACACACACACACACACAC81E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                end
                object RzFileListBox8: TRzFileListBox
                  Left = 0
                  Top = 41
                  Width = 1074
                  Height = 310
                  Align = alClient
                  FileType = [ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ItemHeight = 18
                  ParentFont = False
                  TabOrder = 1
                  AllowOpen = True
                  FrameVisible = True
                  FramingPreference = fpCustomFraming
                end
              end
            end
          end
          object t9: TRzTabSheet
            Color = 16250613
            Caption = #38050#26448#20379#24212#21333#20301
            object cxGroupBox11: TcxGroupBox
              Left = 0
              Top = 32
              Align = alTop
              Caption = #26597#35810
              TabOrder = 0
              Height = 57
              Width = 1399
              object Label40: TLabel
                Left = 24
                Top = 24
                Width = 52
                Height = 14
                Caption = #26597#35810#21517#31216':'
              end
              object Label41: TLabel
                Left = 487
                Top = 27
                Width = 52
                Height = 14
                Caption = #36215#22987#26085#26399':'
              end
              object Label42: TLabel
                Left = 636
                Top = 27
                Width = 52
                Height = 14
                Caption = #32467#26463#26085#26399':'
              end
              object Label47: TLabel
                Left = 339
                Top = 27
                Width = 52
                Height = 14
                Caption = #38050#31563#22411#21495':'
              end
              object cxTextEdit13: TcxTextEdit
                Left = 82
                Top = 22
                TabOrder = 0
                TextHint = #24037#31243#21517#31216','#20379#36135#21333#20301
                OnKeyDown = cxTextEdit13KeyDown
                Width = 247
              end
              object cxDateEdit19: TcxDateEdit
                Left = 545
                Top = 25
                TabOrder = 1
                Width = 85
              end
              object cxDateEdit20: TcxDateEdit
                Left = 694
                Top = 25
                TabOrder = 2
                Width = 96
              end
              object cxButton13: TcxButton
                Left = 796
                Top = 21
                Width = 64
                Height = 30
                Caption = #26597#35810
                OptionsImage.Glyph.Data = {
                  36040000424D3604000000000000360000002800000010000000100000000100
                  2000000000000004000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000020000000B000000120000000C00000003000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000200000010071334970F276AFF0A193B970000000B000000000000
                  00007B5043B8AB705CFFAB6F5AFFAB705CFFAA6F5BFFAA6E59FFA96F5AFFBE91
                  82FFC9ACA3FF5F617FFF417CB9FF70C7FFFF265198FF00000010000000000000
                  0000AD735FFFFDFBF9FFFBF5F2FFF7F2EEFFF3EDE9FFEFE9E5FFECE5E1FFE6DE
                  DAFF707E9FFF4C83BCFF83CFFFFF5694CEFF142B4D930000000A000000000000
                  0000B07762FFFDFBFAFFF7F3F0FFE2D8D2FFA5816CFF8E5E42FF8C5D41FF7A5E
                  54FF577EA6FF92D4FAFF619CD0FF727F9BFF0000000E00000002000000000000
                  0000B07966FFFBF9F9FFE1D5CEFF936346FFC8A37FFFEFD7B2FFF0DAB8FFC7A6
                  88FF895D43FF6891B2FF849DB9FFCCB0A7FF0000000200000000000000000000
                  0000B37C69FFFAF8F7FFAD8975FFC7A07BFFF7D39CFFF5CD93FFF7D39BFFF9DD
                  B2FFC7A688FF84695DFFE8E2DEFFC29888FF0000000000000000000000000000
                  0000B67F6CFFF9F8F7FF98694CFFF1D4A7FFFAE5C0FFFBEACAFFF7D6A0FFF6D3
                  9BFFF2DBBBFF8F5D42FFF0E9E7FFB27A66FF0000000000000000000000000000
                  0000B98371FFFAF9F8FF9D6E51FFF2D4A5FFFDF6E2FFFDF3DCFFFBEACAFFF5CE
                  92FFF1DAB5FF936245FFF2EDE9FFB47D6AFF0000000000000000000000000000
                  0000BC8877FFFCFCFBFFB99783FFCDA77EFFF9E0B5FFFEF7E5FFFBE5C1FFF6D4
                  9DFFCAA681FFAF8C77FFF5F1EEFFB6806DFF0000000000000000000000000000
                  0000BF8C7AFFFDFDFCFFEDE4DFFFA87A5DFFCEA77FFFEFD2A3FFEFD2A5FFCCA7
                  80FFA17356FFE4DAD4FFFAF6F3FFB98371FF0000000000000000000000000000
                  0000C18F7FFFFEFEFEFFFDFCFBFFEDE4DFFFBE9C87FFAA7E62FFA97D60FFBB98
                  82FFEADFD8FFFBF8F6FFFDF9F8FFBD8774FF0000000000000000000000000000
                  0000C49382FFFFFEFEFFFEFEFDFFFBF6F4FFFAF5F3FFF9F3F0FFF9F3F0FFFAF2
                  F0FFFAF4F0FFFDFBF9FFFDFBF9FFBF8C7BFF0000000000000000000000000000
                  0000C79985FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFFEFC
                  FCFFFEFCFBFFFEFCFAFFFDFCFAFFC28F7FFF0000000000000000000000000000
                  0000C99A89FFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
                  FDFFFEFEFDFFFEFDFDFFFEFDFDFFC49382FF0000000000000000000000000000
                  0000967467BDCA9C8BFFCA9C8BFFC99C8AFFC99B89FFC99B8AFFCA9A88FFC89A
                  88FFC99987FFC79887FFC89886FF927163BD0000000000000000}
                TabOrder = 3
                OnClick = cxButton13Click
              end
              object cxComboBox6: TcxComboBox
                Left = 397
                Top = 23
                Properties.OnCloseUp = cxComboBox6PropertiesCloseUp
                TabOrder = 4
                Width = 88
              end
            end
            object Panel1: TPanel
              Left = 0
              Top = 0
              Width = 1399
              Height = 32
              Align = alTop
              BevelOuter = bvNone
              Caption = #38050#26448#20379#24212#21333#20301
              Color = 16250613
              ParentBackground = False
              TabOrder = 1
              object RzBitBtn88: TRzBitBtn
                Left = 12
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #28155#21152
                Color = 15791348
                HotTrack = True
                TabOrder = 0
                OnClick = RzBitBtn88Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80909
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8E8E8E8E8E8091010
                  09E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E809101010
                  1009E8E8E8E8E8E8E8E8E8E881ACACACAC81E8E8E8E8E8E8E8E8E80910101010
                  101009E8E8E8E8E8E8E8E881ACACACACACAC81E8E8E8E8E8E8E8E80910100909
                  10101009E8E8E8E8E8E8E881ACAC8181ACACAC81E8E8E8E8E8E8E8091009E8E8
                  0910101009E8E8E8E8E8E881AC81E8E881ACACAC81E8E8E8E8E8E80909E8E8E8
                  E80910101009E8E8E8E8E88181E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8
                  E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8
                  E8E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E809101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E80909E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn89: TRzBitBtn
                Left = 85
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #32534#36753
                Color = 15791348
                HotTrack = True
                TabOrder = 1
                OnClick = RzBitBtn88Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000520B0000520B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E81710101010
                  1010101010E8E8E8E8E8E8AC818181818181818181E8E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  E3E3E3E3E310E8E8E8E8AC81E3E3E3E3E3E3E3E3E381E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  ACACACACAC10E8E8E8E8AC81E3E3E3E3ACACACACAC81E8E8E8E81710D7D7D7D7
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7D7E3E3E3E3E381E8E8E8E81710E3E3E3AC
                  ACACACACAC10E8E8E8E8AC81E3E3E3ACACACACACAC81E8E8E8E81710D7D7D7E3
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7E3E3E3E3E3E381E8E8E8E81710E3E3ACAC
                  10101010101010101010AC81E3E3ACAC818181818181818181811710D7D7E310
                  17101010101010101010AC81D7D7E381AC8181818181818181811710E3AC1717
                  171717101010101010E8AC81E3ACACACACACAC818181818181E81710D7175F5F
                  1717171717101010E8E8AC81D7ACE3E3ACACACACAC818181E8E81710175F5F5F
                  5F5F1717171710E8E8E8AC81ACE3E3E3E3E3ACACACAC81E8E8E817175F5F5F5F
                  5F5F5F5F1710E8E8E8E8ACACE3E3E3E3E3E3E3E3AC81E8E8E8E8E81781D781D7
                  81D781D781D7E8E8E8E8E8AC81D781D781D781D781D7E8E8E8E8E8E881AC81AC
                  81AC81AC81E8E8E8E8E8E8E881AC81AC81AC81AC81E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn90: TRzBitBtn
                Left = 158
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #21024#38500
                Color = 15791348
                HotTrack = True
                TabOrder = 2
                OnClick = RzBitBtn90Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
                  10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn91: TRzBitBtn
                Left = 309
                Top = 4
                Width = 65
                Height = 28
                FrameColor = 7617536
                Caption = 'Excel'
                Color = 15791348
                HotTrack = True
                TabOrder = 3
                OnClick = RzBitBtn91Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000B30B0000B30B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7ACAC
                  ACACD7D7D7D7D75EE8E8E8E881E8ACACACACE8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D709D7D7D75EE8E8E8E881E8E8E8E8E8E881E8E8E881E8E8E8E85ED7ACAC
                  D7D70909D7D7D75EE8E8E8E881E8ACACE8E88181E8E8E881E8E8E8E85ED7D7D7
                  D7090909095ED75EE8E8E8E881E8E8E8E881818181ACE881E8E8E8E85ED7ACAC
                  D7D70909D709D75EE8E8E8E881E8ACACE8E88181E881E881E8E8E8E85ED7D7D7
                  D7D7D709D709D75EE8E8E8E881E8E8E8E8E8E881E881E881E8E8E8E85ED7ACAC
                  ACD7D7D7D709D75EE8E8E8E881E8ACACACE8E8E8E881E881E8E8E8E85ED7D7D7
                  D7D7D7D7D709D75EE8E8E8E881E8E8E8E8E8E8E8E881E881E8E8E8E85ED7D7D7
                  09090909095ED75EE8E8E8E881E8E8E88181818181ACE881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn92: TRzBitBtn
                Left = 380
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #25171#21360
                Color = 15791348
                HotTrack = True
                TabOrder = 4
                OnClick = RzBitBtn14Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000730E0000730E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909
                  09090909090909E8E8E8E8E88181818181818181818181E8E8E8E85E89898989
                  89898989895E5E09E8E8E8E2ACACACACACACACACACE2E281E8E85E5E5E5E5E5E
                  5E5E5E5E5E5E095E09E8E2E2E2E2E2E2E2E2E2E2E2E281E281E85ED789898989
                  8989898989895E0909E8E2E8ACACACACACACACACACACE28181E85ED789898989
                  181289B490895E5E09E8E2E8ACACACACE281ACE281ACE2E281E85ED7D7D7D7D7
                  D7D7D7D7D7D75E5E5E09E2E8E8E8E8E8E8E8E8E8E8E8E2E2E2815ED789898989
                  8989898989895E5E5E09E2E8ACACACACACACACACACACE2E2E281E85E5E5E5E5E
                  5E5E5E5E5E89895E5E09E8E2E2E2E2E2E2E2E2E2E2ACACE2E281E8E85ED7D7D7
                  D7D7D7D7D75E89895E09E8E8E2E8E8E8E8E8E8E8E8E2ACACE281E8E8E85ED7E3
                  E3E3E3E3D75E5E5E09E8E8E8E8E2E8ACACACACACE8E2E2E281E8E8E8E85ED7D7
                  D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85ED7
                  E3E3E3E3E3D75EE8E8E8E8E8E8E8E2E8ACACACACACE8E2E8E8E8E8E8E8E85ED7
                  D7D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85E
                  5E5E5E5E5E5E5E5EE8E8E8E8E8E8E8E2E2E2E2E2E2E2E2E2E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn93: TRzBitBtn
                Left = 459
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #20851#38381
                Color = 15791348
                HotTrack = True
                TabOrder = 5
                OnClick = RzBitBtn21Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000420B0000420B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
                  D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn128: TRzBitBtn
                Left = 231
                Top = 4
                Width = 72
                Height = 28
                FrameColor = 7617536
                Caption = #26597#35810
                Color = 15791348
                HotTrack = True
                TabOrder = 6
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                  E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                  E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                  E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                  E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                  81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                  7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                  E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                  8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                  88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                  89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                  E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                  E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                  88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                  82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                  AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                  E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                NumGlyphs = 2
              end
            end
            object DBGridEh12: TDBGridEh
              Left = 0
              Top = 89
              Width = 1399
              Height = 428
              Align = alClient
              ColumnDefValues.EndEllipsis = True
              ColumnDefValues.Title.EndEllipsis = True
              Ctl3D = True
              DataSource = DataRebar
              DrawMemoText = True
              DynProps = <>
              FixedColor = 16250613
              Flat = True
              FooterRowCount = 1
              FooterParams.Color = clYellow
              FooterParams.Font.Charset = DEFAULT_CHARSET
              FooterParams.Font.Color = clRed
              FooterParams.Font.Height = -12
              FooterParams.Font.Name = 'Tahoma'
              FooterParams.Font.Style = []
              FooterParams.ParentFont = False
              FooterParams.VertLines = False
              IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
              ParentCtl3D = False
              RowDetailPanel.Active = True
              RowDetailPanel.Height = 600
              RowDetailPanel.Color = clBtnFace
              RowHeight = 2
              RowLines = 2
              RowSizingAllowed = True
              RowPanel.NavKeysNavigationType = rpntCellToCellEh
              RowPanel.TabNavigationType = rpntLeftToRightPriorityEh
              SumList.Active = True
              TabOrder = 2
              TitleParams.Color = 16250613
              TitleParams.FillStyle = cfstSolidEh
              TitleParams.MultiTitle = True
              OnDblClick = DBGridEh12DblClick
              OnKeyDown = DBGridEh12KeyDown
              OnRowDetailPanelShow = DBGridEh12RowDetailPanelShow
              Columns = <
                item
                  BiDiMode = bdLeftToRight
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'numbers'
                  Footer.Value = #21512#35745':'
                  Footer.ValueType = fvtStaticText
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #32534#21495
                  Width = 107
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chCaption'
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #24037#31243#21517#31216
                  Width = 113
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chForGoods'
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #20379#36135#21333#20301
                  Width = 100
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  Footers = <>
                  Title.Caption = #26085#26399
                  Width = 100
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chInGoodsPeople'
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #25910#36135#20154
                  Width = 84
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chDiameter'
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #38050#26448#22411#21495
                  Width = 77
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chRootCount'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #25968#37327'('#26681')'
                  Width = 76
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chPieceCount'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #20214
                  Width = 99
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chMeter'
                  Footer.FieldName = 'chMeter'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #31859#25968
                  Width = 84
                end
                item
                  CellButtons = <>
                  DisplayFormat = '0.00###'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chTheoryWeight'
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #29702#35770'(kg/m)'
                  Width = 103
                end
                item
                  CellButtons = <>
                  DisplayFormat = '0.00#'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chWeight'
                  Footer.FieldName = 'chWeight'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #37325#37327'('#21544')'
                  Title.EndEllipsis = False
                  Width = 105
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chCompany'
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #21333#20301
                  Width = 81
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'#,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chUnitPrice'
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #21333#20215
                  Width = 118
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'#,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chTotal'
                  Footer.DisplayFormat = #65509'#,##.#### '
                  Footer.FieldName = 'chTotal'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #24635#20215
                  Width = 100
                end
                item
                  CellButtons = <>
                  Checkboxes = False
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'chRemarks'
                  Footers = <>
                  Layout = tlCenter
                  Title.Caption = #22791#27880
                  Width = 200
                  WordWrap = True
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object RzPanel18: TRzPanel
                  Left = 0
                  Top = 0
                  Width = 1353
                  Height = 41
                  Align = alTop
                  BorderOuter = fsFlat
                  GradientColorStyle = gcsCustom
                  TabOrder = 0
                  VisualStyle = vsGradient
                  object RzLabel10: TRzLabel
                    Left = 105
                    Top = 12
                    Width = 28
                    Height = 14
                    Caption = #26597#35810':'
                    Color = 16250613
                    ParentColor = False
                    Transparent = True
                    WordWrap = True
                  end
                  object RzEdit17: TRzEdit
                    Left = 139
                    Top = 10
                    Width = 209
                    Height = 22
                    Text = ''
                    TabOrder = 0
                  end
                  object RzBitBtn94: TRzBitBtn
                    Left = 354
                    Top = 7
                    Width = 62
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25628#32034
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 1
                    OnClick = RzBitBtn94Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                      E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                      E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                      E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                      E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                      81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                      7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                      E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                      8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                      88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                      89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                      E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                      E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                      88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                      82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                      AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                      E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn95: TRzBitBtn
                    Left = 422
                    Top = 7
                    Width = 84
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25171#24320#30446#24405
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 2
                    OnClick = RzBitBtn95Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
                      787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
                      CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
                      D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
                      D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
                      D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
                      D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
                      D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
                      A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
                      D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
                      A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
                      E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn96: TRzBitBtn
                    Left = 9
                    Top = 7
                    Height = 28
                    FrameColor = 7617536
                    Caption = #19978#19968#32423
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 3
                    OnClick = RzBitBtn96Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000530B0000530B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E80910101010
                      101010101010101009E8E881ACACACACACACACACACACACAC81E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                end
                object RzFileListBox10: TRzFileListBox
                  Left = 0
                  Top = 41
                  Width = 1353
                  Height = 280
                  Align = alClient
                  FileType = [ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ItemHeight = 18
                  ParentFont = False
                  TabOrder = 1
                  AllowOpen = True
                  FrameVisible = True
                  FramingPreference = fpCustomFraming
                end
              end
            end
          end
          object t10: TRzTabSheet
            Color = 16250613
            Caption = #36164#36136#21333#20301
            object cxGroupBox12: TcxGroupBox
              Left = 0
              Top = 32
              Align = alTop
              Caption = #26597#35810
              TabOrder = 0
              Height = 57
              Width = 1399
              object Label43: TLabel
                Left = 24
                Top = 24
                Width = 52
                Height = 14
                Caption = #26597#35810#21517#31216':'
              end
              object Label44: TLabel
                Left = 335
                Top = 25
                Width = 52
                Height = 14
                Caption = #36215#22987#26085#26399':'
              end
              object Label45: TLabel
                Left = 484
                Top = 25
                Width = 52
                Height = 14
                Caption = #32467#26463#26085#26399':'
              end
              object cxTextEdit14: TcxTextEdit
                Left = 82
                Top = 21
                TabOrder = 0
                TextHint = #24037#31243#21517#31216','#36164#36136#21333#20301#21517#31216
                OnKeyDown = cxTextEdit14KeyDown
                Width = 247
              end
              object cxDateEdit21: TcxDateEdit
                Left = 393
                Top = 23
                TabOrder = 1
                Width = 85
              end
              object cxDateEdit22: TcxDateEdit
                Left = 542
                Top = 23
                TabOrder = 2
                Width = 96
              end
              object cxButton14: TcxButton
                Left = 644
                Top = 19
                Width = 64
                Height = 30
                Caption = #26597#35810
                OptionsImage.Glyph.Data = {
                  36040000424D3604000000000000360000002800000010000000100000000100
                  2000000000000004000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000020000000B000000120000000C00000003000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000200000010071334970F276AFF0A193B970000000B000000000000
                  00007B5043B8AB705CFFAB6F5AFFAB705CFFAA6F5BFFAA6E59FFA96F5AFFBE91
                  82FFC9ACA3FF5F617FFF417CB9FF70C7FFFF265198FF00000010000000000000
                  0000AD735FFFFDFBF9FFFBF5F2FFF7F2EEFFF3EDE9FFEFE9E5FFECE5E1FFE6DE
                  DAFF707E9FFF4C83BCFF83CFFFFF5694CEFF142B4D930000000A000000000000
                  0000B07762FFFDFBFAFFF7F3F0FFE2D8D2FFA5816CFF8E5E42FF8C5D41FF7A5E
                  54FF577EA6FF92D4FAFF619CD0FF727F9BFF0000000E00000002000000000000
                  0000B07966FFFBF9F9FFE1D5CEFF936346FFC8A37FFFEFD7B2FFF0DAB8FFC7A6
                  88FF895D43FF6891B2FF849DB9FFCCB0A7FF0000000200000000000000000000
                  0000B37C69FFFAF8F7FFAD8975FFC7A07BFFF7D39CFFF5CD93FFF7D39BFFF9DD
                  B2FFC7A688FF84695DFFE8E2DEFFC29888FF0000000000000000000000000000
                  0000B67F6CFFF9F8F7FF98694CFFF1D4A7FFFAE5C0FFFBEACAFFF7D6A0FFF6D3
                  9BFFF2DBBBFF8F5D42FFF0E9E7FFB27A66FF0000000000000000000000000000
                  0000B98371FFFAF9F8FF9D6E51FFF2D4A5FFFDF6E2FFFDF3DCFFFBEACAFFF5CE
                  92FFF1DAB5FF936245FFF2EDE9FFB47D6AFF0000000000000000000000000000
                  0000BC8877FFFCFCFBFFB99783FFCDA77EFFF9E0B5FFFEF7E5FFFBE5C1FFF6D4
                  9DFFCAA681FFAF8C77FFF5F1EEFFB6806DFF0000000000000000000000000000
                  0000BF8C7AFFFDFDFCFFEDE4DFFFA87A5DFFCEA77FFFEFD2A3FFEFD2A5FFCCA7
                  80FFA17356FFE4DAD4FFFAF6F3FFB98371FF0000000000000000000000000000
                  0000C18F7FFFFEFEFEFFFDFCFBFFEDE4DFFFBE9C87FFAA7E62FFA97D60FFBB98
                  82FFEADFD8FFFBF8F6FFFDF9F8FFBD8774FF0000000000000000000000000000
                  0000C49382FFFFFEFEFFFEFEFDFFFBF6F4FFFAF5F3FFF9F3F0FFF9F3F0FFFAF2
                  F0FFFAF4F0FFFDFBF9FFFDFBF9FFBF8C7BFF0000000000000000000000000000
                  0000C79985FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFFEFC
                  FCFFFEFCFBFFFEFCFAFFFDFCFAFFC28F7FFF0000000000000000000000000000
                  0000C99A89FFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
                  FDFFFEFEFDFFFEFDFDFFFEFDFDFFC49382FF0000000000000000000000000000
                  0000967467BDCA9C8BFFCA9C8BFFC99C8AFFC99B89FFC99B8AFFCA9A88FFC89A
                  88FFC99987FFC79887FFC89886FF927163BD0000000000000000}
                TabOrder = 3
                OnClick = cxButton14Click
              end
            end
            object DBGridEh14: TDBGridEh
              Left = 0
              Top = 89
              Width = 1399
              Height = 428
              Align = alClient
              ColumnDefValues.Layout = tlCenter
              DataGrouping.Footers = <
                item
                  ColumnItems = <
                    item
                    end
                    item
                    end
                    item
                    end>
                  Visible = False
                end>
              DataSource = DataQualifications
              DrawMemoText = True
              DynProps = <>
              FixedColor = 16250613
              Flat = True
              FooterRowCount = 1
              FooterParams.Color = clYellow
              FooterParams.Font.Charset = DEFAULT_CHARSET
              FooterParams.Font.Color = clRed
              FooterParams.Font.Height = -11
              FooterParams.Font.Name = 'Tahoma'
              FooterParams.Font.Style = []
              FooterParams.ParentFont = False
              FooterParams.VertLines = False
              IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghHotTrack, dghExtendVertLines]
              ReadOnly = True
              RowDetailPanel.Active = True
              RowDetailPanel.Height = 600
              RowDetailPanel.Color = clBtnFace
              RowHeight = 30
              RowSizingAllowed = True
              SumList.Active = True
              TabOrder = 1
              TitleParams.RowHeight = 8
              TitleParams.RowLines = 1
              OnKeyDown = DBGridEh14KeyDown
              OnRowDetailPanelShow = DBGridEh14RowDetailPanelShow
              Columns = <
                item
                  Alignment = taCenter
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'numbers'
                  Footer.Value = #21512#35745':'
                  Footer.ValueType = fvtStaticText
                  Footers = <>
                  Title.Caption = #21333#21495
                  Title.TitleButton = True
                  Width = 71
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'caption'
                  Footers = <>
                  Title.Caption = #24037#31243#21517#31216
                  Title.TitleButton = True
                  Width = 89
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'content'
                  Footers = <>
                  Title.Caption = #36164#36136#21333#20301#21517#31216
                  Title.TitleButton = True
                  Width = 123
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'ProjectDepartment'
                  Footers = <>
                  Title.Caption = #39033#30446#37096
                  Width = 80
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'pay_time'
                  Footers = <>
                  Title.Caption = #26085#26399
                  Title.TitleButton = True
                  Width = 102
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'End_Time'
                  Footers = <>
                  Title.Caption = #32467#26463#26085#26399
                  Title.TitleButton = True
                  Visible = False
                  Width = 91
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Price'
                  Footers = <>
                  Title.Caption = #21333#20215
                  Title.TitleButton = True
                  Width = 89
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'num'
                  Footer.FieldName = 'num'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #25968#37327
                  Title.TitleButton = True
                  Width = 63
                end
                item
                  Alignment = taCenter
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'Units'
                  Footers = <>
                  Title.Caption = #21333#20301
                  Title.TitleButton = True
                  Width = 30
                end
                item
                  CellButtons = <>
                  DisplayFormat = #65509'0,0.00'
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 't'
                  Footer.DisplayFormat = #65509'#,0.##'
                  Footer.FieldName = 't'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.Caption = #24635#20215
                  Title.TitleButton = True
                  Width = 133
                end
                item
                  CellButtons = <>
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'remarks'
                  Footers = <>
                  Title.Caption = #22791#27880
                  Title.TitleButton = True
                  Width = 278
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object RzPanel20: TRzPanel
                  Left = 0
                  Top = 0
                  Width = 1047
                  Height = 41
                  Align = alTop
                  BorderOuter = fsFlat
                  GradientColorStyle = gcsCustom
                  TabOrder = 0
                  VisualStyle = vsGradient
                  object RzLabel12: TRzLabel
                    Left = 105
                    Top = 12
                    Width = 28
                    Height = 14
                    Caption = #26597#35810':'
                    Color = 16250613
                    ParentColor = False
                    Transparent = True
                    WordWrap = True
                  end
                  object RzEdit19: TRzEdit
                    Left = 139
                    Top = 10
                    Width = 209
                    Height = 22
                    Text = ''
                    TabOrder = 0
                  end
                  object RzBitBtn100: TRzBitBtn
                    Left = 354
                    Top = 7
                    Width = 62
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25628#32034
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 1
                    OnClick = RzBitBtn100Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                      E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                      E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                      E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                      E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                      81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                      7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                      E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                      8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                      88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                      89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                      E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                      E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                      88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                      82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                      AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                      E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn101: TRzBitBtn
                    Left = 422
                    Top = 7
                    Width = 84
                    Height = 28
                    FrameColor = 7617536
                    Caption = #25171#24320#30446#24405
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 2
                    OnClick = RzBitBtn101Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000330B0000330B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
                      787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
                      CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
                      D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
                      D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
                      D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
                      D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
                      D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
                      A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
                      D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
                      A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
                      E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                  object RzBitBtn102: TRzBitBtn
                    Left = 9
                    Top = 7
                    Height = 28
                    FrameColor = 7617536
                    Caption = #19978#19968#32423
                    Color = 15791348
                    HotTrack = True
                    TabOrder = 3
                    OnClick = RzBitBtn102Click
                    Glyph.Data = {
                      36060000424D3606000000000000360400002800000020000000100000000100
                      08000000000000020000530B0000530B00000001000000000000000000003300
                      00006600000099000000CC000000FF0000000033000033330000663300009933
                      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                      000000990000339900006699000099990000CC990000FF99000000CC000033CC
                      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                      330000333300333333006633330099333300CC333300FF333300006633003366
                      33006666330099663300CC663300FF6633000099330033993300669933009999
                      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                      66006600660099006600CC006600FF0066000033660033336600663366009933
                      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                      660000996600339966006699660099996600CC996600FF99660000CC660033CC
                      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                      990000339900333399006633990099339900CC339900FF339900006699003366
                      99006666990099669900CC669900FF6699000099990033999900669999009999
                      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                      000000808000800000008000800080800000C0C0C00080808000191919004C4C
                      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E80910101010
                      101010101010101009E8E881ACACACACACACACACACACACAC81E8E8E809101010
                      101010101010101009E8E8E881ACACACACACACACACACACAC81E8E8E8E8091010
                      101009090909090909E8E8E8E881ACACACAC81818181818181E8E8E8E8E80910
                      101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E809
                      101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E8E8E8
                      090909E8E8E8E8E8E8E8E8E8E8E8E8E8818181E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                    NumGlyphs = 2
                  end
                end
                object RzFileListBox12: TRzFileListBox
                  Left = 0
                  Top = 41
                  Width = 1047
                  Height = 310
                  Align = alClient
                  FileType = [ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ItemHeight = 18
                  ParentFont = False
                  TabOrder = 1
                  AllowOpen = True
                  FrameVisible = True
                  FramingPreference = fpCustomFraming
                end
              end
            end
            object Panel2: TPanel
              Left = 0
              Top = 0
              Width = 1399
              Height = 32
              Align = alTop
              BevelOuter = bvNone
              Caption = #36164#36136#21333#20301
              Color = 16250613
              ParentBackground = False
              TabOrder = 2
              object RzBitBtn103: TRzBitBtn
                Left = 12
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #28155#21152
                Color = 15791348
                HotTrack = True
                TabOrder = 0
                OnClick = RzBitBtn103Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80909
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8E8E8E8E8E8091010
                  09E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E809101010
                  1009E8E8E8E8E8E8E8E8E8E881ACACACAC81E8E8E8E8E8E8E8E8E80910101010
                  101009E8E8E8E8E8E8E8E881ACACACACACAC81E8E8E8E8E8E8E8E80910100909
                  10101009E8E8E8E8E8E8E881ACAC8181ACACAC81E8E8E8E8E8E8E8091009E8E8
                  0910101009E8E8E8E8E8E881AC81E8E881ACACAC81E8E8E8E8E8E80909E8E8E8
                  E80910101009E8E8E8E8E88181E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8
                  E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8
                  E8E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E809101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E80909E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn104: TRzBitBtn
                Left = 85
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #32534#36753
                Color = 15791348
                HotTrack = True
                TabOrder = 1
                OnClick = RzBitBtn103Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000520B0000520B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E81710101010
                  1010101010E8E8E8E8E8E8AC818181818181818181E8E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  E3E3E3E3E310E8E8E8E8AC81E3E3E3E3E3E3E3E3E381E8E8E8E81710D7D7D7D7
                  D7D7D7D7D710E8E8E8E8AC81D7D7D7D7D7D7D7D7D781E8E8E8E81710E3E3E3E3
                  ACACACACAC10E8E8E8E8AC81E3E3E3E3ACACACACAC81E8E8E8E81710D7D7D7D7
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7D7E3E3E3E3E381E8E8E8E81710E3E3E3AC
                  ACACACACAC10E8E8E8E8AC81E3E3E3ACACACACACAC81E8E8E8E81710D7D7D7E3
                  E3E3E3E3E310E8E8E8E8AC81D7D7D7E3E3E3E3E3E381E8E8E8E81710E3E3ACAC
                  10101010101010101010AC81E3E3ACAC818181818181818181811710D7D7E310
                  17101010101010101010AC81D7D7E381AC8181818181818181811710E3AC1717
                  171717101010101010E8AC81E3ACACACACACAC818181818181E81710D7175F5F
                  1717171717101010E8E8AC81D7ACE3E3ACACACACAC818181E8E81710175F5F5F
                  5F5F1717171710E8E8E8AC81ACE3E3E3E3E3ACACACAC81E8E8E817175F5F5F5F
                  5F5F5F5F1710E8E8E8E8ACACE3E3E3E3E3E3E3E3AC81E8E8E8E8E81781D781D7
                  81D781D781D7E8E8E8E8E8AC81D781D781D781D781D7E8E8E8E8E8E881AC81AC
                  81AC81AC81E8E8E8E8E8E8E881AC81AC81AC81AC81E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn105: TRzBitBtn
                Left = 158
                Top = 4
                Width = 67
                Height = 28
                FrameColor = 7617536
                Caption = #21024#38500
                Color = 15791348
                HotTrack = True
                TabOrder = 2
                OnClick = RzBitBtn103Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
                  10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
                  1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
                  100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E809101010
                  09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E80910101009
                  E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E809090909E8
                  E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn106: TRzBitBtn
                Left = 309
                Top = 4
                Width = 65
                Height = 28
                FrameColor = 7617536
                Caption = 'Excel'
                Color = 15791348
                HotTrack = True
                TabOrder = 3
                OnClick = RzBitBtn13Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000B30B0000B30B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7ACAC
                  ACACD7D7D7D7D75EE8E8E8E881E8ACACACACE8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D709D7D7D75EE8E8E8E881E8E8E8E8E8E881E8E8E881E8E8E8E85ED7ACAC
                  D7D70909D7D7D75EE8E8E8E881E8ACACE8E88181E8E8E881E8E8E8E85ED7D7D7
                  D7090909095ED75EE8E8E8E881E8E8E8E881818181ACE881E8E8E8E85ED7ACAC
                  D7D70909D709D75EE8E8E8E881E8ACACE8E88181E881E881E8E8E8E85ED7D7D7
                  D7D7D709D709D75EE8E8E8E881E8E8E8E8E8E881E881E881E8E8E8E85ED7ACAC
                  ACD7D7D7D709D75EE8E8E8E881E8ACACACE8E8E8E881E881E8E8E8E85ED7D7D7
                  D7D7D7D7D709D75EE8E8E8E881E8E8E8E8E8E8E8E881E881E8E8E8E85ED7D7D7
                  09090909095ED75EE8E8E8E881E8E8E88181818181ACE881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7D7D7
                  D7D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85E5E5E5E
                  5E5E5E5E5E5E5E5EE8E8E8E8818181818181818181818181E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn107: TRzBitBtn
                Left = 380
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #25171#21360
                Color = 15791348
                HotTrack = True
                TabOrder = 4
                OnClick = RzBitBtn14Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000730E0000730E00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909
                  09090909090909E8E8E8E8E88181818181818181818181E8E8E8E85E89898989
                  89898989895E5E09E8E8E8E2ACACACACACACACACACE2E281E8E85E5E5E5E5E5E
                  5E5E5E5E5E5E095E09E8E2E2E2E2E2E2E2E2E2E2E2E281E281E85ED789898989
                  8989898989895E0909E8E2E8ACACACACACACACACACACE28181E85ED789898989
                  181289B490895E5E09E8E2E8ACACACACE281ACE281ACE2E281E85ED7D7D7D7D7
                  D7D7D7D7D7D75E5E5E09E2E8E8E8E8E8E8E8E8E8E8E8E2E2E2815ED789898989
                  8989898989895E5E5E09E2E8ACACACACACACACACACACE2E2E281E85E5E5E5E5E
                  5E5E5E5E5E89895E5E09E8E2E2E2E2E2E2E2E2E2E2ACACE2E281E8E85ED7D7D7
                  D7D7D7D7D75E89895E09E8E8E2E8E8E8E8E8E8E8E8E2ACACE281E8E8E85ED7E3
                  E3E3E3E3D75E5E5E09E8E8E8E8E2E8ACACACACACE8E2E2E281E8E8E8E85ED7D7
                  D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85ED7
                  E3E3E3E3E3D75EE8E8E8E8E8E8E8E2E8ACACACACACE8E2E8E8E8E8E8E8E85ED7
                  D7D7D7D7D7D7D75EE8E8E8E8E8E8E2E8E8E8E8E8E8E8E8E2E8E8E8E8E8E8E85E
                  5E5E5E5E5E5E5E5EE8E8E8E8E8E8E8E2E2E2E2E2E2E2E2E2E8E8E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn108: TRzBitBtn
                Left = 459
                Top = 4
                Width = 73
                Height = 28
                FrameColor = 7617536
                Caption = #20851#38381
                Color = 15791348
                HotTrack = True
                TabOrder = 5
                OnClick = RzBitBtn21Click
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000420B0000420B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
                  E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
                  D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
                  D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
                  D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
                  B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
                  B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
                  B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
                  B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
                  D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
                NumGlyphs = 2
              end
              object RzBitBtn129: TRzBitBtn
                Left = 231
                Top = 4
                Width = 72
                Height = 28
                FrameColor = 7617536
                Caption = #26597#35810
                Color = 15791348
                HotTrack = True
                TabOrder = 6
                Glyph.Data = {
                  36060000424D3606000000000000360400002800000020000000100000000100
                  08000000000000020000330B0000330B00000001000000000000000000003300
                  00006600000099000000CC000000FF0000000033000033330000663300009933
                  0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                  000000990000339900006699000099990000CC990000FF99000000CC000033CC
                  000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                  0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                  330000333300333333006633330099333300CC333300FF333300006633003366
                  33006666330099663300CC663300FF6633000099330033993300669933009999
                  3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                  330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                  66006600660099006600CC006600FF0066000033660033336600663366009933
                  6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                  660000996600339966006699660099996600CC996600FF99660000CC660033CC
                  660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                  6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                  990000339900333399006633990099339900CC339900FF339900006699003366
                  99006666990099669900CC669900FF6699000099990033999900669999009999
                  9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                  990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                  CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                  CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                  CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                  CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                  CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                  FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                  FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                  FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                  FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
                  000000808000800000008000800080800000C0C0C00080808000191919004C4C
                  4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
                  6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
                  E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
                  E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
                  E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
                  E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
                  81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
                  7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
                  E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
                  8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
                  88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
                  89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
                  E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
                  E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
                  88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
                  82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
                  AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
                  E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
                NumGlyphs = 2
              end
            end
          end
        end
      end
      object TabSheet14: TRzTabSheet
        Color = 16119543
        Caption = #20132#26131#35760#24405
        object Splitter1: TSplitter
          Left = 0
          Top = 331
          Width = 1407
          Height = 10
          Cursor = crVSplit
          Align = alBottom
          Color = 16250613
          ParentColor = False
          ExplicitTop = 327
        end
        object RzToolbar2: TRzToolbar
          Left = 0
          Top = 0
          Width = 1407
          Height = 29
          AutoStyle = False
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsNone
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 0
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer5
            RzToolButton7
            RzSpacer15
            RzToolButton16
            RzSpacer8
            RzToolButton8
            RzSpacer9
            RzToolButton9
            RzSpacer10
            RzToolButton10
            RzSpacer1
            RzToolButton11
            RzSpacer11
            cxLabel9
            Date3
            RzSpacer26
            cxLabel11
            Date4
            RzSpacer27
            RzToolButton23
            RzSpacer12
            RzToolButton12)
          object RzSpacer5: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton7: TRzToolButton
            Left = 12
            Top = 2
            Width = 90
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152#25903#27454
            OnClick = RzToolButton7Click
          end
          object RzSpacer8: TRzSpacer
            Left = 176
            Top = 2
          end
          object RzToolButton8: TRzToolButton
            Left = 184
            Top = 2
            Width = 70
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
            OnClick = RzToolButton8Click
          end
          object RzSpacer9: TRzSpacer
            Left = 254
            Top = 2
          end
          object RzToolButton9: TRzToolButton
            Left = 262
            Top = 2
            Width = 70
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
            OnClick = RzToolButton7Click
          end
          object RzSpacer10: TRzSpacer
            Left = 332
            Top = 2
          end
          object RzToolButton10: TRzToolButton
            Left = 340
            Top = 2
            Width = 90
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
            OnClick = RzToolButton7Click
          end
          object RzToolButton11: TRzToolButton
            Left = 438
            Top = 2
            Width = 75
            DropDownMenu = Print
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzSpacer11: TRzSpacer
            Left = 513
            Top = 2
          end
          object RzToolButton12: TRzToolButton
            Left = 865
            Top = 2
            Width = 70
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
            OnClick = RzToolButton12Click
          end
          object RzSpacer12: TRzSpacer
            Left = 857
            Top = 2
          end
          object RzSpacer15: TRzSpacer
            Left = 102
            Top = 2
          end
          object RzToolButton16: TRzToolButton
            Left = 110
            Top = 2
            Width = 66
            ImageIndex = 16
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #26126#32454#21333
            OnClick = RzToolButton16Click
          end
          object RzSpacer1: TRzSpacer
            Left = 430
            Top = 2
          end
          object RzToolButton23: TRzToolButton
            Left = 829
            Top = 2
            Width = 28
            SelectionColorStop = 16250613
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
            OnClick = RzToolButton23Click
          end
          object RzSpacer26: TRzSpacer
            Left = 667
            Top = 2
          end
          object RzSpacer27: TRzSpacer
            Left = 821
            Top = 2
          end
          object cxLabel9: TcxLabel
            Left = 521
            Top = 5
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object Date3: TcxDateEdit
            Left = 577
            Top = 3
            TabOrder = 1
            Width = 90
          end
          object cxLabel11: TcxLabel
            Left = 675
            Top = 5
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object Date4: TcxDateEdit
            Left = 731
            Top = 3
            TabOrder = 3
            Width = 90
          end
        end
        object cxGrid1: TcxGrid
          Left = 0
          Top = 29
          Width = 1407
          Height = 302
          Align = alClient
          TabOrder = 1
          object tvGrid3: TcxGridDBTableView
            OnKeyDown = tvGrid3KeyDown
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvGrid3Editing
            OnEditKeyDown = tvGrid3EditKeyDown
            DataController.DataSource = dsGrid3
            DataController.KeyFieldNames = 'Numbers'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                OnGetText = tvGrid3TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                Column = tvGrid3Column11
              end
              item
                Kind = skCount
                Column = tvGrid3Column12
              end
              item
                Kind = skCount
                Column = tvGrid3Column1
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#26597#35810#31579#36873
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#26242#26080#25903#27454#25968#25454#26174#31034'>'
            OptionsView.DataRowHeight = 28
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 28
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            Styles.OnGetContentStyle = tvGrid3StylesGetContentStyle
            object tvGrid3Column1: TcxGridDBColumn
              Caption = #24207#21495
              OnGetDisplayText = tvGrid3Column1GetDisplayText
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 35
            end
            object tvGrid3Column2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'Numbers'
              Options.Filtering = False
              Options.Sorting = False
              Width = 75
            end
            object tvGrid3Column18: TcxGridDBColumn
              Caption = #29366#24577
              DataBinding.FieldName = 'status'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.OnChange = tvGrid3Column18PropertiesChange
              OnCustomDrawCell = tvGrid3Column18CustomDrawCell
              HeaderAlignmentHorz = taCenter
              Width = 42
            end
            object tvGrid3Column3: TcxGridDBColumn
              Caption = #20132#26131#26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Width = 118
              OnCompareRowValuesForCellMerging = tvGrid3Column3CompareRowValuesForCellMerging
            end
            object tvGrid3Column4: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 162
            end
            object tvGrid3Column5: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 180
            end
            object tvGrid3Column6: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 224
            end
            object tvGrid3Column7: TcxGridDBColumn
              Caption = #39033#30446#37096#36127#36131#20154
              DataBinding.FieldName = 'ProjectManager'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 120
            end
            object tvGrid3Column8: TcxGridDBColumn
              Caption = #25910#27454#21333#20301
              DataBinding.FieldName = 'CollectMoneyCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 100
            end
            object tvGrid3Column9: TcxGridDBColumn
              Caption = #20132#26131#26041#24335
              DataBinding.FieldName = 'DealType'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 60
            end
            object tvGrid3Column10: TcxGridDBColumn
              Caption = #20184#27454#21333#20301
              DataBinding.FieldName = 'PaymentCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 104
            end
            object tvGrid3Column13: TcxGridDBColumn
              Caption = #25910#27454#20154
              DataBinding.FieldName = 'Payee'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 60
            end
            object tvGrid3Column14: TcxGridDBColumn
              Caption = #25910#27454#24080#21495
              DataBinding.FieldName = 'PayeeAccountNumber'
              PropertiesClassName = 'TcxSpinEditProperties'
              Width = 125
            end
            object tvGrid3Column15: TcxGridDBColumn
              Caption = #20184#27454#20154
              DataBinding.FieldName = 'Drawee'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 60
            end
            object tvGrid3Column16: TcxGridDBColumn
              Caption = #20184#27454#24080#21495
              DataBinding.FieldName = 'DraweeAccountNumber'
              PropertiesClassName = 'TcxSpinEditProperties'
              Width = 153
            end
            object tvGrid3Column11: TcxGridDBColumn
              Caption = #32467#31639#37329#39069
              DataBinding.FieldName = 'SettleAccountsMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              OnCustomDrawCell = tvGrid3Column11CustomDrawCell
              Width = 240
            end
            object tvGrid3Column12: TcxGridDBColumn
              Caption = #22823#20889#37329#39069
              DataBinding.FieldName = 'SettleAccountslarge'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 368
            end
            object tvGrid3Column17: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'remarks'
              PropertiesClassName = 'TcxMemoProperties'
              Width = 320
            end
          end
          object tvView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dsGrid4
            DataController.DetailKeyFieldNames = 'parent'
            DataController.MasterKeyFieldNames = 'Numbers'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsView.CellAutoHeight = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            object tvViewColumn1: TcxGridDBColumn
              Caption = #24207#21495
              OnGetDisplayText = tvViewColumn1GetDisplayText
              Options.Filtering = False
              Options.Sorting = False
              Width = 30
            end
            object tvViewColumn2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'Numbers'
              Options.Filtering = False
              Options.Sorting = False
              Width = 70
            end
            object tvViewColumn16: TcxGridDBColumn
              Caption = #39033#30446#21517#31216
              DataBinding.FieldName = 'ProjectName'
              Width = 70
            end
            object tvViewColumn3: TcxGridDBColumn
              Caption = #24212#25320#27454#39033
              DataBinding.FieldName = 'appropriation'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn4: TcxGridDBColumn
              Caption = #39033#30446#25187#32602
              DataBinding.FieldName = 'fine'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn5: TcxGridDBColumn
              Caption = #30002#26041#20379#26448
              DataBinding.FieldName = 'Armourforwood'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn6: TcxGridDBColumn
              Caption = #23454#25320#27454#39033
              DataBinding.FieldName = 'Realmoney'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn7: TcxGridDBColumn
              Caption = #31246#37329
              DataBinding.FieldName = 'tax'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn8: TcxGridDBColumn
              Caption = #27700#30005
              DataBinding.FieldName = 'hydropower'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn9: TcxGridDBColumn
              Caption = #21457#31080
              DataBinding.FieldName = 'invoice'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn10: TcxGridDBColumn
              Caption = #22686#39033
              DataBinding.FieldName = 'increase'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn11: TcxGridDBColumn
              Caption = #23427#39033
              DataBinding.FieldName = 'LtIsa'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn12: TcxGridDBColumn
              Caption = #22870#21169
              DataBinding.FieldName = 'reward'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn13: TcxGridDBColumn
              Caption = #35745#31639#20844#24335
              DataBinding.FieldName = 'DesignFormulas'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn14: TcxGridDBColumn
              Caption = #37329#39069
              DataBinding.FieldName = 'receiptsMoney'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvViewColumn15: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
          end
          object Lv1: TcxGridLevel
            Caption = #20027#34920
            GridView = tvGrid3
            object Lv2: TcxGridLevel
              Caption = #35814#32454#34920
              GridView = tvView
            end
          end
        end
        object RzPanel3: TRzPanel
          Left = 0
          Top = 341
          Width = 1407
          Height = 212
          Align = alBottom
          BorderOuter = fsNone
          TabOrder = 2
          object FlowingGrid: TcxGrid
            Left = 0
            Top = 0
            Width = 1407
            Height = 212
            Align = alClient
            TabOrder = 0
            object tvFlowingView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = DataFlowingGroup
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = #165',0.00;'#165'-,0.00'
                  Kind = skSum
                  Column = tvFlowingViewColumn3
                end>
              DataController.Summary.SummaryGroups = <>
              OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#27969#27700#24080#35760#24405'>'
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              OptionsView.HeaderHeight = 26
              OptionsView.Indicator = True
              OptionsView.IndicatorWidth = 20
              object tvFlowingViewColumn1: TcxGridDBColumn
                Caption = #24207#21495
                OnGetDisplayText = tvFlowingViewColumn1GetDisplayText
                Options.Filtering = False
                Options.Focusing = False
                Options.Sorting = False
                Width = 40
              end
              object tvFlowingViewColumn2: TcxGridDBColumn
                Caption = #24037#31243#21517#31216
                DataBinding.FieldName = 'ProjectName'
                HeaderAlignmentHorz = taCenter
                Options.Filtering = False
                Options.Sorting = False
                Width = 208
              end
              object tvFlowingViewColumn4: TcxGridDBColumn
                Caption = #25910#27454#21333#20301
                DataBinding.FieldName = 'Receivables'
                HeaderAlignmentHorz = taCenter
                Options.Filtering = False
                Options.Sorting = False
                Width = 100
              end
              object tvFlowingViewColumn5: TcxGridDBColumn
                Caption = #25910#27454#20154#21517#31216
                DataBinding.FieldName = 'Payee'
                HeaderAlignmentHorz = taCenter
                Options.Filtering = False
                Options.Sorting = False
                Width = 100
              end
              object tvFlowingViewColumn3: TcxGridDBColumn
                Caption = #24635#39069
                DataBinding.FieldName = 't'
                PropertiesClassName = 'TcxCurrencyEditProperties'
                HeaderAlignmentHorz = taCenter
                Options.Filtering = False
                Options.Sorting = False
                Width = 176
              end
            end
            object FlowingLv: TcxGridLevel
              GridView = tvFlowingView
            end
          end
        end
      end
    end
  end
  object cxSplitter3: TcxSplitter
    Left = 257
    Top = 0
    Width = 8
    Height = 624
    HotZoneClassName = 'TcxXPTaskBarStyle'
    HotZone.SizePercent = 61
    AutoSnap = True
    ResizeUpdate = True
  end
  object PopupMenu1: TPopupMenu
    Left = 392
    Top = 248
    object N1: TMenuItem
      Caption = #20184#27454
    end
  end
  object PrintDBGridEh1: TPrintDBGridEh
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'Tahoma'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'Tahoma'
    PageHeader.Font.Style = []
    Units = MM
    Left = 264
    Top = 168
  end
  object ADOMechanics: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 56
    Top = 408
  end
  object DataMechanics: TDataSource
    DataSet = ADOMechanics
    Left = 144
    Top = 408
  end
  object DataLease: TDataSource
    DataSet = ADOLease
    Left = 456
    Top = 320
  end
  object ADOLease: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 536
    Top = 320
  end
  object ADOFinance: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 56
    Top = 584
  end
  object DataFinance: TDataSource
    DataSet = ADOFinance
    Left = 136
    Top = 584
  end
  object ADOCash: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 56
    Top = 520
  end
  object DataCash: TDataSource
    DataSet = ADOCash
    Left = 136
    Top = 520
  end
  object DataConcrete: TDataSource
    DataSet = ADOConcrete
    Left = 144
    Top = 472
  end
  object ADOConcrete: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 56
    Top = 472
  end
  object DataRebar: TDataSource
    DataSet = ADORebar
    Left = 240
    Top = 288
  end
  object ADORebar: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 176
    Top = 288
  end
  object PopupMenu2: TPopupMenu
    Images = DM.il1
    Left = 392
    Top = 192
    object N2: TMenuItem
      Caption = #32534#36753
    end
    object N3: TMenuItem
      Caption = #21024#38500
    end
    object N4: TMenuItem
      Caption = #23548#20986'Excel'
    end
  end
  object PopupMenu3: TPopupMenu
    Images = DM.il1
    Left = 192
    Top = 72
    object MenuItem1: TMenuItem
      Caption = #28155#21152#21512#21516
      ImageIndex = 8
    end
    object MenuItem2: TMenuItem
      Caption = #32534#36753#21512#21516
      ImageIndex = 17
    end
    object MenuItem3: TMenuItem
      Caption = #21024#38500#21512#21516
      ImageIndex = 13
    end
  end
  object DataFrame: TDataSource
    DataSet = ADOFrame
    Left = 285
    Top = 520
  end
  object ADOFrame: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 237
    Top = 520
  end
  object DataQualifications: TDataSource
    DataSet = ADOQualifications
    Left = 149
    Top = 344
  end
  object ADOQualifications: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 61
    Top = 344
  end
  object QryGrid4: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 48
    Top = 176
  end
  object dsGrid4: TDataSource
    DataSet = QryGrid4
    Left = 96
    Top = 176
  end
  object Print: TPopupMenu
    Images = DM.cxImageList1
    Left = 112
    Top = 72
    object MenuItem4: TMenuItem
      Caption = #25253#34920#35774#35745
      ImageIndex = 7
      OnClick = MenuItem4Click
    end
    object MenuItem5: TMenuItem
      Caption = #25171#21360#39044#35272
      OnClick = MenuItem4Click
    end
    object MenuItem6: TMenuItem
      Caption = #30452#25509#25171#21360
      ImageIndex = 6
      OnClick = MenuItem4Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Execl1: TMenuItem
      Caption = #23548#20986'Execl'
      OnClick = MenuItem4Click
    end
  end
  object dsGrid3: TDataSource
    DataSet = qryGrid3
    Left = 96
    Top = 136
  end
  object qryGrid3: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 48
    Top = 136
  end
  object ADOWork: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 48
    Top = 232
  end
  object DataWork: TDataSource
    DataSet = ADOWork
    Left = 104
    Top = 232
  end
  object ADOMaterial: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 184
    Top = 176
  end
  object DataMaterial: TDataSource
    DataSet = ADOMaterial
    Left = 168
    Top = 240
  end
  object ADOQuantity: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 456
    Top = 264
  end
  object DataQuantity: TDataSource
    DataSet = ADOQuantity
    Left = 536
    Top = 264
  end
  object ADODetailed: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 608
    Top = 264
  end
  object DataDetailed: TDataSource
    DataSet = ADODetailed
    Left = 688
    Top = 264
  end
  object frxGrid3: TfrxDBDataset
    UserName = #25903#27454#20132#26131#35760#24405
    CloseDataSource = False
    DataSource = dsGrid3
    BCDToCurrency = False
    Left = 680
    Top = 136
  end
  object frxWork: TfrxDBDataset
    UserName = #25903#27454#39033#30446#37096#32771#21220
    CloseDataSource = False
    DataSource = DataWork
    BCDToCurrency = False
    Left = 720
    Top = 136
  end
  object frxQuantity: TfrxDBDataset
    UserName = #25903#27454#39033#30446#20998#21253
    CloseDataSource = False
    DataSet = ADOQuantity
    BCDToCurrency = False
    Left = 776
    Top = 136
  end
  object frxDetailed: TfrxDBDataset
    Description = 'DataDetailed'
    UserName = #25903#27454#39033#30446#20998#21253#26126#32454
    CloseDataSource = False
    DataSet = ADODetailed
    BCDToCurrency = False
    Left = 840
    Top = 136
  end
  object ADOWorkGroup: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 832
    Top = 320
  end
  object DataWorkGroup: TDataSource
    DataSet = ADOWorkGroup
    Left = 832
    Top = 264
  end
  object ADOProjectName: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 440
    Top = 472
  end
  object DataProjectName: TDataSource
    DataSet = ADOProjectName
    Left = 552
    Top = 472
  end
  object ADOFlowingGroup: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 920
    Top = 168
  end
  object DataFlowingGroup: TDataSource
    DataSet = ADOFlowingGroup
    Left = 928
    Top = 216
  end
end
