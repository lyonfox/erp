object frmCompanyInfo: TfrmCompanyInfo
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #20844#21496#20449#24687
  ClientHeight = 339
  ClientWidth = 346
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object RzToolbar2: TRzToolbar
    Left = 0
    Top = 0
    Width = 346
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    Caption = #36864#20986
    GradientColorStyle = gcsCustom
    TabOrder = 0
    VisualStyle = vsGradient
    ExplicitLeft = 1
    ExplicitTop = 329
    ExplicitWidth = 272
    ToolbarControls = (
      RzSpacer12
      RzToolButton10
      RzSpacer13
      RzToolButton11
      RzSpacer15
      RzToolButton13
      RzSpacer14
      RzToolButton12
      RzSpacer1
      RzToolButton1)
    object RzSpacer12: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzToolButton10: TRzToolButton
      Left = 12
      Top = 2
      Width = 50
      SelectionColorStop = 16119543
      ImageIndex = 37
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #26032#22686
      OnClick = RzToolButton10Click
    end
    object RzSpacer13: TRzSpacer
      Left = 62
      Top = 2
    end
    object RzToolButton11: TRzToolButton
      Left = 70
      Top = 2
      Width = 50
      SelectionColorStop = 16119543
      ImageIndex = 0
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #32534#36753
      OnClick = RzToolButton11Click
    end
    object RzSpacer14: TRzSpacer
      Left = 178
      Top = 2
    end
    object RzToolButton12: TRzToolButton
      Left = 186
      Top = 2
      Width = 50
      SelectionColorStop = 16119543
      ImageIndex = 51
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21024#38500
      OnClick = RzToolButton12Click
    end
    object RzSpacer15: TRzSpacer
      Left = 120
      Top = 2
    end
    object RzToolButton13: TRzToolButton
      Left = 128
      Top = 2
      Width = 50
      ImageIndex = 3
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #20445#23384
      OnClick = RzToolButton13Click
    end
    object RzSpacer1: TRzSpacer
      Left = 236
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 244
      Top = 2
      Width = 65
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986
      OnClick = RzToolButton1Click
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 29
    Width = 346
    Height = 310
    Align = alClient
    TabOrder = 1
    ExplicitTop = 33
    object tvCompany: TcxGridDBTableView
      OnDblClick = tvCompanyDblClick
      Navigator.Buttons.CustomButtons = <>
      OnEditing = tvCompanyEditing
      DataController.DataSource = CompanySource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.DataRowHeight = 23
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 23
      OptionsView.Indicator = True
      object tvCompanyColumn1: TcxGridDBColumn
        Caption = #24207#21495
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        OnGetDisplayText = tvCompanyColumn1GetDisplayText
        HeaderAlignmentHorz = taCenter
        HeaderAlignmentVert = vaTop
        Options.Editing = False
        Options.Filtering = False
        Options.Moving = False
        Options.Sorting = False
        Width = 35
      end
      object tvCompanyColumn2: TcxGridDBColumn
        Caption = #20844#21496#21517#31216
        DataBinding.FieldName = 'SignName'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'SignName'
        Properties.ListColumns = <
          item
            Caption = #20844#21496#21517#31216
            FieldName = 'SignName'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = DM.DataCompany
        Properties.OnCloseUp = tvCompanyColumn2PropertiesCloseUp
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Moving = False
        Options.Sorting = False
        Width = 211
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = tvCompany
    end
  end
  object Company: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = CompanyAfterInsert
    Left = 64
    Top = 151
  end
  object CompanySource: TDataSource
    DataSet = Company
    Left = 64
    Top = 215
  end
  object pm: TPopupMenu
    Left = 160
    Top = 128
    object N1: TMenuItem
      Caption = #26032#12288#22686
    end
    object N2: TMenuItem
      Caption = #32534#12288#36753
    end
    object N3: TMenuItem
      Caption = #20445#12288#23384
    end
    object N4: TMenuItem
      Caption = #21024#12288#38500
    end
    object N5: TMenuItem
      Caption = #36864#12288#20986
    end
  end
end
