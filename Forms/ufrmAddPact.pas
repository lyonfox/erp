unit ufrmAddPact;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmAddPact = class(TForm)
    Button1: TButton;
    procedure RzBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_Prefix  : string;
  end;

var
  frmAddPact: TfrmAddPact;
  FilePath : string;
  g_sum_Money: Currency;
  g_Borrow_Code : string;
  g_TabName  : string = 'contract_tree';
  

implementation

uses global,TreeFillThrd,uDataModule,ShellAPI,ufrmInAccounts,ufrmManage,ufrmBorrowMoney;

{$R *.dfm}

procedure TfrmAddPact.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

end.
